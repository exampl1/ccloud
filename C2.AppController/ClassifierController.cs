﻿using C2.AppModel;
using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.AppController
{
    public class ClassifierController
    {
        /// <summary>
        /// Построение .
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static TreeModel BuildTree<TEntity>(Classifier classifier, IEnumerable<Tuple<ClassifierNode, TEntity>> source)
        {
            Dictionary<long, TreeNodeModel> nodes = new Dictionary<long, TreeNodeModel>();
            List<TreeNodeModel> roots = new List<TreeNodeModel>();
            
            foreach (var item in source)
            {
                if (nodes.ContainsKey(item.Item1.Id))
                {
                    // Возможно, что запрос был сформулирован таким образом, что возвращает
                    // дублирующие узлы (например, при локализации сущности узла). Такие узлы пропускаем.
                    continue;
                }

                TreeNodeModel node = new TreeNodeModel(item.Item1.Id, item.Item2);
                nodes.Add(node.NodeId, node);

                TreeNodeModel parent;
                if (nodes.TryGetValue(item.Item1.GetParentId(), out parent))
                {
                    parent.AddNode(node);
                }
                else
                {
                    roots.Add(node);
                }
            }

            return new TreeModel()
            {
                AllNodes = nodes.Values.ToList(),
                Roots = roots
            };
        }
    }
}
