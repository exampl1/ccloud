﻿using C2.DataMapping;
using C2.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace C2.AppController
{
    public class MailController
    {
        public static void SendMail(params MailQueue[] mails)
        {
            using (MailDbContext context = new MailDbContext())
            {
                foreach (var mail in mails)
                {
                    context.MailQueue.Add(mail);
                }
                context.SaveChanges();
            }
        }

        class MailDbContext : BaseContext
        {
            public MailDbContext()
                : base("DefaultConnection")
            {
            }
        }
    }

    public abstract class BaseMailTemplate : SimpleTemplateProcessor
    {
        protected BaseMailTemplate(string name)
            : base(LoadTemplate(name + ".tpl"))
        {
        }

        protected override bool ResolveInclude(string name, out string result)
        {
            if (base.ResolveInclude(name, out result))
            {
                return true;
            }
            result = LoadTemplate(name + ".tpl");
            return true;
        }

        static string LoadTemplate(string name)
        {
            try
            {
                WebRequest request = WebRequest.Create(Properties.Settings.Default.MailTemplateBaseUrl + name);
                var response = request.GetResponse();
                using (var stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
