﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using C2.DataModel;
using System.Collections;
using System.Data.Entity;
using C2.DataMapping;


namespace C2.AppController
{
    public class SupplierModel
    {
        private static Hashtable _config = new Hashtable();
        public static Supplier Current
        {
            get
            {
                if (_config[HttpContext.Current.Request.Url.DnsSafeHost] == null)
                {
                    InitFor(HttpContext.Current.Request.Url.DnsSafeHost);
                }

                return _config[HttpContext.Current.Request.Url.DnsSafeHost] as Supplier;
            }
        }

        private static void InitFor(string dns)
        {
            using (var dbContext = new AppContext("DefaultConnection"))
            {
                var q = dbContext
                    .Suppliers
                    .Include(x => x.Subject)
                    .Include(x => x.Subject.Localization);

                if (q.Count() == 0)
                {
                    AppController.GlobalIniters.Init();
                }

                var supplier = q
                    .Where(s => dns.Contains(s.Domain))
                    .FirstOrDefault();

                if (supplier == null)
                {
                    supplier = q
                        .FirstOrDefault();
                }

                lock (_config)
                {
                    if (!_config.ContainsKey(dns))
                    {
                        _config.Add(dns, supplier);
                    }
                }
            }
        }
    }

}