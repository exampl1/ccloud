﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.AppModel
{
    /// <summary>
    /// Модель данных "дерево".
    /// </summary>
    public class TreeModel
    {
        /// <summary>
        /// Линейный список всех узлов дерева.
        /// </summary>
        public List<TreeNodeModel> AllNodes
        {
            get;
            set;
        }

        /// <summary>
        /// Список корневых узлов дерева.
        /// </summary>
        public List<TreeNodeModel> Roots
        {
            get;
            set;
        }
    }
}
