﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.AppModel
{
    /// <summary>
    /// Модель данных узла дерева.
    /// </summary>
    public class TreeNodeModel
    {
        public TreeNodeModel(long nodeId, object value, string path = null, string caption = null)
        {
            _nodes = new List<TreeNodeModel>();
            NodeId = nodeId;
            Value = value;
            Path = path;
            Caption = caption;
        }

        List<TreeNodeModel> _nodes;

        /// <summary>
        /// Идентификатор узла.
        /// </summary>
        public long NodeId
        {
            get;
            private set;
        }

        /// <summary>
        /// Читабельное значение
        /// </summary>
        public string Caption
        {
            get;
            private set;
        }

        /// <summary>
        /// Путь до узла
        /// </summary>
        public string Path
        {
            get;
            private set;
        }

        /// <summary>
        /// Контекстные данные.
        /// </summary>
        public object Value
        {
            get;
            private set;
        }

        /// <summary>
        /// Родительский узел.
        /// </summary>
        public TreeNodeModel Parent
        {
            get;
            set;
        }

        /// <summary>
        /// Дочерние узлы.
        /// </summary>
        public List<TreeNodeModel> Nodes
        {
            get
            {
                return _nodes;
            }
        }

        /// <summary>
        /// Признак, что узел не содержит вложенных узлов.
        /// </summary>
        public bool IsLeaf
        {
            get
            {
                return _nodes.Count == 0;
            }
        }

        /// <summary>
        /// Добавление дочернего узла.
        /// </summary>
        /// <param name="node"></param>
        public void AddNode(TreeNodeModel node)
        {
            _nodes.Add(node);
            node.Parent = this;
        }
    }
}
