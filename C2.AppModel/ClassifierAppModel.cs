﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.AppModel
{
    /// <summary>
    /// Модель данных классификатора уровня приложения.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class ClassifierAppModel<TEntity>
    {
        /// <summary>
        /// Линейный список всех узлов классификатора.
        /// </summary>
        public List<ClassifierNodeAppModel<TEntity>> AllNodes
        {
            get;
            set;
        }

        /// <summary>
        /// Список корневых узлов классификатора.
        /// </summary>
        public List<ClassifierNodeAppModel<TEntity>> Roots
        {
            get;
            set;
        }
    }
}
