﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.AppModel
{
    public class ClassifierNodeAppModel<TEntity>
    {
        public ClassifierNodeAppModel(long nodeId, TEntity value)
        {
            _nodes = new List<ClassifierNodeAppModel<TEntity>>();
            NodeId = nodeId;
            Value = value;
        }

        List<ClassifierNodeAppModel<TEntity>> _nodes;

        /// <summary>
        /// Идентификатор узла.
        /// </summary>
        public long NodeId
        {
            get;
            private set;
        }

        /// <summary>
        /// Контекстные данные.
        /// </summary>
        public TEntity Value
        {
            get;
            private set;
        }

        public ClassifierNodeAppModel<TEntity> Parent
        {
            get;
            set;
        }

        /// <summary>
        /// Дочерние узлы.
        /// </summary>
        public List<ClassifierNodeAppModel<TEntity>> Nodes
        {
            get
            {
                return _nodes;
            }
        }

        /// <summary>
        /// Признак, что узел не содержит вложенных узлов.
        /// </summary>
        public bool IsLeaf
        {
            get
            {
                return _nodes.Count == 0;
            }
        }

        public void AddNode(ClassifierNodeAppModel<TEntity> node)
        {
            _nodes.Add(node);
            node.Parent = this;
        }
    }
}
