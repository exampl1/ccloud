﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.Entity
{
    /// <summary>
    /// Интерфейс-маркер для фильтрующих модификаторов.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IFilterModificator<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> Apply(IQueryable<TEntity> query);
    }

    /// <summary>
    /// Добавление в запрос условий поиска.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class FilterModificator<TEntity> : IFilterModificator<TEntity>
        where TEntity : class
    {
        public FilterModificator(Expression<Func<TEntity, bool>> predicate)
        {
            _predicate = predicate;
        }

        Expression<Func<TEntity, bool>> _predicate;

        public IQueryable<TEntity> Apply(IQueryable<TEntity> query)
        {
            return query.Where(_predicate);
        }
    }

    /// <summary>
    /// Интерфейс-маркер для модификаторов, управляющих загрузкой образов сущностей.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IIncludeModificator<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> Apply(IQueryable<TEntity> query);
    }

    /// <summary>
    /// Добавление в запрос путей совместно загружаемых свойств.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TProperty"></typeparam>
    public class IncludeModificator<TEntity, TProperty> : IIncludeModificator<TEntity>
        where TEntity : class
    {
        public IncludeModificator(Expression<Func<TEntity, TProperty>> path)
        {
            _path = path;
        }

        Expression<Func<TEntity, TProperty>> _path;

        public IQueryable<TEntity> Apply(IQueryable<TEntity> query)
        {
            return query.Include(_path);
        }
    }

    /// <summary>
    /// Интерфейс-маркер для сортирующих модификаторов.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IOrderModificator<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> Apply(IQueryable<TEntity> query, int index);
    }

    /// <summary>
    /// Добавление в запрос сортировки.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TProperty"></typeparam>
    public class OrderModificator<TEntity, TProperty> : IOrderModificator<TEntity>
        where TEntity : class
    {
        public OrderModificator(Expression<Func<TEntity, TProperty>> keySelector, bool isAsc = true)
        {
            _keySelector = keySelector;
            _isAsc = isAsc;
        }

        Expression<Func<TEntity, TProperty>> _keySelector;

        bool _isAsc;

        public IQueryable<TEntity> Apply(IQueryable<TEntity> query, int index)
        {
            if (index > 0)
            {
                // Query already ordered.
                IOrderedQueryable<TEntity> orderedQuery = (IOrderedQueryable<TEntity>)query;
                return _isAsc ? orderedQuery.ThenBy(_keySelector) : orderedQuery.ThenByDescending(_keySelector);
            }
            else
            {
                return _isAsc ? query.OrderBy(_keySelector) : query.OrderByDescending(_keySelector);
            }
        }
    }

    public static class QueryExtensions
    {
        public static IQueryable<TEntity> Apply<TEntity>(this IQueryable<TEntity> query, IEnumerable<IFilterModificator<TEntity>> filters)
            where TEntity : class
        {
            if (filters != null)
            {
                foreach (var filter in filters)
                {
                    query = filter.Apply(query);
                }
            }

            return query;
        }

        public static IQueryable<TEntity> Apply<TEntity>(this IQueryable<TEntity> query, IEnumerable<IOrderModificator<TEntity>> filters)
            where TEntity : class
        {
            if (filters != null)
            {
                int index = 0;
                foreach (var filter in filters)
                {
                    query = filter.Apply(query, index);
                    index++;
                }
            }
            return query;
        }

        public static IQueryable<TEntity> Apply<TEntity>(this IQueryable<TEntity> query, IEnumerable<IIncludeModificator<TEntity>> filters)
            where TEntity : class
        {
            if (filters != null)
            {
                foreach (var filter in filters)
                {
                    query = filter.Apply(query);
                }
            }
            return query;
        }
    }
}
