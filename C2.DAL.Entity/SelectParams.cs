﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.Entity
{
    public class SelectParams<TEntity>
        where TEntity : class
    {
        public SelectParams()
        {
            PageIndex = 0;
            PageSize = 100;
        }

        public SelectParams(int pageIndex,
            int pageSize,
            IEnumerable<IFilterModificator<TEntity>> filters = null,
            IEnumerable<IOrderModificator<TEntity>> orders = null,
            IEnumerable<IIncludeModificator<TEntity>> includes = null)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            Filters = filters;
            Orders = orders;
            Includes = includes;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public IEnumerable<IFilterModificator<TEntity>> Filters
        {
            get;
            set;
        }

        public IEnumerable<IOrderModificator<TEntity>> Orders
        {
            get;
            set;
        }

        public IEnumerable<IIncludeModificator<TEntity>> Includes
        {
            get;
            set;
        }

    }
}
