﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public abstract class Column : ISqlExpression
    {
        protected Column(IDataSource dataSource, string name)
        {
            Name = name;
        }

        public IDataSource Source
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public void ToSql(TextWriter writer)
        {
            writer.Write("{0}.[{1}]", Source.Reference, Name);
        }
    }

    public class BooleanColumn : Column, IBooleanExpression
    {
        public BooleanColumn(IDataSource dataSource, string name)
            : base(dataSource, name)
        {
        }
    }

    public class DateTimeColumn : Column, IDateTimeExpression
    {
        public DateTimeColumn(IDataSource dataSource, string name)
            : base(dataSource, name)
        {
        }
    }

    public class GeographicColumn : Column, IGeographicExpression
    {
        public GeographicColumn(IDataSource dataSource, string name)
            : base(dataSource, name)
        {
        }
    }

    public class NumberColumn : Column, INumberExpression
    {
        public NumberColumn(IDataSource dataSource, string name)
            : base(dataSource, name)
        {
        }
    }

    public class StringColumn : Column, IStringExpression
    {
        public StringColumn(IDataSource dataSource, string name)
            : base(dataSource, name)
        {
        }
    }
}
