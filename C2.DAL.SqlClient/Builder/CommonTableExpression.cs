﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public class CommonTableExpression : IDataSource
    {
        public CommonTableExpression(Query source, string name, string alias = null)
        {
            _source = source;
            Name = name;
            Alias = alias;
        }

        public string Name
        {
            get;
            private set;
        }

        public string Alias
        {
            get;
            private set;
        }

        Query _source;

        public void ToFromSql(TextWriter writer)
        {
            writer.WriteLine();
            writer.Write(" [{0}]", Name);

            // TODO: columns

            writer.Write(" AS (");
            _source.ToSql(writer);
            writer.Write(")");
        }

        public string Reference
        {
            get
            {
                return string.IsNullOrEmpty(Alias)
                    ? string.Format("[{0}]", Name)
                    : string.Format("[{0}]", Alias);
            }
        }
    }
}
