﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public interface IBooleanExpression : ISqlExpression
    {
    }

    public interface IDateTimeExpression : ISqlExpression
    {
    }

    public interface IGeographicExpression : ISqlExpression
    {
    }

    public interface INumberExpression : ISqlExpression
    {
    }

    public interface IStringExpression : ISqlExpression
    {
    }
}
