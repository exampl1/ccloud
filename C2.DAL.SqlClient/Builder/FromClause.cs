﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace C2.DAL.SqlClient.Builder
{
    public class FromClause : ISqlClause
    {
        public FromClause(IDataSource source)
        {
            _items = new List<ClauseItem>()
            {
                new ClauseItem()
                {
                    TableSource = source
                }
            };
        }

        List<ClauseItem> _items;

        struct ClauseItem
        {
            public IDataSource TableSource;

            public SearchCondition Condition;

            public JoinType JoinType;

            public JoinHints JoinHints;
        }

        public FromClause Join(IDataSource source,
            SearchCondition condition,
            JoinType joinType = JoinType.Inner,
            JoinHints joinHints = JoinHints.None)
        {
            _items.Add(new ClauseItem
            {
                TableSource = source,
                Condition = condition,
                JoinType = joinType,
                JoinHints = joinHints
            });
            return this;
        }

        public void ToSql(TextWriter writer)
        {
            var item = _items[0];

            writer.WriteLine();
            writer.Write(" FROM ");
            item.TableSource.ToFromSql(writer);

            for (var i = 1; i < _items.Count; i++)
            {
                item = _items[i];

                writer.WriteLine();
                switch (item.JoinType)
                {
                    case JoinType.Inner:
                        writer.Write(" INNER ");
                        break;
                    case JoinType.LeftOuter:
                        writer.Write(" OUTER LEFT");
                        break;
                    case JoinType.RightOuter:
                        writer.Write(" OUTER RIGHT");
                        break;
                    case JoinType.FullOuter:
                        writer.Write(" OUTER FULL");
                        break;
                }
                if (item.JoinHints != JoinHints.None)
                {
                    writer.Write(string.Format(" {0}", item.JoinHints).ToUpper());
                }
                writer.Write(" JOIN ");
                item.TableSource.ToFromSql(writer);
                writer.Write(" ON ");
                item.Condition.ToSql(writer);
            }
        }
    }
}
