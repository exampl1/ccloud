﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public interface IDataSource
    {
        void ToFromSql(TextWriter writer);

        string Reference
        {
            get;
        }
    }

    public class QueryAdapter : IDataSource
    {
        public QueryAdapter(Query query, string alias)
        {
            _query = query;
            _alias = alias;
        }

        Query _query;

        string _alias;

        public void ToFromSql(TextWriter writer)
        {
            writer.Write("(");
            _query.ToSql(writer);
            writer.Write(") AS [{0}]", _alias);
        }

        public string Reference
        {
            get
            {
                return string.Format("[{0}]", _alias);
            }
        }
    }
}
