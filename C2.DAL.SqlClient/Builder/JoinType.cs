﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public enum JoinType
    {
        Inner,

        LeftOuter,

        RightOuter,

        FullOuter
    }
}
