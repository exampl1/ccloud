﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public class OptionClause : ISqlClause
    {
        Dictionary<string, string> _items = new Dictionary<string, string>();

        public OptionClause Group(QueryGroupHints hint)
        {
            _items["GROUP"] = Enum.GetName(typeof(QueryGroupHints), hint).ToUpperInvariant() + " GROUP";
            return this;
        }

        public OptionClause Union(QueryUnionHints hint)
        {
            _items["UNION"] = Enum.GetName(typeof(QueryUnionHints), hint).ToUpperInvariant() + " UNION";
            return this;
        }

        public OptionClause Join(JoinHints hint)
        {
            if (hint != JoinHints.None)
            {
                _items["JOIN"] = Enum.GetName(typeof(JoinHints), hint).ToUpperInvariant() + " JOIN";
            }
            return this;
        }

        public OptionClause ExpandViews()
        {
            _items["EXPAND VEIWS"] = "EXPAND VEIWS";
            return this;
        }

        public OptionClause Fast(int numberRows)
        {
            _items["FAST"] = string.Format("FAST {0}", numberRows);
            return this;
        }

        public OptionClause ForceOrder()
        {
            _items["FORCE ORDER"] = "FORCE ORDER";
            return this;
        }

        public OptionClause IgnoreNonclusteredColumnstoreIndex()
        {
            _items["IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX"] = "IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX";
            return this;
        }

        public OptionClause KeepPlan()
        {
            _items["KEEP PLAN"] = "KEEP PLAN";
            return this;
        }

        public OptionClause KeepFixedPlan()
        {
            _items["KEEPFIXED PLAN"] = "KEEPFIXED PLAN";
            return this;
        }

        public OptionClause MaxDop(int numberOfProcessors)
        {
            _items["MAXDOP"] = string.Format("MAXDOP {0}", numberOfProcessors);
            return this;
        }

        public OptionClause MaxRecursion(int number)
        {
            _items["MAXRECURSION"] = string.Format("MAXRECURSION {0}", number);
            return this;
        }

        public OptionClause Parametrization(QueryParametrizationHints hint)
        {
            _items["PARAMETERIZATION"] = string.Format("PARAMETERIZATION {0}", Enum.GetName(typeof(QueryParametrizationHints), hint).ToUpperInvariant());
            return this;
        }

        public OptionClause Recompile()
        {
            _items["RECOMPILE"] = "RECOMPILE";
            return this;
        }

        public OptionClause RobustPlan()
        {
            _items["ROBUST PLAN"] = "ROBUST PLAN";
            return this;
        }

        public void ToSql(TextWriter writer)
        {
            if (_items.Count > 0)
            {
                writer.Write(" OPTION (");
                writer.Write(string.Join(", ", _items.Values));
                writer.Write(")");
            }
        }
    }

    public enum QueryGroupHints
    {
        Hash,

        Order
    }

    public enum QueryUnionHints
    {
        Concat,

        Hash,

        Merge
    }

    public enum QueryParametrizationHints
    {
        Simple,

        Forced
    }
}
