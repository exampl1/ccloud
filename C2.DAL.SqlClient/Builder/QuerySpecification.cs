﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public class QuerySpecification : ISqlClause
    {
        public SelectClause SelectClause
        {
            get;
            private set;
        }

        public FromClause FromClause
        {
            get;
            private set;
        }

        public SearchCondition WhereClause
        {
            get;
            private set;
        }

        public GroupByClause GroupBy
        {
            get;
            private set;
        }

        public QuerySpecification Join(IDataSource source,
            SearchCondition condition,
            IEnumerable<ISqlExpression> columns,
            JoinType joinType = JoinType.Inner,
            JoinHints joinHints = JoinHints.None)
        {
            FromClause.Join(source, condition, joinType, joinHints);
            SelectClause.Add(columns);

            return this;
        }

        public void ToSql(TextWriter writer)
        {
            SelectClause.ToSql(writer);
            FromClause.ToSql(writer);
            if ((WhereClause != null) && !WhereClause.IsEmpty)
            {
                writer.WriteLine();
                writer.Write(" WHERE");
                WhereClause.ToSql(writer);
            }
            if ((GroupBy != null) && !GroupBy.IsEmpty)
            {
                GroupBy.ToSql(writer);
            }
        }
    }
}
