﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace C2.DAL.SqlClient.Builder
{
    public interface ISearchConditionItem : ISqlExpression
    {
    }

    public class SearchCondition : ISqlExpression, ISearchConditionItem
    {
        static readonly SqlLiteral NotToken = " NOT ";
        
        static readonly SqlLiteral AndToken = " AND ";
        
        static readonly SqlLiteral OrToken = " OR ";
        
        static readonly SqlLiteral OpenBraketToken = "(";
        
        static readonly SqlLiteral CloseBraketToken = ")";

        List<ISqlExpression> _items = new List<ISqlExpression>();

        void Add(params ISqlExpression[] items)
        {
            _items.AddRange(items);
        }

        void Add(ISearchConditionItem rvalue)
        {
            Add(OpenBraketToken, rvalue, CloseBraketToken);
        }

        public SearchCondition Not(ISearchConditionItem rvalue)
        {
            Add(NotToken);
            Add(rvalue);
            return this;
        }

        public SearchCondition And(ISearchConditionItem rvalue)
        {
            if (!IsEmpty)
            {
                Add(AndToken);
            }
            Add(rvalue);
            return this;
        }

        public SearchCondition AndNot(ISearchConditionItem rvalue)
        {
            if (!IsEmpty)
            {
                Add(AndToken);
            }
            return Not(rvalue);
        }

        public SearchCondition Or(ISearchConditionItem rvalue)
        {
            if (!IsEmpty)
            {
                Add(OrToken);
            }
            Add(rvalue);
            return this;
        }

        public SearchCondition OrNot(ISearchConditionItem rvalue)
        {
            if (!IsEmpty)
            {
                Add(OrToken);
            }
            return Not(rvalue);
        }

        public void ToSql(TextWriter writer)
        {
            _items.ToSql(writer);
        }

        public bool IsEmpty
        {
            get
            {
                return _items.Count == 0;
            }
        }
    }
}
