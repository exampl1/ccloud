﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace C2.DAL.SqlClient.Builder
{
    public class SelectClause : ISqlClause
    {
        public SelectClause()
        {
            Сolumns = new List<ISqlExpression>();
        }

        public SelectClause Add(IEnumerable<ISqlExpression> columns)
        {
            Сolumns.AddRange(columns);
            return this;
        }

        public SelectClause Add(params ISqlExpression[] columns)
        {
            return Add((IEnumerable<ISqlExpression>)columns);
        }

        public List<ISqlExpression> Сolumns
        {
            get;
            private set;
        }

        public bool Distinct
        {
            get;
            set;
        }

        public int Top
        {
            get;
            set;
        }

        public bool UsePercent
        {
            get;
            set;
        }

        public bool WithTies
        {
            get;
            set;
        }

        public void ToSql(TextWriter writer)
        {
            writer.Write("SELECT ");
            if (Distinct)
            {
                writer.Write("DISTINCT ");
            }

            if (Top > 0)
            {
                writer.Write("TOP ({0}) ", Top);
                if (UsePercent)
                {
                    writer.Write("PERCENT ");
                }
                if (WithTies)
                {
                    writer.Write("WITH TIES ");
                }
            }

            if (Сolumns.Count > 0)
            {
                for (var i = 0; i < Сolumns.Count; i++)
                {
                    if (i > 0)
                    {
                        writer.Write(", ");
                    }
                    Сolumns[i].ToSql(writer);
                }
            }
            else
            {
                writer.Write("*");
            }
        }
    }
}
