﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public sealed class SubQuery : IDataSource
    {
        public SubQuery(Query query, string alias)
        {
        }

        public Query Query
        {
            get;
            private set;
        }

        public string Alias
        {
            get;
            private set;
        }

        public void ToFromSql(TextWriter writer)
        {
            writer.Write("(");
            Query.ToSql(writer);
            writer.Write(") AS [{0}]", Alias);
        }

        public string Reference
        {
            get
            {
                return Alias;
            }
        }
    }
}
