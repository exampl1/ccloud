﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public abstract class Table : IDataSource, ISchemableEntity
    {
        public Table(string name, string schema = null)
        {
            Name = name;
            Schema = schema;
        }

        public string Name
        {
            get;
            set;
        }

        public string Schema
        {
            get;
            set;
        }

        public string Alias
        {
            get;
            set;
        }

        public TableHints Hints
        {
            get;
            set;
        }

        public void ToFromSql(TextWriter writer)
        {
            writer.Write(this.GetFullName());
            if (!string.IsNullOrWhiteSpace(Alias))
            {
                writer.Write(" AS [{0}]", Alias);
            }
            if (Hints != null)
            {
                Hints.ToSql(writer);
            }
        }

        public string Reference
        {
            get
            {
                return string.IsNullOrEmpty(Alias)
                    ? this.GetFullName()
                    : string.Format("[{0}]", Alias);
            }
        }
    }

    public sealed class GenericTable<TFields> : Table
    {
        public GenericTable(TFields fields, string name, string schema = null)
            : base(name, schema)
        {
            Name = name;
            Schema = schema;
            Fields = fields;
        }

        public TFields Fields
        {
            get;
            private set;
        }
    }

    public class Test
    {
        public void Do()
        {
            List<System.Globalization.NumberFormatInfo> l = null;
            var q = l.Select(i => new { i.CurrencySymbol, i.PositiveSign });
            var q1 = q;

            var table = CreateTable(new {
                Id = new NumberColumn(null, "Id")
            }, "product");
            var t = table;
        }

        GenericTable<TFields> CreateTable<TFields>(TFields fields, string name)
        {
            return new GenericTable<TFields>(fields, name);
        }
    }
}
