﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public class TableHints : ISqlExpression
    {
        Dictionary<string, string> _items = new Dictionary<string, string>();

        public TableHints NoExpand()
        {
            _items["NOEXPAND"] = "NOEXPAND";
            return this;
        }

        public TableHints Index(params string[] indexes)
        {
            _items["INDEX"] = "INDEX(" + string.Join(",", indexes) + ")";
            return this;
        }

        public TableHints ForceScan()
        {
            _items["FORCESCAN"] = "FORCESCAN";
            return this;
        }

        public TableHints ForceSeek()
        {
            _items["FORCESEEK"] = "FORCESEEK";
            return this;
        }

        public TableHints HoldLock()
        {
            _items["HOLDLOCK"] = "HOLDLOCK";
            return this;
        }

        public TableHints NoLock()
        {
            _items["NOLOCK"] = "NOLOCK";
            return this;
        }

        public TableHints NoWait()
        {
            _items["NOWAIT"] = "NOWAIT";
            return this;
        }

        public TableHints PageLock()
        {
            _items["PAGLOCK"] = "PAGLOCK";
            return this;
        }

        public TableHints ReadCommitted()
        {
            _items["READCOMMITTED"] = "READCOMMITTED";
            return this;
        }

        public TableHints ReadCommittedLock()
        {
            _items["READCOMMITTEDLOCK"] = "READCOMMITTEDLOCK";
            return this;
        }

        public TableHints ReadPast()
        {
            _items["READPAST"] = "READPAST";
            return this;
        }

        public TableHints ReadUncommitted()
        {
            _items["READUNCOMMITTED"] = "READUNCOMMITTED";
            return this;
        }

        public TableHints RepeatableRead()
        {
            _items["REPEATABLEREAD"] = "REPEATABLEREAD";
            return this;
        }

        public TableHints RowLock()
        {
            _items["ROWLOCK"] = "ROWLOCK";
            return this;
        }

        public TableHints Serializable()
        {
            _items["SERIALIZABLE"] = "SERIALIZABLE";
            return this;
        }

        public TableHints SpatialWindowMaxCells(int value)
        {
            _items["SPATIAL_WINDOW_MAX_CELLS"] = string.Format("SPATIAL_WINDOW_MAX_CELLS = {0}", value);
            return this;
        }

        public TableHints TabLock()
        {
            _items["TABLOCK"] = "TABLOCK";
            return this;
        }

        public TableHints TabLockX()
        {
            _items["TABLOCKX"] = "TABLOCKX";
            return this;
        }

        public TableHints UpdLock()
        {
            _items["UPDLOCK"] = "UPDLOCK";
            return this;
        }

        public TableHints XLock()
        {
            _items["XLOCK"] = "XLOCK";
            return this;
        }

        public void ToSql(TextWriter writer)
        {
            if (_items.Count > 0)
            {
                writer.Write(" WITH (");
                writer.Write(string.Join(", ", _items.Values));
                writer.Write(")");
            }
        }
    }
}
