﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace C2.DAL
{
    public class IdGenerator : IIdGenerator<long>
    {
        /// <summary>
        /// Создание генератора уникальных идентфикаторов на основе SEQUENCE.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="sequnceName"></param>
        /// <returns></returns>
        public static IdGenerator CreateSequenceGenerator(string connectionString, string sequnceName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Создание генератора уникальных идентфикаторов на основе IDENTITY.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static IdGenerator CreateIdentityGenerator(string connectionString, string tableName)
        {
            // Получим размера пула на основе шага IDENTITY
            int increment;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = string.Format("select IDENT_INCR('{0}')", tableName);
                    connection.Open();
                    increment = Convert.ToInt32(command.ExecuteScalar());
                }
            }

            string sql = string.Format("INSERT INTO {0} (Created) VALUES (GETUTCDATE()); SELECT @@IDENTITY;", tableName);

            return new IdGenerator(connectionString, sql, increment);
        }

        public IdGenerator(string connectionString, string sql, int poolSize = 1000)
        {
            _connectionString = connectionString;
            _sql = sql;
            _poolSize = poolSize;
        }

        public long GetId()
        {
            long maxId;
            long id;
            do
            {
                maxId = _maxId;
                id = Interlocked.Increment(ref _curId);

                if (maxId < id)
                {
                    GetNewPool();
                }

            } while (maxId < id);

            return id;
        }

        void GetNewPool()
        {
            lock (_syncRoot)
            {
                if (_maxId < _curId)
                {
                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        SqlCommand command = connection.CreateCommand();
                        command.CommandText = _sql;

                        connection.Open();
                        long curId = Convert.ToInt64(command.ExecuteScalar());
                        long maxId = curId + _poolSize;
                        _curId = curId;
                        _maxId = maxId;
                    }
                }
            }
        }

        string _connectionString;

        string _sql;

        object _syncRoot = new object();

        int _poolSize;

        long _curId;

        long _maxId;
    }
}
