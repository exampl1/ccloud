﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL
{
    /// <summary>
    /// Result of execute select query.
    /// </summary>
    /// <typeparam name="TEntity">Type of loaded data.</typeparam>
    public class SelectResult<TEntity> : ISelectResult
    {
        public SelectResult(List<TEntity> items, long totalCount, int pageIndex, int pageSize)
        {
            Items = items;
            TotalCount = totalCount;
            PageIndex = pageIndex;
            PageSize = pageSize;
        }

        public SelectResult(List<TEntity> items)
            : this(items, items.Count, 0, 100)
        {
        }

        /// <summary>
        /// Loaded data items.
        /// </summary>
        public List<TEntity> Items
        {
            get;
            private set;
        }

        /// <summary>
        /// Total number of items.
        /// </summary>
        public long TotalCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Index of loaded page.
        /// </summary>
        public int PageIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Page size.
        /// </summary>
        public int PageSize
        {
            get;
            private set;
        }

        /// <summary>
        /// Total number of pages.
        /// </summary>
        public int PageCount
        {
            get
            {
                return (int)Math.Ceiling(((decimal)TotalCount / (decimal)PageSize));
            }
        }

        public bool IsFirstPage
        {
            get
            {
                return (PageIndex == 0);
            }
        }

        public bool IsLastPage
        {
            get
            {
                return (PageIndex == (PageCount - 1));
            }
        }
    }
}
