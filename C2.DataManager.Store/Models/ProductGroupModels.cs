﻿using C2.DataModel;
using C2.DataModel.StoreModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.DataManager.Store.Models
{
    public class ProductGroupModel
    {
        /// <summary>
        /// путь ноды в классификаторе
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// путь до родительской ноды
        /// </summary>
        public string ParentPath { get; set; }

        /// <summary>
        /// Название группы продуктов
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Код группы продуктов. Задается програмно
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Id ноды
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Положение ноды в группе
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Валидность модели для сохранения данных
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return !(string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(Description));
        }

    }
}