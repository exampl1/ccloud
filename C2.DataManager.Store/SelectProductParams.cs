﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataManager.Store
{
    /// <summary>
    /// Параметры выбора товаров.
    /// </summary>
    public class SelectProductParams
    {
        public SelectProductParams()
        {
            OrderBy = "Order";
            OrderDir = Properties.Settings.Default.DefaultSortDirection;
            PageSize = 32;
            CurrencyCode = "RUB";
        }

        public string UserCultureCode
        {
            get;
            set;
        }

        public string DefaultCultureCode
        {
            get;
            set;
        }

        public string OrderBy
        {
            get;
            set;
        }

        public string OrderDir
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int Page
        {
            get { return PageIndex + 1; }
            set { PageIndex = value - 1; }
        }

        public int PageSize
        {
            get;
            set;
        }

        public string CurrencyCode
        {
            get;
            set;
        }

        public long SupplierId
        {
            get;
            set;
        }

        public long CategoryNodeId
        {
            get;
            set;
        }

        public int? Selling
        {
            get;
            set;
        }

        public string VendorCode
        {
            get;
            set;
        }

        public decimal? MinPrice
        {
            get;
            set;
        }

        public decimal? MaxPrice
        {
            get;
            set;
        }

        private string freeText;
        public string FreeText
        {
            get { return freeText; }
            set { freeText = value.Trim(); }
        }
    
        public long[] ProductIds
        {
            get;
            set;
        }
    }
}
