﻿    using C2.AppController;
using C2.AppModel;
using C2.DAL;
using C2.DAL.Entity;
using C2.DataManager.Store.Models;
using C2.DataMapping.Store;
using C2.DataModel;
using C2.DataModel.StoreModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using C2.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace C2.DataManager.Store
{
    /// <summary>
    /// Менеджер данных для модуля Store.
    /// </summary>
    public class StoreDataManager
    {
        public static IdGenerator IdGenerator;

        static StoreDataManager()
        {
            IdGenerator = IdGenerator.CreateIdentityGenerator(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString, "dbo.UIdTable");
        }

        public StoreDataManager(StoreContext context)
        {
            _context = context;
        }

        StoreContext _context;

        /// <summary>
        /// Получение классификатора товарных групп.
        /// </summary>
        /// <param name="classifierCode"></param>
        /// <param name="root"></param>
        /// <param name="maxLevel"></param>
        /// <returns></returns>
        public TreeModel GetProductGroupClassifier(string classifierCode = ProductGroup.DefaultClassifierCode,
            string root = null,
            int maxLevel = -1)
        {
            //var classifier = _context.Classifiers
            //    .FirstOrDefault(c => (c.ClassifierType == 1) && (c.Code == classifierCode) && (c.Status == EntityStatus.Normal));

            //if (classifier == null)
            //{
            //    return null;
            //}
            
            //var dataQuery = from n in _context.ClassifierNodes
            //                join g in _context.ProductGroups on n.EntityId equals g.Id
            //                join l in _context.DictionaryItemLocalizations on g.Id equals l.DictionaryItemId
            //                where (n.Status == EntityStatus.Normal)
            //                    && (g.Status == EntityStatus.Normal)
            //                select new
            //                {
            //                    Entity = g,
            //                    Localization = l,
            //                    Node = n
            //                };

            //if (root != null)
            //{
            //    dataQuery = from d in dataQuery
            //                where d.Node.Path.StartsWith(root)
            //                select d;
            //}

            //if (maxLevel > -1)
            //{
            //    dataQuery = from d in dataQuery
            //                where d.Node.Level <= maxLevel
            //                select d;
            //}

            //dataQuery = from d in dataQuery
            //        orderby d.Node.Level, d.Node.Order
            //        select d;

            //return ClassifierController.LoadClassifier<ProductGroup>(classifier, dataQuery.Select(i => new Tuple<ClassifierNode, ProductGroup>(i.Node, i.Entity)).ToList());

            var query = from c in _context.Classifiers
                        join n in _context.ClassifierNodes on c.Id equals n.ClassifierId
                        join g in _context.ProductGroups on n.EntityId equals g.Id
                        join l in _context.DictionaryItemLocalizations on g.Id equals l.DictionaryItemId
                        where (c.Code == classifierCode)
                            && (c.ClassifierType == 1)
                            && (c.Status == EntityStatus.Normal)
                            && (n.Status == EntityStatus.Normal)
                            && (g.Status == EntityStatus.Normal)
                        select new { Classifier = c, Node = n, Group = g, Localization = l };

            if (root != null)
            {
                query = from d in query
                        where d.Node.Path.StartsWith(root) && !d.Node.Path.Equals(root)
                        select d;
            }

            if (maxLevel > -1)
            {
                query = from d in query
                        where d.Node.Level <= maxLevel
                        select d;
            }

            query = from d in query
                    orderby d.Node.Level, d.Node.Order
                    select d;

            Dictionary<long, TreeNodeModel> nodes = new Dictionary<long, TreeNodeModel>();
            List<TreeNodeModel> roots = new List<TreeNodeModel>();
            foreach (var item in query)
            {
                if (nodes.ContainsKey(item.Node.Id))
                {
                    // Т.к. мы делаем join с локализацией товарных групп, то у нас могут быть повторения узлов.
                    // Такие узлы пропускаем.
                    continue;
                }

                TreeNodeModel node = new TreeNodeModel(item.Node.Id, item.Group, item.Node.Path, item.Group.CurrentLocalization.Title);
                nodes.Add(node.NodeId, node);

                TreeNodeModel parent;
                if (nodes.TryGetValue(item.Node.GetParentId(), out parent))
                {
                    parent.AddNode(node);
                }
                else
                {
                    roots.Add(node);
                }
            }

            return new TreeModel()
                {
                    AllNodes = nodes.Values.ToList(),
                    Roots = roots
                };
        }

        /// <summary>
        /// Получение списка товаров.
        /// </summary>
        /// <param name="params">Параметры выбора.</param>
        /// <returns></returns>
        public SelectResult<Product> SelectProducts(SelectProductParams @params)
        {
            var query = from p in _context.Products
                        join o in _context.Offers on p.Id equals o.ProductId
                        where (p.Status == EntityStatus.Normal) &&
                            (o.Status == EntityStatus.Normal) &&
                            (o.CurrencyCode == @params.CurrencyCode) &&
                            (o.AvailQuantity > 0)
                        select new { Product = p, Offer = o};

            // filters:
            if (@params.ProductIds != null)
            {
                query = query.Where(i => @params.ProductIds.Contains(i.Product.Id));
            }
            if (!string.IsNullOrWhiteSpace(@params.VendorCode))
            {
                query = query.Where(i => i.Product.VendorCode == @params.VendorCode);
            }
            if (@params.MinPrice.HasValue)
            {
                query = query.Where(i => i.Offer.Price >= @params.MinPrice.Value);
            }
            if (@params.MaxPrice.HasValue)
            {
                query = query.Where(i => i.Offer.Price <= @params.MaxPrice.Value);
            }
            if (@params.CategoryNodeId > 0)
            {
                var classifierNode = _context.ClassifierNodes.Find(@params.CategoryNodeId);
                if (classifierNode != null)
                {
                    var pg = from n in _context.ClassifierNodes
                             join g in _context.ProductGroups on n.EntityId equals g.Id
                             where n.Path.StartsWith(classifierNode.Path)
                             select g;

                    query = from i in query
                            where i.Product.ProductGroups.Any(x => pg.Select(g=>g.Id).Contains(x.Id))
                            select i;

                    //query = from i in query
                    //        join pig in _context.ProductInGroups on i.Product.Id equals pig.ProductId
                    //        join pg in _context.ProductGroups on pig.ProductGroupId equals pg.Id
                    //        join c in _context.ClassifierNodes on pg.Id equals c.EntityId
                    //        where c.Path.StartsWith(classifierNode.Path)
                    //        select i;
                }
            }

            if (@params.Selling.HasValue)
            {
                query = from i in query
                        where (i.Offer.PriceOpt20.HasValue && i.Offer.PriceOpt20.Value > @params.Selling.Value)
                        select i;
            } 

            if (!string.IsNullOrWhiteSpace(@params.FreeText))
            {
                query = from i in query
                        join l in _context.ProductLocalizations on i.Product.Id equals l.ProductId
                        where (l.Title.Contains(@params.FreeText) || l.Description.Contains(@params.FreeText) || i.Product.VendorCode.Contains(@params.FreeText))
                        select i;
            }

            var totalCount = query.Select(i => i.Product)
                .Distinct()
                .LongCount();

            var items = _context.Products
                .Include(p => p.ProductGroups)
                .Include(p => p.Localization)
                .Include(p => p.Vendor)
                .Include(p => p.Vendor.Localization)
                .Include(p => p.Offers)
                .Include(p => p.Offers.Select(o => o.Unit))
                .Include(p => p.Offers.Select(o => o.Unit.Localization))
                .Include(p => p.MediaCollection)
                .Include(p => p.MediaCollection.Select(m => m.MediaItem))
                .Where(p => query.Select(i => i.Product.Id).Contains(p.Id));

            switch (@params.OrderBy)
            {
                case "Price":
                    items = items.Order(i => i.Offers.Max(o => o.Price), @params.OrderDir.Equals("ASC", StringComparison.InvariantCultureIgnoreCase));
                    break;
                case "Code":
                    items = items.Order(i => i.VendorCode, @params.OrderDir.Equals("ASC", StringComparison.InvariantCultureIgnoreCase));
                    break;
                case "Title":
                    items = items.Order(i => i.Localization.FirstOrDefault().Title, @params.OrderDir.Equals("ASC", StringComparison.InvariantCultureIgnoreCase));
                    break;
                case "Order":
                    items = items.OrderByDescending(i => i.Order).ThenBy(i => i.Offers.Max(o => o.Price));
                    break;
                default:
                    if (@params.OrderDir.Equals("ASC", StringComparison.InvariantCultureIgnoreCase))
                    {
                        items = items.OrderByDescending(i => i.Order).ThenBy(i => i.Offers.Max(o => o.Price));
                    }
                    else
                    {
                        items = items.OrderByDescending(i => i.Order).ThenByDescending(i => i.Offers.Max(o => o.Price));
                    }
                    break;
            }

            var products = totalCount > @params.PageSize ?
                items.Skip(@params.PageSize * @params.PageIndex).Take(@params.PageSize) :
                items;

            return new SelectResult<Product>(products.ToList(), totalCount, @params.PageIndex, @params.PageSize);
        }

        /// <summary>
        /// Поиск товара.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        public bool TryFindProduct(long id, out Product product, out ICollection<ClassifierNode> nodes, out Product next, out Product prev)
        {
            var query = _context.Products
                .Include(p => p.Localization)
                .Include(p => p.Offers)
                .Include(p => p.Offers.Select(o => o.Unit))
                .Include(p => p.Offers.Select(o => o.Unit.Localization))
                .Include(p => p.Offers.Select(o => o.Properties))
                .Include(p => p.Offers.Select(o => o.Properties.Select(pr => pr.PropertyType)))
                .Include(p => p.Offers.Select(o => o.Properties.Select(pr => pr.PropertyType.Localization)))
                .Include(p => p.MediaCollection)
                .Include(p => p.MediaCollection.Select(m => m.MediaItem))
                .Include(p => p.Properties)
                .Include(p => p.Properties.Select(pr => pr.PropertyType))
                .Include(p => p.Properties.Select(pr => pr.PropertyType.Localization))
                .Include(p => p.ProductGroups)
                .Include(p => p.Vendor)
                .Include(p => p.Vendor.Localization);

            product = query.FirstOrDefault(p => p.Id == id);
            nodes = null;

            if (product != null)
            {
                var pid = product.ProductGroups.Select(pg => pg.Id);
                var vid = product.VendorId;
                var price = product.Offers.Count() > 0 ? product.Offers.Max(o => o.Price) : 0;

                query = query.Where(p => p.VendorId == vid && pid.Any(pids => pids == p.ProductGroups.FirstOrDefault().Id));

                nodes = _context.ClassifierNodes.Where(c => pid.Any(pids => pids == c.EntityId)).ToList();
                //next = query.Where(i => i.Offers.Max(o => o.Price) > price).Order(i => i.Offers.Max(o => o.Price)).FirstOrDefault();
                //prev = query.Where(i => i.Offers.Max(o => o.Price) < price).OrderByDescending(i => i.Offers.Max(o => o.Price)).FirstOrDefault();
                next = prev = null;
            }
            else
            {
                next = prev = null;
            }

            return product != null;
        }

        public void AddProduct(ProductGroupModel productGroupModel
            , string classifierCode = ProductGroup.DefaultClassifierCode)
        {
            var parentNode = _context.ClassifierNodes.Where(n => n.Path == productGroupModel.ParentPath).SingleOrDefault();
            
            var productGroup = _context.ProductGroups.Add(new ProductGroup
            {
                Code = productGroupModel.Code,
                Status = DataModel.EntityStatus.Normal,
                Id = GetId(),
            });

            productGroup.Localization.Add(new DictionaryItemLocalization
            {
                CultureCode = ResourceManager.DefaultCulture.TwoLetterISOLanguageName,
                Title = productGroupModel.Title,
                Description = productGroupModel.Description,
            });

            if (parentNode == null)
            {
                //корневой узел
                productGroup.SetFlags(ProductGroupFlag.Abstract);
            }

            //Главный классификатор. Должен быть определен конфигурацией сайта в базе (supplier)
            //TODO: Брать код из базы
            var categoryClassifier = _context.Classifiers.Where(c => c.Code == ProductGroup.DefaultClassifierCode).SingleOrDefault();

            var node = _context.ClassifierNodes.Add(new ClassifierNode()
            {
                Classifier = categoryClassifier,
                EntityId = productGroup.Id,
                Id = IdGenerator.GetId(),
                Status = EntityStatus.Normal,

            });
            node.SetParent(parentNode);

            productGroupModel.Id = node.Id;
            productGroupModel.Path = node.Path;

            _context.SaveChanges();
        }


        public void AddProductGroupClassifier(ProductGroupModel productGroupModel
            , string classifierCode = ProductGroup.DefaultClassifierCode)
        {
            var parentNode = _context.ClassifierNodes.Where(n => n.Path == productGroupModel.ParentPath).SingleOrDefault();
            var productGroup = _context.ProductGroups.Add(new ProductGroup
            {
                Code = productGroupModel.Code,
                Status = DataModel.EntityStatus.Normal,
                Id = GetId(),

            });

            productGroup.Localization.Add(new DictionaryItemLocalization
            {
                CultureCode = ResourceManager.DefaultCulture.TwoLetterISOLanguageName,
                Title = productGroupModel.Title,
                Description = productGroupModel.Description,
            });

            if (parentNode == null)
            {
                //корневой узел
                productGroup.SetFlags(ProductGroupFlag.Abstract);
            }

            //Главный классификатор. Должен быть определен конфигурацией сайта в базе (supplier)
            //TODO: Брать код из базы
            var categoryClassifier = _context.Classifiers.Where(c => c.Code == ProductGroup.DefaultClassifierCode).SingleOrDefault();

            var node = _context.ClassifierNodes.Add(new ClassifierNode()
            {
                Classifier = categoryClassifier,
                EntityId = productGroup.Id,
                Id = IdGenerator.GetId(),
                Status = EntityStatus.Normal,
                
            });
            node.SetParent(parentNode);
            
            productGroupModel.Id = node.Id;
            productGroupModel.Path = node.Path;

            _context.SaveChanges();
        }

        public static long GetId()
        {
            return IdGenerator.GetId();
        }

        public void UpdateProductGroupClassifier(ProductGroupModel productGroupModel)
        {
            var node = _context.ClassifierNodes.Where(n => n.Path == productGroupModel.Path).SingleOrDefault();

            var group = _context.ProductGroups.Include(p => p.Localization).Where(g => g.Id == node.EntityId).SingleOrDefault();
            if (group != null)
            {
                group.CurrentLocalization.Title = productGroupModel.Title;
                group.CurrentLocalization.Description = productGroupModel.Description;
                node.Code = productGroupModel.Code;
            }

            _context.SaveChanges();

        }

        public void CopyProductGroupClassifier(ProductGroupModel productGroupModel
            , string classifierCode = ProductGroup.DefaultClassifierCode)
        {
            var parentNode = _context.ClassifierNodes.Where(n => n.Path == productGroupModel.ParentPath).SingleOrDefault();
            var copingNode = _context.ClassifierNodes.Find(productGroupModel.Id);
            var copingParentNode = _context.ClassifierNodes.Find(copingNode.GetParentId());

            if (parentNode == copingParentNode)
            {
                //перемещение в рамках одной группы
                var q = from n in _context.ClassifierNodes
                        select n;

                if (parentNode == null)
                {
                    //орудуем рутами
                    q = from q1 in q where q1.Level == 0 select q1;
                }
                else
                {
                    q = from q1 in q where q1.Path.StartsWith(parentNode.Path) && q1.Path != parentNode.Path select q1;
                }

                if (copingNode.Order < productGroupModel.Position)
                {
                    // двигаем вниз
                    q.Where(nd => nd.Order <= productGroupModel.Position && nd.Order > copingNode.Order).ToList().ForEach(n =>
                        n.Order = n.Order - 1
                    );
                    copingNode.Order = productGroupModel.Position;
                }
                else
                {
                    q.Where(nd => nd.Order > productGroupModel.Position && nd.Order <= copingNode.Order).ToList().ForEach(n =>
                        n.Order = n.Order + 1
                    );
                    copingNode.Order = productGroupModel.Position + 1;
                }

                _context.SaveChanges();
                return;
            }

            //Главный классификатор. Должен быть определен конфигурацией сайта в базе (supplier)
            //TODO: Брать код из базы
            var categoryClassifier = _context.Classifiers.Where(c => c.Code == ProductGroup.DefaultClassifierCode).SingleOrDefault();

            var node = _context.ClassifierNodes.Add(new ClassifierNode()
            {
                Classifier = categoryClassifier,
                EntityId = copingNode.EntityId,
                Id = IdGenerator.GetId(),
                Status = EntityStatus.Normal,

            });
            node.SetParent(parentNode);

            productGroupModel.Id = node.Id;
            productGroupModel.Path = node.Path;

            _context.SaveChanges();
        }
        
        public void DelProductGroupClassifier(ProductGroupModel productGroupModel
            , string classifierCode = ProductGroup.DefaultClassifierCode)
        {
            var node = _context.ClassifierNodes.Where(n => n.Path == productGroupModel.Path).SingleOrDefault();

            if (_context.ClassifierNodes.Where(n => n.EntityId == node.EntityId).Count() == 1)
            {
                var group = _context.ProductGroups
                    .Include(p => p.Localization)
                    .Where(g => g.Id == node.EntityId)
                    .SingleOrDefault();

                if (group != null)
                {
                    group.Localization.Clear();
                    //for (int i = 0; i < group.Localization.Count(); i++)
                    //{
                    //    var loc = group.Localization.ElementAt(i);
                    //    _context.DictionaryItemLocalizations.Remove(loc);
                    //}
                    _context.ProductGroups.Remove(group);
                }
            }

            _context.ClassifierNodes.Remove(node);

            _context.SaveChanges();
        }

        public PageContent GetContent(string id)
        {
            return _context.PageContents.Where(c => c.HtmlId.ToLower() == id.ToLower()).FirstOrDefault();
        }

        public ProductGroupModel GetProductGroupByPath(string path)
        {
            var query = from c in _context.Classifiers
                        join n in _context.ClassifierNodes on c.Id equals n.ClassifierId
                        join g in _context.ProductGroups on n.EntityId equals g.Id
                        join l in _context.DictionaryItemLocalizations on g.Id equals l.DictionaryItemId
                        where (n.Path == path)
                            && (c.ClassifierType == 1)
                            && (c.Status == EntityStatus.Normal)
                            && (n.Status == EntityStatus.Normal)
                            && (g.Status == EntityStatus.Normal)
                        select new ProductGroupModel
                        {
                            Code = n.Code,
                            Path = n.Path,
                            Title = l.Title,
                            Description = l.Description
                        };

            return query.SingleOrDefault();
        }

        public void SetupSubscriber(Subscriber model)
        {
            var subscriber = _context.Subscribers.Where(s => s.Email == model.Email).FirstOrDefault();

            if (subscriber == null)
            {
                _context.Subscribers.Add(new Subscriber
                {
                    Created = DateTime.UtcNow,
                    Email = model.Email,
                    Name = model.Name,
                    Id = IdGenerator.GetId(),
                });

                _context.SaveChanges();
            } //возможно сделать else для смены статуса (отписка)
        }

        public void SendOrderItemsBySuppliers(Order order, string FromEmail, string ToEmail = null)
        {
            var full_order = _context.Orders
                .Include(o => o.Items)
                .Include(o => o.Items.Select(x => x.Offer))
                .Include(o => o.Items.Select(x => x.Offer.Properties))
                .Include(o => o.Items.Select(x => x.Offer.Properties.Select(l => l.PropertyType)))
                .Include(o => o.Items.Select(x => x.Offer.Properties.Select(l => l.PropertyType.Localization)))
                .Include(o => o.Items.Select(x => x.Offer.Product))
                .Include(o => o.Items.Select(x => x.Offer.Product.Localization))
                .First(o => o.Id == order.Id);

            var groupByVendor = full_order.Items.GroupBy(x => x.Offer.Product.VendorId);

            foreach (IGrouping<long?, OrderItem> item in groupByVendor)
            {
                var vendor = _context.Subjects
                    .Include(x=> x.Localization)
                    .First(x=> x.Id == item.Key.Value);

                if (vendor.Email != null)
                {
                    var mailTemplate = new VendorMailTemplate(vendor, item.ToList());
                    var mail = new MailQueue
                    {
                        Body = mailTemplate.Execute(),
                        Created = DateTime.UtcNow,
                        FromEmail = FromEmail,
                        IsBodyHtml = true,
                        Priority = 0,
                        Status = 0,
                        Subject = string.Format("Просим выставить счет к заказу {0}", order.Id),
                        ToEmail = ToEmail ?? vendor.Email
                    };
                    MailController.SendMail(mail);
                }
            }
        }

        public Comment GetComment(int id)
        {
            return _context.Comments.OrderByDescending(c => c.Created).FirstOrDefault(c => c.Id == id);
        }

        public SelectResult<Comment> GetComments(int? page)
        {
            int pi = 0;
            int ps = 10;

            if (page.HasValue)
            {
                pi = page.Value -1;
            }
            
            return new SelectResult<Comment>(_context.Comments.OrderByDescending(c => c.Created).Skip(pi * ps).Take(ps).ToList(), _context.Comments.Count(), pi, ps);
        }

        public int AddComment(Comment comment)
        {
            comment.Id = IdGenerator.GetId();
            
            _context.Comments.Add(comment);

            return _context.SaveChanges();
        }

        public void FireSid(Guid sid)
        {
            var sidrow = _context.Sids.Where(s => s.Sid == sid).FirstOrDefault();
            if (sidrow == null)
            {
                sidrow = CreateSid(sid);
            }

            sidrow.Status = sidrow.Status | EntityStatus.Used;

            _context.SaveChanges();
        }

        public SidEntity CreateSid(Guid sid, int type = 0, long? entityId = null, string data = null)
        {
            var sidrow = _context.Sids.Add(new SidEntity
            {
                Status = EntityStatus.Normal,
                Type = type,
                Data = data,
                EntityId = entityId,
                Id = IdGenerator.GetId(),
                Sid = sid,
                Created = DateTime.UtcNow,
            });

            return sidrow;
        }


    }




}
