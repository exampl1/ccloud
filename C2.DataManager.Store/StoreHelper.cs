﻿using C2.DataModel.StoreModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataManager.Store
{
    public static class StoreHelper
    {
        /// <summary>
        /// Получение предложения с минимальной ценой.
        /// </summary>
        /// <param name="offers"></param>
        /// <returns></returns>
        public static Offer GetMinOffer(this ICollection<Offer> offers)
        {
            if ((offers == null) || (offers.Count == 0))
            {
                return new Offer
                {
                    AvailQuantity = 0,
                    Price = 0,
                    PriceOpt40 = 0,
                };
            }

            if (offers.Count == 1)
            {
                return offers.First();
            }

            Offer minOffer = null;
            foreach (var offer in offers)
            {
                if (minOffer == null)
                {
                    minOffer = offer;
                }
                else
                {
                    if (minOffer.Price > offer.Price)
                    {
                        minOffer = offer;
                    }
                }
            }

            return minOffer;
        }
    }
}
