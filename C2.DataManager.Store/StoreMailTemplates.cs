﻿using C2.AppController;
using C2.DataModel;
using C2.DataModel.StoreModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataManager.Store
{
    public class VendorMailTemplate : BaseMailTemplate
    {
        Subject _subject; List<OrderItem> _items;
        int _index = 0;

        public VendorMailTemplate(Subject subject, List<OrderItem> items)
            : base("VendorOrder")
        {
            _subject = subject;
            _items = items;
        }

        protected override string GetValue(string name)
        {
            switch (name)
            {
                case "VendorName":
                    return _subject.CurrentLocalization.Name;
            }

            throw new ArgumentException();
        }

        protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
        {
            _index++;
            return new OrderItemMailTemplate(model as OrderItem, _index);
        }

        protected override IEnumerable GetEnumeration(string name)
        {
            switch (name)
            {
                case "Items":
                    return _items;
            }

            throw new ArgumentException();
        }

    }

    class OrderItemMailTemplate : BaseMailTemplate
    {
        public OrderItemMailTemplate(OrderItem item, int index)
            : base("VendorOrderItem")
        {
            _item = item;
            _index = index;
        }

        OrderItem _item;

        int _index;

        protected override string GetValue(string name)
        {
            switch (name)
            {
                case "Num":
                    return _index.ToString();
                case "Articul":
                    return _item.Offer.Product.VendorCode;
                case "ProductTitle":
                    return _item.Offer.Product.CurrentLocalization.Title;
                case "Properties":
                    if (_item.Offer.Properties.Count > 0)
                    {
                        StringBuilder builder = new StringBuilder("<ul>");
                        foreach (var p in _item.Offer.Properties)
                        {
                            builder.AppendFormat("<li>{0} {1}</li>", p.PropertyType.CurrentLocalization.Title, p.StringValue);
                        }
                        builder.Append("</ul>");
                        return builder.ToString();
                    }
                    else
                    {
                        return "";
                    }
                case "Quantity":
                    return _item.Quantity.ToString("N");
            }

            throw new ArgumentException();
        }

        protected override IEnumerable GetEnumeration(string name)
        {
            throw new NotImplementedException();
        }

        protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
        {
            throw new NotImplementedException();
        }
    }
}
