﻿using C2.DataMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.AppModel;
using C2.DataModel;
using C2.AppController;
using C2.DAL;

namespace C2.DataManager
{
    public class AppDataManager
    {
        public static IdGenerator IdGenerator;

        public static long GetId()
        {
            return IdGenerator.GetId();
        }

        static AppDataManager()
        {
            IdGenerator = IdGenerator.CreateIdentityGenerator(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString, "dbo.UIdTable");
        }

        public AppDataManager(AppContext context)
        {
            _context = context;
        }

        AppContext _context;

        /// <summary>
        /// Получение классификаторов проекта
        /// </summary>
        /// <param name="classifierCode"></param>
        /// <param name="root"></param>
        /// <param name="maxLevel"></param>
        /// <returns></returns>
        public TreeModel GetClassifier(string root = null,
            int maxLevel = -1)
        {

            var query = from c in _context.Classifiers
                        join n in _context.ClassifierNodes on c.Id equals n.ClassifierId
                        where 
                               (c.ClassifierType == 1)
                            && (c.Status == EntityStatus.Normal)
                            && (n.Status == EntityStatus.Normal)
                        select new { Classifier = c, Node = n };

            if (root != null)
            {
                query = from d in query
                        where d.Node.Path.StartsWith(root)
                        select d;
            }

            if (maxLevel > -1)
            {
                query = from d in query
                        where d.Node.Level <= maxLevel
                        select d;
            }

            query = from d in query
                    orderby d.Node.Level, d.Node.Order
                    select d;

            Dictionary<long, TreeNodeModel> nodes = new Dictionary<long, TreeNodeModel>();
            List<TreeNodeModel> roots = new List<TreeNodeModel>();
            foreach (var item in query)
            {
                if (nodes.ContainsKey(item.Node.Id))
                {
                    // пропускаем повторения (если есть)
                    continue;
                }

                TreeNodeModel node = new TreeNodeModel(item.Node.Id, item.Node.Path);
                nodes.Add(node.NodeId, node);

                TreeNodeModel parent;
                if (nodes.TryGetValue(item.Node.GetParentId(), out parent))
                {
                    parent.AddNode(node);
                }
                else
                {
                    roots.Add(node);
                }
            }

            return new TreeModel()
            {
                AllNodes = nodes.Values.ToList(),
                Roots = roots
            };
        }

        public IQueryable<PageContent> GetContentPages()
        {
            var query = from c in _context.PageContents
                        where
                            c.SupplierId == SupplierModel.Current.SubjectId
                        select c;
            
            return query;
        }

        public bool AddSupplier(Subject supplier)
        {
            var sup = _context.Subjects.Where(s => s.Code == supplier.Code).FirstOrDefault();

            if (sup == null)
            {
                sup = _context.Subjects.Add(new Subject
                {
                    Id = GetId(),
                    Code = supplier.Code,
                    Status = EntityStatus.Normal,
                    SubjectType = SubjectType.Organization,
                    Email = supplier.Email,
                    Phone = supplier.Phone,
                });

                sup.Localization.Add(new SubjectLocalization
                {
                    CultureCode = "ru-RU",
                    Description = supplier.CurrentLocalization.Description,
                    FullName = supplier.CurrentLocalization.FullName,
                    ShortName = supplier.CurrentLocalization.ShortName,
                    Name = supplier.CurrentLocalization.Name,
                });

                _context.SaveChanges();
                return true;

            }
            
            return false;
        }

        public void SaveContentPage(PageContent page)
        {
            if (page.Id > 0)
            {
                var _page = _context.PageContents.Find(page.Id);
                _page.HtmlTitle = page.HtmlTitle;
                _page.HtmlKeywords = page.HtmlKeywords;
                _page.HtmlId = page.HtmlId;
                _page.HtmlDescription = page.HtmlDescription;
                _page.Text = page.Text;
                _page.Title = page.Title;
            }
            else
            {
                page.Id = IdGenerator.GetId();
                page.SupplierId = SupplierModel.Current.SubjectId;
                _context.PageContents.Add(page);
            }

            _context.SaveChanges();
        }
    }
}
