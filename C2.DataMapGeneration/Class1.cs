﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace C2.DataMapGeneration
{
    public class Class1
    {
        public static void Main(string[] args) {
            var file = File.OpenText("D:\\Users\\ssysalov\\Documents\\Visual Studio 2012\\Projects\\ComponentsCloud\\ComponentsCloud\\C2.DataMapGeneration\\input.txt");
            foreach (Match m in Regex.Matches(file.ReadToEnd(), @"unsubscribe\\/([\w\-]+)"""))
            {
                if (m.Success)
                {
                    Console.WriteLine("update v set Status=1 from dbo.Visitor v inner join dbo.Sid s on s.EntityId=v.Id and s.Sid='{0}' and v.Status!=1", m.Groups[1]);
                }
            }
        }
    }
}
