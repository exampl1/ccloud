﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel.MediaModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace C2.DataMapping
{
    /// <summary>
    /// Контекст главного приложения
    /// </summary>
    public class AppContext : BaseContext
    {
        static AppContext()
        {
            Database.SetInitializer<AppContext>(null);
        }

        public AppContext()
        {
        }

        public AppContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public AppContext(DbCompiledModel model)
            : base(model)
        {
        }

        public AppContext(string nameOrConnectionString, DbCompiledModel model)
            : base(nameOrConnectionString, model)
        {
        }

        public AppContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
        }

        public AppContext(ObjectContext objectContext, bool dbContextOwnsObjectContext)
            : base(objectContext, dbContextOwnsObjectContext)
        {
        }

        public AppContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection)
            : base(existingConnection, model, contextOwnsConnection)
        {
        }

    }
}
