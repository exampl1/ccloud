﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace C2.DataMapping
{
    public class ClassifierNodeMap : EntityTypeConfiguration<ClassifierNode>
    {
        public ClassifierNodeMap()
        {
            ToTable("ClassifierNode");

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
