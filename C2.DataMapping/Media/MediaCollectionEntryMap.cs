﻿using C2.DataModel.MediaModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataMapping
{
    public class MediaCollectionEntryMap : EntityTypeConfiguration<MediaCollectionEntry>
    {
        public MediaCollectionEntryMap()
        {
            ToTable("MediaCollectionEntry");
            HasKey(t => new { t.CollectionHostId, t.MediaItemId });
        }
    }
}
