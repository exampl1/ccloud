﻿using C2.DataModel.MediaModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace C2.DataMapping
{
    public class MediaItemLocalizationMap : EntityTypeConfiguration<MediaItemLocalization>
    {
        public MediaItemLocalizationMap()
        {
            ToTable("MediaItemLocalization");

            HasKey(t => new { t.MediaItemId, t.CultureCode });
        }
    }
}
