﻿using C2.DataModel.MediaModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace C2.DataMapping
{
    public class MediaItemMap : EntityTypeConfiguration<MediaItem>
    {
        public MediaItemMap()
        {
            ToTable("MediaItem");

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
