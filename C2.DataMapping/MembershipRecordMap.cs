﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataMapping
{
    public class MembershipRecordMap : EntityTypeConfiguration<MembershipRecord>
    {
        public MembershipRecordMap()
        {
            ToTable("webpages_Membership");

            HasKey(t => t.UserId);

            HasRequired(t => t.User);
        }
    }
}
