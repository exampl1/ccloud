﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataMapping
{
    public class OrganizationMap : EntityTypeConfiguration<Organization>
    {
        public OrganizationMap()
        {
            HasKey(t => t.SubjectId);

            Property(t => t.SubjectId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
