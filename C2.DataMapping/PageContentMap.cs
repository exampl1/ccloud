using C2.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataMapping
{
    public class PageContentMap : EntityTypeConfiguration<PageContent>
    {
        public PageContentMap()
        {
            ToTable("PageContent");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasRequired(t => t.Supplier)
                .WithOptional();
        }
    }
}
