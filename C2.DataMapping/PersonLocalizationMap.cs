﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace C2.DataMapping
{
    public class PersonLocalizationMap : EntityTypeConfiguration<PersonLocalization>
    {
        public PersonLocalizationMap()
        {
            HasKey(t => new { t.PersonId, t.CultureCode });
        }
    }
}
