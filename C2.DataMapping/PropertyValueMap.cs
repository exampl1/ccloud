﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;

namespace C2.DataMapping
{
    public class PropertyValueMap : EntityTypeConfiguration<PropertyValue>
    {
        public PropertyValueMap()
        {
            ToTable("PropertyValue");

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
