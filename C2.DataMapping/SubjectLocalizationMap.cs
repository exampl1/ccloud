﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace C2.DataMapping
{
    public class SubjectLocalizationMap : EntityTypeConfiguration<SubjectLocalization>
    {
        public SubjectLocalizationMap()
        {
            ToTable("SubjectLocalization");
            HasKey(t => new { t.SubjectId, t.CultureCode });
        }
    }
}
