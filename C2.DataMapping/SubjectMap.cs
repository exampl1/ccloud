﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace C2.DataMapping
{
    public class SubjectMap : EntityTypeConfiguration<Subject>
    {
        public SubjectMap()
        {
            ToTable("Subject");

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasMany(t => t.SubjectActivities)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("SubjectActivityScope");
                    m.MapLeftKey("SubjectId");
                    m.MapRightKey("ActivityId");
                });
        }
    }
}
