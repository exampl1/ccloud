﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataMapping
{
    public class UserProfileMap : EntityTypeConfiguration<UserProfile>
    {
        public UserProfileMap()
        {
            ToTable("User");

            HasKey(t => t.UserId);

            Property(p => p.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasOptional(t => t.Affiliate)
                .WithRequired(t => t.User);
        }
    }
}
