﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;

namespace C2.DataMapping
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Affiliate> Affiliates { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<MembershipRecord> MembershipRecords { get; set; }
        public DbSet<SidEntity> Sids { get; set; }
        public DbSet<Visitor> Visitors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations
                .Add(new SidMap())
                .Add(new AffiliateMap())
                .Add(new MembershipRecordMap())
                .Add(new UserProfileMap());

        }
    }

}
