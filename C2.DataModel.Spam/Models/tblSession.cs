using System;
using System.Collections.Generic;

namespace C2.DataModel.Spam.Models
{
    public partial class tblSession
    {
        public string Session { get; set; }
        public Nullable<System.Guid> UserId { get; set; }
        public Nullable<System.Guid> ActivatorId { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ExpiredDate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public short Type { get; set; }
        public string Data { get; set; }
        public Nullable<int> DataId { get; set; }
        public Nullable<System.DateTime> LastActiveDate { get; set; }
        public string IP { get; set; }
        public byte[] Version { get; set; }
    }
}
