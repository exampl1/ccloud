using System;
using System.Collections.Generic;

namespace C2.DataModel.Spam.Models
{
    public partial class tblVisitor
    {
        public tblVisitor()
        {
        }

        public int VisitorId { get; set; }
        public string Email { get; set; }
        public Nullable<int> CityId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<bool> Gender { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int Status { get; set; }
    }
}
