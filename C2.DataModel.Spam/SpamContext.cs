﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.Spam
{
    public class SpamContext: DbContext
    {
        public SpamContext()
            : base("SpamConnection")
        {
        }

        public DbSet<Models.tblSession> Sids { get; set; }
        public DbSet<Models.tblVisitor> Visitors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations
                .Add(new Models.Mapping.tblSessionMap())
                .Add(new Models.Mapping.tblVisitorMap());

        }
    }
}
