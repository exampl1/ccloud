﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Адрес.
    /// </summary>
    public class Address : IBaseDataEntity
    {
        /// <summary>
        /// Идентифкатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        [Display(Name = "Почтовый индекс")]
        [StringLength(10)]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string PostCode
        {
            get;
            set;
        }

        /// <summary>
        /// Государство.
        /// </summary>
        [Display(Name = "Страна")]
        [StringLength(MaxLengths.Name)]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string Country
        {
            get;
            set;
        }

        /// <summary>
        /// Регион (область).
        /// </summary>
        [Display(Name = "Область/Регион")]
        [StringLength(MaxLengths.Name)]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string Region
        {
            get;
            set;
        }

        /// <summary>
        /// Город.
        /// </summary>
        [Display(Name = "Город")]
        [StringLength(MaxLengths.Name)]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Улица.
        /// </summary>
        [Display(Name = "Улица")]
        [StringLength(100)]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string Street
        {
            get;
            set;
        }

        /// <summary>
        /// Строение.
        /// </summary>
        [Display(Name = "Номер дома")]
        [StringLength(MaxLengths.Name)]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string Building
        {
            get;
            set;
        }

        /// <summary>
        /// Квартира/офис/апартаменты.
        /// </summary>
        [Display(Name = "Номер квартиры/офиса")]
        [StringLength(MaxLengths.Name)]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string Room
        {
            get;
            set;
        }

        /// <summary>
        /// Специальные элементы.
        /// </summary>
        [Display(Name = "Специальные элементы")]
        [StringLength(100)]
        public string Special
        {
            get;
            set;
        }

        /// <summary>
        /// Примечания.
        /// </summary>
        [Display(Name = "Примечания")]
        [StringLength(MaxLengths.Description)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        public string Notes
        {
            get;
            set;
        }

        /// <summary>
        /// Координаты расположения адреса.
        /// </summary>
        public DbGeography Location
        {
            get;
            set;
        }
    }
}
