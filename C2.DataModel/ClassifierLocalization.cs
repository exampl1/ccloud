﻿using C2.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    public class ClassifierLocalization : ILocalization
    {
        [C2Required]
        [C2MaxLength(MaxLengths.CultureCode)]
        public string CultureCode
        {
            get;
            set;
        }

        public long ClassifierId
        {
            get;
            set;
        }

        public Classifier Classifier
        {
            get;
            set;
        }

        [C2Required]
        [C2MaxLength(MaxLengths.Title)]
        [Display(ResourceType = typeof(ClassifierLocalizationMetaData), Name = "TitleDisplayName")]
        public string Title
        {
            get;
            set;
        }

        [C2MaxLength(MaxLengths.Description)]
        [Display(ResourceType = typeof(ClassifierLocalizationMetaData), Name = "DescriptionDisplayName")]
        public string Description
        {
            get;
            set;
        }
    }

    public static class ClassifierLocalizationMetaData
    {
        public static string TitleDisplayName
        {
            get
            {
                return ResourceManager.GetString("ClassifierLocalization.TitleDisplayName");
            }
        }

        public static string DescriptionDisplayName
        {
            get
            {
                return ResourceManager.GetString("ClassifierLocalization.DescriptionDisplayName");
            }
        }
    }
}
