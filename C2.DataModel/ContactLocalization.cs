﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.Globalization;

namespace C2.DataModel
{
    /// <summary>
    /// Локализация контактной информации.
    /// </summary>
    public class ContactLocalization : ILocalization
    {
        /// <summary>
        /// Код культуры.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.CultureCode)]
        public string CultureCode
        {
            get;
            set;
        }

        /// <summary>
        /// Идентифкатор контакта.
        /// </summary>
        public long ContactId
        {
            get;
            set;
        }

        /// <summary>
        /// Контакт.
        /// </summary>
        public Contact Contact
        {
            get;
            set;
        }

        /// <summary>
        /// Название контаката.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.Title)]
        [Display(ResourceType = typeof(ContactLocalizationMetaData), Name = "TitleDisplayName")]
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Примечания к контакту.
        /// </summary>
        [DataType(DataType.MultilineText)]
        [C2MaxLength(MaxLengths.DescriptionS)]
        [Display(ResourceType = typeof(ContactLocalizationMetaData), Name = "DescriptionDisplayName")]
        public string Description
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Метаданные для класса ContactLocalization.
    /// </summary>
    public static class ContactLocalizationMetaData
    {
        /// <summary>
        /// Название свойства Title.
        /// </summary>
        public static string TitleDisplayName
        {
            get
            {
                return ResourceManager.GetString("Contact.TitleDisplayName");
            }
        }

        /// <summary>
        /// Название свойства Description.
        /// </summary>
        public static string DescriptionDisplayName
        {
            get
            {
                return ResourceManager.GetString("Contact.DescriptionDisplayName");
            }
        }
    }
}
