﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.Globalization;

namespace C2.DataModel
{
    /// <summary>
    /// Справочник.
    /// </summary>
    public class DictionaryItem : IBaseDataEntity,
        ILocalizable<DictionaryItemLocalization>
    {
        public DictionaryItem()
        {
            Localization = new List<DictionaryItemLocalization>();
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Код. Используется для программного поиска элемента словаря.
        /// </summary>
        /// <remarks>
        /// Сюда должны помещаться уникальные (в пределах одного типа словаря) коды элемента. Этот код указывается
        /// явным образом и служит для программного поиска конктретного элемента, т.к. идентфикатор может быть "плавающим".
        /// </remarks>
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        /// Контекстно-значимые битовые признаки (флаги).
        /// </summary>
        public int Flags
        {
            get;
            set;
        }

        protected bool HasFlag(int mask)
        {
            return (Flags & mask) == mask;
        }

        protected void SetFlag(int mask, bool value)
        {
            if (value)
            {
                Flags |= mask;
            }
            else
            {
                Flags &= ~mask;
            }
        }

        /// <summary>
        /// Контекстно-значимые данные.
        /// </summary>
        /// <remarks>
        /// Как правило, это некоторые сериализованные данные.
        /// </remarks>
        public string Data
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация.
        /// </summary>
        public ICollection<DictionaryItemLocalization> Localization
        {
            get;
            private set;
        }

        [NotMapped]
        public DictionaryItemLocalization CurrentLocalization
        {
            get
            {
                return this.GetLocalization();
            }
        }
    }
}
