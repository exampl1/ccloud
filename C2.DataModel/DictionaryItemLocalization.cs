﻿using C2.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Локализация словаря.
    /// </summary>
    public class DictionaryItemLocalization : ILocalization
    {
        /// <summary>
        /// Код культуры.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.CultureCode)]
        public string CultureCode
        {
            get;
            set;
        }

        /// <summary>
        /// Идентфикатор элемента словаря.
        /// </summary>
        public long DictionaryItemId
        {
            get;
            set;
        }

        /// <summary>
        /// Элемент словаря.
        /// </summary>
        public DictionaryItem DictionaryItem
        {
            get;
            set;
        }

        /// <summary>
        /// Заголовок.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.Title)]
        [Display(ResourceType = typeof(DictionaryItemLocalizationMetadataProvider), Name = "TitleDisplayName")]
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Описание.
        /// </summary>
        [Display(ResourceType = typeof(DictionaryItemLocalizationMetadataProvider), Name = "DescriptionDisplayName")]
        public string Description
        {
            get;
            set;
        }
    }

    public class DictionaryItemLocalizationMetadataProvider
    {
        public static string TitleDisplayName
        {
            get
            {
                return ResourceManager.GetString("DictionaryItemLocalization.TitleDisplayName");
            }
        }

        public static string DescriptionDisplayName
        {
            get
            {
                return ResourceManager.GetString("DictionaryItemLocalization.DescriptionDisplayName");
            }
        }
    }
}
