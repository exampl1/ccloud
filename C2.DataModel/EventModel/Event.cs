﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace BaseDataModel.EventModel
//{
//    public class Event : BaseObject,
//        ILocalizableObject<EventLocalization>,
//        ITaggable
//    {
//        public Event()
//        {
//            Localizations = new List<EventLocalization>();
//            AvailableRoleTypes = new List<EventParticipantRoleType>();
//        }

//        public long EventTypeId
//        {
//            get;
//            set;
//        }

//        public EventType EventType
//        {
//            get;
//            set;
//        }

//        public DateTime StartDate
//        {
//            get;
//            set;
//        }

//        public DateTime EndDate
//        {
//            get;
//            set;
//        }

//        public List<EventLocalization> Localizations
//        {
//            get;
//            private set;
//        }

//        // Many-To-Many
//        public List<EventParticipantRoleType> AvailableRoleTypes
//        {
//            get;
//            private set;
//        }

//        public string Tags
//        {
//            get;
//            set;
//        }
//    }

//    public class EventType : Dictionary
//    {
//        public const string DescriminatorCode = "EventType";
//    }

//    public class EventLocalization : ObjectLocalization
//    {
//        public string Name
//        {
//            get;
//            set;
//        }

//        public string Description
//        {
//            get;
//            set;
//        }
//    }
//}
