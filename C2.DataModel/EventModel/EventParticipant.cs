﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace BaseDataModel.EventModel
//{
//    public class EventParticipant : BaseObject
//    {
//        public EventParticipant()
//        {
//            Roles = new List<EventParticipantRole>();
//        }

//        public long EventId
//        {
//            get;
//            set;
//        }

//        public Event Event
//        {
//            get;
//            set;
//        }

//        public long SubjectId
//        {
//            get;
//            set;
//        }

//        public Subject Subject
//        {
//            get;
//            set;
//        }

//        public List<EventParticipantRole> Roles
//        {
//            get;
//            private set;
//        }
//    }

//    public class EventParticipantRoleType : Dictionary
//    {
//        public const string DescriminatorCode = "EventParticipantRoleType";
//    }

//    public class EventParticipantRole
//    {
//        public long ParticipantId
//        {
//            get;
//            set;
//        }

//        public EventParticipant Participant
//        {
//            get;
//            set;
//        }

//        public long RoleTypeId
//        {
//            get;
//            set;
//        }

//        public EventParticipantRoleType RoleType
//        {
//            get;
//            set;
//        }
//    }
//}
