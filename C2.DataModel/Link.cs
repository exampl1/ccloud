﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Связи между объектами.
    /// </summary>
    public class Link : IBaseDataEntity
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа связи.
        /// </summary>
        public long LinkTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Тип связи.
        /// </summary>
        public LinkType LinkType
        {
            get;
            set;
        }

        /// <summary>
        /// Идентфикатор первичной сущности.
        /// </summary>
        public long PrimaryEntityId
        {
            get;
            set;
        }

        /// <summary>
        /// Идентфикатор связанной сущности.
        /// </summary>
        public long SecondaryEntityId
        {
            get;
            set;
        }

        /// <summary>
        /// Порядковая позиция по отношению к другим связям.
        /// </summary>
        public int Order
        {
            get;
            set;
        }
    }
}
