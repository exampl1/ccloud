﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Очередь отправки почтовых сообщений.
    /// </summary>
    public class MailQueue
    {
        /// <summary>
        /// Идентифкатор сообщения.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Адрес получателя.
        /// </summary>
        public string ToEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Приоритет сообщения.
        /// </summary>
        public byte? Priority
        {
            get;
            set;
        }

        /// <summary>
        /// Тип сообщения.
        /// </summary>
        public int? Type
        {
            get;
            set;
        }

        /// <summary>
        /// Тема сообщения.
        /// </summary>
        public string Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Тело сообщения.
        /// </summary>
        public string Body
        {
            get;
            set;
        }

        /// <summary>
        /// Статус.
        /// </summary>
        public byte Status
        {
            get;
            set;
        }

        /// <summary>
        /// Адрес отправителя.
        /// </summary>
        public string FromEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Признак, что тело сообщения имеет формат HTML.
        /// </summary>
        public bool IsBodyHtml
        {
            get;
            set;
        }

        /// <summary>
        /// Дата создания.
        /// </summary>
        public DateTime Created
        {
            get;
            set;
        }

        /// <summary>
        /// Дата отправки.
        /// </summary>
        public DateTime? Sent
        {
            get;
            set;
        }
    }
}
