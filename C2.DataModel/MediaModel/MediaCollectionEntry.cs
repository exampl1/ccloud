﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.MediaModel
{
    /// <summary>
    /// Элемент медиа-коллекции.
    /// </summary>
    public class MediaCollectionEntry
    {
        /// <summary>
        /// Идентификатор коллекции-владельца.
        /// </summary>
        public long CollectionHostId
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор медиа-элемента.
        /// </summary>
        public long MediaItemId
        {
            get;
            set;
        }

        /// <summary>
        /// Медиа-элемент.
        /// </summary>
        public MediaItem MediaItem
        {
            get;
            set;
        }

        /// <summary>
        /// Порядковый номер в коллекции.
        /// </summary>
        public int OrderNum
        {
            get;
            set;
        }
    }
}
