﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.MediaModel
{
    public class MediaItem : IBaseDataEntity,
        ILocalizable<MediaItemLocalization>
    {
        public MediaItem()
        {
            Localization = new List<MediaItemLocalization>();
        }

        public long Id
        {
            get;
            set;
        }

        public EntityStatus Status
        {
            get;
            set;
        }

        public MediaType MediaType
        {
            get;
            set;
        }

        public string MediaProvider
        {
            get;
            set;
        }

        public ICollection<MediaItemLocalization> Localization
        {
            get;
            private set;
        }

        [NotMapped]
        public MediaItemLocalization CurrentLocalization
        {
            get
            {
                return this.GetLocalization();
            }
        }

        [MaxLength(MaxLengths.Url)]
        public string PermaLink
        {
            get;
            set;
        }

        [MaxLength]
        public string Data
        {
            get;
            set;
        }
    }

    public static class MediaItemHelper
    {
        public static List<PictureMetaData> GetPictureMetaData(this MediaItem media)
        {
            if (string.IsNullOrWhiteSpace(media.Data))
            {
                return null;
            }
            return JsonConvert.DeserializeObject<List<PictureMetaData>>(media.Data);
        }

        public static void SetPictureMetaData(this MediaItem media, List<PictureMetaData> metaData)
        {
            if ((metaData == null) || (metaData.Count == 0))
            {
                media.Data = null;
            }
            else
            {
                media.Data = JsonConvert.SerializeObject(metaData);
            }
        }
    }
}
