﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Организация.
    /// </summary>
    public class Organization : ILocalizable<OrganizationLocalization>
    {
        public Organization()
        {
            Localization = new List<OrganizationLocalization>();
        }

        /// <summary>
        /// Идентфикатор базового субъекта.
        /// </summary>
        public long SubjectId
        {
            get;
            set;
        }

        /// <summary>
        /// Базовый субъект.
        /// </summary>
        public Subject Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Идентфикатор формы собственности.
        /// </summary>
        public long OwnershipTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Форма собственности.
        /// </summary>
        public OrganizationOwnershipType OwnershipType
        {
            get;
            set;
        }

        /// <summary>
        /// Дата основания.
        /// </summary>
        public DateTime? FoundationDate
        {
            get;
            set;
        }

        /// <summary>
        /// Дата закрытия.
        /// </summary>
        public DateTime? ClosingDate
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация.
        /// </summary>
        public ICollection<OrganizationLocalization> Localization
        {
            get;
            private set;
        }

        [NotMapped]
        public OrganizationLocalization CurrentLocalization
        {
            get
            {
                return this.GetLocalization();
            }
        }
    }
}
