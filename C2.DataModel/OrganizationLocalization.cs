﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Локализация информации об организации.
    /// </summary>
    public class OrganizationLocalization : ILocalization
    {
        /// <summary>
        /// Код культуры локализации.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.CultureCode)]
        public string CultureCode
        {
            get;
            set;
        }

        /// <summary>
        /// Идентфикатор организации.
        /// </summary>
        public long OrganizationId
        {
            get;
            set;
        }

        /// <summary>
        /// Организация.
        /// </summary>
        public Organization Organization
        {
            get;
            set;
        }
    }
}
