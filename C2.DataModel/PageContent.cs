using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace C2.DataModel
{
    public class PageContent
    {
        public PageContent()
        {
        }

        public long Id { get; set; }
        
        public string Text { get; set; }
        public string HtmlTitle { get; set; }
        public string HtmlDescription { get; set; }
        public string HtmlKeywords { get; set; }
        public string HtmlId { get; set; }
        public string Title { get; set; }
        public string ContentType { get; set; }
        public long? SupplierId { get; set; }
        public Supplier Supplier { get; set; }
    }
}
