﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Пол человека.
    /// </summary>
    public enum PersonGender
    {
        /// <summary>
        /// Неопределённый.
        /// </summary>
        Undefined,

        /// <summary>
        /// Мужчина.
        /// </summary>
        Male,

        /// <summary>
        /// Женщина.
        /// </summary>
        Female,

        /// <summary>
        /// Другое.
        /// </summary>
        Other
    }
}
