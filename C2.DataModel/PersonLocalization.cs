﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Локализация информации о человеке.
    /// </summary>
    public class PersonLocalization : ILocalization
    {
        /// <summary>
        /// Код культуры локализации.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.CultureCode)]
        public string CultureCode
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор персоны.
        /// </summary>
        public long PersonId
        {
            get;
            set;
        }

        /// <summary>
        /// Персона.
        /// </summary>
        public Person Person
        {
            get;
            set;
        }

        /// <summary>
        /// Имя.
        /// </summary>
        [C2Required]
        [MaxLength(MaxLengths.Name)]
        public string FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Отчество.
        /// </summary>
        [C2MaxLength(MaxLengths.Name)]
        public string MiddleName
        {
            get;
            set;
        }

        /// <summary>
        /// Фамилия.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.Name)]
        public string LastName
        {
            get;
            set;
        }

        /// <summary>
        /// Звание.
        /// </summary>
        [C2MaxLength(MaxLengths.Name)]
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Прозвище.
        /// </summary>
        [C2MaxLength(MaxLengths.Name)]
        public string AlterName
        {
            get;
            set;
        }
    }
}
