﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    public class PropertyValue
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа свойства.
        /// </summary>
        public long PropertyTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Тип свойства.
        /// </summary>
        public PropertyType PropertyType
        {
            get;
            set;
        }

        public byte? ByteValue
        {
            get;
            set;
        }

        public long? IntValue
        {
            get;
            set;
        }

        public decimal? DecimalValue
        {
            get;
            set;
        }

        public DateTime? DateValue
        {
            get;
            set;
        }

        public string StringValue
        {
            get;
            set;
        }
    }
}
