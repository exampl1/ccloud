﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.SecurityModel
{
    public class Permission
    {
        public long EntityId
        {
            get;
            set;
        }

        public long ActionTypeId
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }
    }
}
