﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace C2.DataModel
{

    public class SidEntity : IBaseDataEntity
    {
        public Guid Sid
        {
            get;
            set;
        }

        public DateTime Created
        {
            get;
            set;
        }

        public long? EntityId
        {
            get;
            set;
        }

        public string Data
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public EntityStatus Status
        {
            get;
            set;
        }

        public long Id
        {
            get;
            set;
        }
    }
}
