﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace C2.DataModel
{
    /// <summary>
    /// Субъект.
    /// </summary>
    public class Subject : IBaseDataEntity, ILocalizable<SubjectLocalization>
    {
        public Subject()
        {
            Localization = new List<SubjectLocalization>();
            SubjectActivities = new List<SubjectActivityType>();
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Уникальный код субъекта.
        /// </summary>
        public string Code
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        /// <summary>
        /// Статус.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Тип субъекта.
        /// </summary>
        public SubjectType SubjectType
        {
            get;
            set;
        }

        /// <summary>
        /// Области деятельности.
        /// </summary>
        public ICollection<SubjectActivityType> SubjectActivities
        {
            get;
            private set;
        }

        /// <summary>
        /// Локализация.
        /// </summary>
        public ICollection<SubjectLocalization> Localization
        {
            get;
            private set;
        }

        [NotMapped]
        public SubjectLocalization CurrentLocalization
        {
            get
            {
                return this.GetLocalization();
            }
        }
    }
}
