﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace C2.DataModel
{
    /// <summary>
    /// Локализация субъекта.
    /// </summary>
    public class SubjectLocalization : ILocalization
    {
        /// <summary>
        /// Код культуры локализации.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.CultureCode)]
        public string CultureCode
        {
            get;
            set;
        }

        /// <summary>
        /// Идентфикатор субъекта.
        /// </summary>
        public long SubjectId
        {
            get;
            set;
        }

        /// <summary>
        /// Субъект.
        /// </summary>
        public Subject Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Краткое имя/название.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.Name)]
        public string ShortName
        {
            get;
            set;
        }

        /// <summary>
        /// Имя/название.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.Name)]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Полное имя/название.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.FullName)]
        public string FullName
        {
            get;
            set;
        }

        /// <summary>
        /// Описание.
        /// </summary>
        [C2MaxLength(MaxLengths.Description)]
        public string Description
        {
            get;
            set;
        }
    }
}
