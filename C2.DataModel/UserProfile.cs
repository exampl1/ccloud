﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace C2.DataModel
{
    [Table("User")]
    public class UserProfile
    {
        [Key]
        public int UserId { get; set; }

        public string UserName { get; set; }

        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Телефон")]
        public string Phone { get; set; }

        public MailFlag? MailFlag { get; set; }

        public MembershipRecord MembershipRecord { get; set; }

        [ForeignKey("UserId")]
        public Affiliate Affiliate { get; set; }
    }

    [Flags]
    public enum MailFlag : byte
    {
        Unsubcribed = 1,
    }
}
