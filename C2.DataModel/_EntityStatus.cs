﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Состояние сущности.
    /// </summary>
    [Flags]
    public enum EntityStatus
    {
        /// <summary>
        /// Сущность не сохранена в БД.
        /// </summary>
        New = 0,

        /// <summary>
        /// Сущность доступна для использования.
        /// </summary>
        Enabled = 1,

        /// <summary>
        /// Сущность не доступна для использования.
        /// </summary>
        Disabled = Enabled << 1,

        /// <summary>
        /// Нормальное.
        /// </summary>
        Normal = Enabled | (Disabled << 1),

        /// <summary>
        /// Скрыто. Просмотр сущности требует специальной процедуры.
        /// </summary>
        Hidden = Enabled | (Normal << 1),

        /// <summary>
        /// Только для чтения. Изменение данных сущности требует специальной процедуры.
        /// </summary>
        ReadOnly = Enabled | (Hidden << 1),

        /// <summary>
        /// Неопубликовано, черновик.
        /// </summary>
        Unpublished = Disabled | (ReadOnly << 1),

        /// <summary>
        /// Удалено.
        /// </summary>
        Removed = Disabled | (Unpublished << 1),

        /// <summary>
        /// Истёк срок достоверности.
        /// </summary>
        Expired = Disabled | (Removed << 1),

        /// <summary>
        /// Сущность не используется, но доступна.
        /// </summary>
        Unused = Disabled | (Expired << 1),

        /// <summary>
        /// Заблокировано.
        /// </summary>
        Blocked = Disabled | (Unused << 1),

        /// <summary>
        /// Использовано.
        /// </summary>
        Used = Disabled | (Blocked << 1),

        /// <summary>
        /// Состояния, в которых сущность доступна пользователям для просмотра без дополнительных условий.
        /// </summary>
        VisibleForEndUser = Normal | ReadOnly | Unused,
    }
}
