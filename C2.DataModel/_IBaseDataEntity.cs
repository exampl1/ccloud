﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace C2.DataModel
{
    /// <summary>
    /// Базовая сущность.
    /// </summary>
    public interface IBaseDataEntity
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        EntityStatus Status
        {
            get;
            set;
        }
    }
}
