﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Сущность, поддерживающая комментирование.
    /// </summary>
    public interface ICommentable
    {
        /// <summary>
        /// Комментарии.
        /// </summary>
        ICollection<Comment> Comments
        {
            get;
        }
    }
}
