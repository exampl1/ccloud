﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Расширяемый объект.
    /// </summary>
    public interface IExpando
    {
        /// <summary>
        /// Свойства объекта.
        /// </summary>
        ICollection<PropertyValue> Properties
        {
            get;
        }
    }
}
