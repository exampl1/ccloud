﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    public class C2DataTypeAttribute : DataTypeAttribute
    {
        public C2DataTypeAttribute(DataType dataType)
            : base(dataType)
        {
            Init();
        }

        public C2DataTypeAttribute(string customDataType)
            : base(customDataType)
        {
            Init();
        }

        void Init()
        {
            ErrorMessageResourceType = typeof(MetaData);
            ErrorMessageResourceName = "DataTypeFormatErrorMessage";
        }
    }
}
