﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    public class C2MaxLengthAttribute : MaxLengthAttribute
    {
        public C2MaxLengthAttribute(int length)
            : base(length)
        {
            ErrorMessageResourceType = typeof(MetaData);
            ErrorMessageResourceName = "MaxLengthErrorMessage";
        }
    }
}
