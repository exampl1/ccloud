﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    public static class MaxLengths
    {
        /// <summary>
        /// (128)
        /// </summary>
        public const int Code = 128;

        /// <summary>
        /// (1000)
        /// </summary>
        public const int Comment = 1000;

        /// <summary>
        /// (250)
        /// </summary>
        public const int Contact = 250;

        /// <summary>
        /// (10)
        /// </summary>
        public const int CultureCode = 10;

        /// <summary>
        /// (1000)
        /// </summary>
        public const int Description = 1000;

        /// <summary>
        /// (200)
        /// </summary>
        public const int DescriptionS = 200;

        /// <summary>
        /// (8000)
        /// </summary>
        public const int DescriptionXL = 8000;

        /// <summary>
        /// (100)
        /// </summary>
        public const int FullName = 100;

        /// <summary>
        /// (50)
        /// </summary>
        public const int Name = 50;

        /// <summary>
        /// (340)
        /// </summary>
        public const int NodePath = 340;

        /// <summary>
        /// (1000)
        /// </summary>
        public const int Title = 1000;

        /// <summary>
        /// (2048)
        /// </summary>
        public const int Url = 2048;
    }
}