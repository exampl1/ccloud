﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.Globalization;

namespace C2.DataModel
{
    /// <summary>
    /// Стандартные соообщения об ошибках.
    /// </summary>
    public class MetaData
    {
        /// <summary>
        /// Сообщение о превышении максимально разрешённой длины поля.
        /// </summary>
        public static string MaxLengthErrorMessage
        {
            get
            {
                return ResourceManager.GetString("MaxLengthErrorMessage");
            }
        }

        /// <summary>
        /// Сообщение об ошибке при отсутствии данных для обязательного поля.
        /// </summary>
        public static string RequiredErrorMessage
        {
            get
            {
                return ResourceManager.GetString("RequiredErrorMessage");
            }
        }

        /// <summary>
        /// Сообщение об ошибке при ошибочном формате данных.
        /// </summary>
        public static string DataTypeFormatErrorMessage
        {
            get
            {
                return ResourceManager.GetString("DataTypeFormatErrorMessage");
            }
        }
    }
}
