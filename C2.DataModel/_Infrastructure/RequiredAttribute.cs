﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    public class C2RequiredAttribute : System.ComponentModel.DataAnnotations.RequiredAttribute
    {
        public C2RequiredAttribute()
            : base()
        {
            ErrorMessageResourceType = typeof(MetaData);
            ErrorMessageResourceName = "RequiredErrorMessage";
        }
    }
}
