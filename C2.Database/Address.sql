﻿CREATE TABLE [dbo].[Address]
(
	[Id] [dbo].[EntityId] NOT NULL,
	[Status] INT NOT NULL,
	[PostCode] NVARCHAR(10) NULL,
	[Country] [dbo].[Name] NULL,
	[Region] [dbo].[Name] NULL,
	[City] [dbo].[Name] NULL,
	[Street] NVARCHAR(100) NULL,
	[Building] [dbo].[Name] NULL,
	[Room] [dbo].[Name] NULL,
	[Special] NVARCHAR(100) NULL,
	[Notes] [dbo].[Description] NULL,
    [Location] [sys].[geography] NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY ([Id])
)

GO