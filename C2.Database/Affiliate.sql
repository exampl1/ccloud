﻿CREATE TABLE [dbo].[Affiliate]
(
	[UserId] INT NOT NULL PRIMARY KEY, 
    [Code] [dbo].[EntityId] NOT NULL, 
    [ParentUserId] INT NULL, 
    [RegistrationCount] INT NOT NULL DEFAULT 0, 
    [BasketCount] INT NOT NULL DEFAULT 0, 
    [BasketSum] DECIMAL NOT NULL DEFAULT 0, 
    [PaidCount] INT NOT NULL DEFAULT 0, 
    [PaidSum] DECIMAL NOT NULL DEFAULT 0, 
    [Amount] DECIMAL NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Affiliate_Parent] FOREIGN KEY ([ParentUserId]) REFERENCES [Affiliate]([UserId]), 
    CONSTRAINT [FK_Affiliate_User] FOREIGN KEY ([UserId]) REFERENCES [User]([UserId])
)
