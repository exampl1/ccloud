﻿CREATE TABLE [dbo].[ClassifierNode]
(
	[Id] [dbo].[EntityId] NOT NULL,
    [Status] INT NOT NULL,
    [ClassifierId] [dbo].[EntityId] NOT NULL,
    [Path] [dbo].[NodePath] NOT NULL,
	[Level] INT NOT NULL,
	[Order] INT NOT NULL,
    [Code] [dbo].[Code] NULL,
    [EntityId] [dbo].[EntityId] NOT NULL,
    CONSTRAINT [PK_ClassifierNode] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ClassifierNode_ToClassifier] FOREIGN KEY ([ClassifierId]) REFERENCES [Classifier]([Id])
)
GO

CREATE INDEX [IX_ClassifierNode_Path] ON [dbo].[ClassifierNode] ([Path])
GO
