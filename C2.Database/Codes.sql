﻿CREATE TABLE [dbo].[Codes]
(
	[Id] VARCHAR(16) NOT NULL PRIMARY KEY, 
    [CreateData] DATETIME NOT NULL DEFAULT getutcdate(), 
	[Discount] tinyInt NOT NULL,
	[Duration] tinyint NOT NULL,
    [Status] INT NOT NULL DEFAULT 0,
	[UserId] INT NULL,
    [ActivationDate] DATETIME NULL, 
    [FinishDate] DATETIME NULL, 
    CONSTRAINT [FK_Codes_User] FOREIGN KEY ([UserId]) REFERENCES dbo.[User]([UserId])
)
