﻿CREATE TABLE [dbo].[Contact]
(
	[Id] [dbo].[EntityId] NOT NULL, 
    [Status] INT NOT NULL, 
	[EntityId] [dbo].[EntityId] NOT NULL, 
	[ContactTypeId] [dbo].[EntityId] NOT NULL,
	[Value] nvarchar(100) NOT NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_Contact_ToDictionaryItem] FOREIGN KEY ([ContactTypeId]) REFERENCES [DictionaryItem]([Id])
)
GO

CREATE INDEX [IX_Contact_EntityId] ON [dbo].[Contact] ([EntityId])