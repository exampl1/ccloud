﻿CREATE TABLE [dbo].[ContactLocalization]
(
	[ContactId] [dbo].[EntityId] NOT NULL , 
    [CultureCode] [dbo].[CultureCode] NOT NULL, 
    [Title] [dbo].[Title] NOT NULL, 
    [Description] [dbo].[Description] NOT NULL, 
    CONSTRAINT [PK_ContactLocalization] PRIMARY KEY ([ContactId], [CultureCode]), 
    CONSTRAINT [FK_ContactLocalization_ToContact] FOREIGN KEY ([ContactId]) REFERENCES [Contact]([Id])
)
