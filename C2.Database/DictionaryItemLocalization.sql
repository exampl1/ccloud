﻿CREATE TABLE [dbo].[DictionaryItemLocalization]
(
	[DictionaryItemId] [dbo].[EntityId] NOT NULL, 
    [CultureCode] [dbo].[CultureCode] NOT NULL, 
    [Title] [dbo].[Title] NOT NULL, 
    [Description] [dbo].[Description] NOT NULL, 
    CONSTRAINT [PK_DictionaryItemLocalization] PRIMARY KEY ([DictionaryItemId], [CultureCode]), 
    CONSTRAINT [FK_DictionaryItemLocalization_ToDictionaryItem] FOREIGN KEY ([DictionaryItemId]) REFERENCES [DictionaryItem]([Id]) 
)
