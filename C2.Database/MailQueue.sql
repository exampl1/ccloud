﻿CREATE TABLE [dbo].[MailQueue]
(
	[Id] BIGINT IDENTITY(1,1) NOT NULL,
	[ToEmail] [nvarchar](256) NOT NULL,
	[ToUserId] INT  NULL,
	[Priority] [tinyint] NULL,
	[Type] [int] NULL,
	[Subject] [nvarchar](1024) NOT NULL,
	[Body] [nvarchar](max) NULL,
	[Status] [tinyint] NOT NULL,
	[FromEmail] [nvarchar](256) NULL,
	[IsBodyHtml] [bit] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Sent] [datetime] NULL,
    CONSTRAINT [PK_MailQueue] PRIMARY KEY ([Id])
)