﻿CREATE TABLE [dbo].[MediaCollectionEntry]
(
	[CollectionHostId] [dbo].[EntityId] NOT NULL,
	[MediaItemId] [dbo].[EntityId] NOT NULL,
    [OrderNum] INT NOT NULL,
	CONSTRAINT [PK_MediaCollectionEntry] PRIMARY KEY ([CollectionHostId], [MediaItemId]),
    CONSTRAINT [FK_MediaCollectionEntry_ToMediaItem] FOREIGN KEY ([MediaItemId]) REFERENCES [MediaItem]([Id])
)
