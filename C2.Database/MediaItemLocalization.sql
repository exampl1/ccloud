﻿CREATE TABLE [dbo].[MediaItemLocalization]
(
	[MediaItemId] [dbo].[EntityId] NOT NULL, 
    [CultureCode] [dbo].[CultureCode] NOT NULL, 
    [Title] [dbo].[Title] NOT NULL, 
    [Description] [dbo].[Description] NULL, 
    CONSTRAINT [PK_MediaItemLocalization] PRIMARY KEY ([MediaItemId], [CultureCode]), 
    CONSTRAINT [FK_MediaItemLocalization_ToMediaItem] FOREIGN KEY ([MediaItemId]) REFERENCES [MediaItem]([Id]) 
)
