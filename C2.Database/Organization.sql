﻿CREATE TABLE [dbo].[Organization]
(
	[SubjectId] [dbo].[EntityId] NOT NULL , 
    [OwnershipTypeId] [dbo].[EntityId] NOT NULL, 
    [FoundationDate] DATE NULL, 
    [ClosingDate] DATE NULL, 
    [INN] [dbo].[Key] NULL, 
    [KPP] [dbo].[Key] NULL, 
    [BIC] [dbo].[Key] NULL, 
    [Account] [dbo].[Key] NULL, 
    [Bank] [dbo].[Key] NULL, 
    [OGRN] [dbo].[Key] NULL, 
    CONSTRAINT [PK_Organization] PRIMARY KEY ([SubjectId]), 
    CONSTRAINT [FK_Organization_ToSubject] FOREIGN KEY ([SubjectId]) REFERENCES [Subject]([Id])
)
