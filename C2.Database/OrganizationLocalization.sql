﻿CREATE TABLE [dbo].[OrganizationLocalization]
(
	[SubjectId] [dbo].[EntityId] NOT NULL , 
    [CultureCode] [dbo].[CultureCode] NOT NULL, 
    CONSTRAINT [PK_OrganizationLocalization] PRIMARY KEY ([SubjectId], [CultureCode]), 
    CONSTRAINT [FK_OrganizationLocalization_ToOrganization] FOREIGN KEY ([SubjectId]) REFERENCES [Organization]([SubjectId]), 
    CONSTRAINT [FK_OrganizationLocalization_ToSubjectLocalization] FOREIGN KEY ([SubjectId], [CultureCode]) REFERENCES [SubjectLocalization]([SubjectId], [CultureCode])
)
