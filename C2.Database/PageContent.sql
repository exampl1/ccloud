﻿CREATE TABLE [dbo].[PageContent]
(
	[Id] [dbo].[EntityId] NOT NULL PRIMARY KEY, 
    [Text] NVARCHAR(MAX) NOT NULL, 
    [HtmlTitle] NVARCHAR(1024) NOT NULL, 
    [HtmlDescription] NVARCHAR(1024) NULL, 
    [HtmlKeywords] NVARCHAR(512) NOT NULL, 
    [HtmlId] NVARCHAR(256) NOT NULL, 
    [Title] NVARCHAR(1024) NOT NULL, 
    [SupplierId] [dbo].[EntityId] NULL, 
    [ContentType] NVARCHAR(256) NULL, 
    CONSTRAINT [FK_PageContent_ToSupplier] FOREIGN KEY ([SupplierId]) REFERENCES Supplier([SubjectId])
)
