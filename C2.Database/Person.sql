﻿CREATE TABLE [dbo].[Person]
(
	[SubjectId] [dbo].[EntityId] NOT NULL, 
    [UserName] [dbo].[Name] NULL, 
    [Gender] TINYINT NOT NULL,
	[BirthDate] DATE NULL,
    [DeathDate] DATE NULL, 
    CONSTRAINT [PK_Person] PRIMARY KEY ([SubjectId]), 
    CONSTRAINT [FK_Person_ToSubject] FOREIGN KEY ([SubjectId]) REFERENCES [Subject]([Id]) 
)

GO

CREATE INDEX [IX_Person_UserName] ON [dbo].[Person] ([UserName])
