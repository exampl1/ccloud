﻿CREATE TABLE [dbo].[PersonLocalization]
(
	[SubjectId] [dbo].[EntityId], 
    [CultureCode] [dbo].[CultureCode] NOT NULL, 
    [FirstName] [dbo].[Name] NOT NULL, 
    [LastName] [dbo].[Name] NOT NULL, 
    [MiddleName] [dbo].[Name] NULL, 
	[Title] [dbo].[Name] NULL, 
    [AlterName] [dbo].[Name] NULL,
    CONSTRAINT [PK_PersonLocalization] PRIMARY KEY ([SubjectId], [CultureCode]),
    CONSTRAINT [FK_PersonLocalization_ToPerson] FOREIGN KEY ([SubjectId]) REFERENCES [Person]([SubjectId]),
    CONSTRAINT [FK_PersonLocalization_ToSubjectLocalization] FOREIGN KEY ([SubjectId], [CultureCode]) REFERENCES [SubjectLocalization]([SubjectId], [CultureCode])
)