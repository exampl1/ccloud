﻿CREATE VIEW [Security].[PermissionView] AS
WITH
	[Enabled] (EntityId, RoleId, ActionTypeId)
	AS (
		SELECT s.EntityId, r.RoleId, r.ActionTypeId
		FROM [Rule] r WITH (NOLOCK)
		INNER JOIN [Security].[ScopeEntity] s WITH (NOLOCK)
			ON (s.ScopeId = r.ScopeId)
		WHERE r.[Enabled] = 1
	),
	[Disabled] (EntityId, RoleId, ActionTypeId)
	AS (
		SELECT s.EntityId, r.RoleId, r.ActionTypeId
		FROM [Rule] r WITH (NOLOCK)
		INNER JOIN [ScopeEntity] s WITH (NOLOCK)
			ON (s.ScopeId = r.ScopeId)
		WHERE r.[Enabled] = 0
	),
	[Permitted] (EntityId, RoleId, ActionTypeId)
	AS (
		SELECT	EntityId, RoleId, ActionTypeId
		FROM	[Enabled]
		EXCEPT
		SELECT	EntityId, RoleId, ActionTypeId
		FROM	[Disabled]
	)
SELECT p.EntityId, p.ActionTypeId, m.UserName
FROM [Permitted] p
JOIN [Membership] m WITH (NOLOCK)
ON m.RoleId = p.RoleId