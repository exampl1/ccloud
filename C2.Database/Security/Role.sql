﻿CREATE TABLE [Security].[Role]
(
	[Id] [dbo].[EntityId] NOT NULL , 
    [OwnerUserName] [dbo].[UserName] NOT NULL, 
    [Name] [dbo].[Name] NOT NULL, 
    [Path] [dbo].[NodePath] NOT NULL, 
    CONSTRAINT [PK_Role] PRIMARY KEY ([Id])
)
