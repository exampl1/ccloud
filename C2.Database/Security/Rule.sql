﻿CREATE TABLE [Security].[Rule]
(
	[ScopeId] [dbo].[EntityId] NOT NULL, 
	[RoleId] [dbo].[EntityId] NOT NULL, 
    [ActionTypeId] [dbo].[EntityId] NOT NULL, 
    [Enabled] BIT NOT NULL, 

    CONSTRAINT [PK_Rule] PRIMARY KEY ([ScopeId], [RoleId], [ActionTypeId]),
	CONSTRAINT [FK_Rule_ToScope] FOREIGN KEY ([ScopeId]) REFERENCES [Security].[Scope]([Id]),
	CONSTRAINT [FK_Rule_ToRole] FOREIGN KEY ([RoleId]) REFERENCES [Security].[Role]([Id]),
	CONSTRAINT [FK_Rule_ToActionType] FOREIGN KEY ([ActionTypeId]) REFERENCES [DictionaryItem]([Id])
)
