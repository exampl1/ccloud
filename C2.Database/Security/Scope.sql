﻿CREATE TABLE [Security].[Scope]
(
	[Id] [dbo].[EntityId] NOT NULL , 
    [OwnerUserName] [dbo].[UserName] NOT NULL, 
    [Name] [dbo].[Name] NOT NULL, 
    [Description] [dbo].[Description] NULL, 
    CONSTRAINT [PK_Scope] PRIMARY KEY ([Id])
)
