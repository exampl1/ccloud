﻿CREATE TABLE [Security].[ScopeEntity]
(
	[ScopeId] [dbo].[EntityId] NOT NULL , 
    [EntityId] [dbo].[EntityId] NOT NULL, 
    CONSTRAINT [PK_ScopeEntity] PRIMARY KEY ([ScopeId], [EntityId]), 
    CONSTRAINT [FK_ScopeEntity_ToScope] FOREIGN KEY ([ScopeId]) REFERENCES [Security].[Scope]([Id])
)
