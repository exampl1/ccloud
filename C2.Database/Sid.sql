﻿CREATE TABLE [dbo].[Sid] (
    [Id]       [dbo].[EntityId] IDENTITY (1, 1) NOT NULL,
    [EntityId] [dbo].[EntityId] NULL,
    [Data]     NVARCHAR (MAX)   NULL,
    [Status]   INT              NOT NULL,
    [Type]     INT              NOT NULL,
    [Sid]      UNIQUEIDENTIFIER NOT NULL,
    [Created]  SMALLDATETIME    NOT NULL,
    CONSTRAINT [PK__Sid__3214EC071CA49B30] PRIMARY KEY CLUSTERED ([Id] ASC)
);



GO

CREATE INDEX [IX_Sid_EntityId] ON [dbo].[Sid] ([EntityId])
go

CREATE INDEX [IX_Sid_Sid] ON [dbo].[Sid] ([Sid])
go
