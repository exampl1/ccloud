﻿CREATE procedure [dbo].[CreateSpamVisitor] 
	@TemplateName nvarchar(127), -- resume_email
	@Flag int = 1, -- 1 - users, 2 - visitors - 3 - both
	@send_cnt int = 1,
	@from_email nvarchar(127) = 'customer.service@optodrom.ru',
	@TestEmail nvarchar(256) = null --'optodrom.ru@yandex.ru'
as
BEGIN	
	SET NOCOUNT ON;
	
	DECLARE @Body nvarchar(max), @Title nvarchar(256);
	
	-- достаем шаблон письма
	SELECT @Body = c.Text, @Title = c.Title
		from dbo.PageContent c with(nolock)		
		where c.HtmlID = @TemplateName;

	Print 'Title of Email: ' + @Title;

	CREATE TABLE #tmpList (
		[Id] int not null,
		[Email] varchar(256) COLLATE Cyrillic_General_CS_AI,
		[Name] varchar(256) COLLATE Cyrillic_General_CS_AI null,
		[Type] int not null
		);

		if (@Flag & 1 > 0) 
		BEGIN
			INSERT INTO #tmpList ([Id], [Email], [Name], [Type])
			SELECT top (@send_cnt) v.UserId, v.UserName, COALESCE(v.FirstName, v.UserName) + CASE WHEN v.FirstName IS NOT NULL THEN COALESCE(' ' + v.MiddleName, '') ELSE '' END 
			, 0 FROM dbo.[User] v with(nolock)
			where 
			MailFlag is null and
			UserId not in (
				select EntityId from dbo.[Sid] where Data = @TemplateName and Type = 0
			)

		SET @send_cnt = @send_cnt - @@ROWCOUNT;
		END

		if (@Flag & 2 > 0) 
		BEGIN
			INSERT INTO #tmpList  ([Id], [Email], [Name], [Type])
			SELECT top (@send_cnt) v.Id, v.Email, CASE WHEN v.Name IS NOT NULL and v.Name != '' THEN v.Name ELSE v.Email END, 1
			FROM dbo.[Visitor] v with(nolock)
			where 
			v.Status != 1 and
			Id not in (
				select EntityId from dbo.[Sid] where Data = @TemplateName and Type = 1
			)
		END
	
	IF (@Body is not null and @Body <> '' and @Title is not null and @Title <> '') BEGIN
		DECLARE UsersCursor CURSOR FAST_FORWARD FOR
		SELECT [Email], [Name], [Id], [Type] FROM #tmpList;
		
		OPEN UsersCursor;

		DECLARE 
			@Email nvarchar(256), 
			@Name nvarchar(256), 
			@Link nvarchar(256),
			@Sid nvarchar(256),
			@Type int,
			@Id int;
			
		DECLARE @ModifiedBody nvarchar(max);
		DECLARE @ModifiedTitle nvarchar(256);

		FETCH NEXT FROM UsersCursor
		INTO @Email, @Name, @Id, @Type;
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT 'Processing ' + convert(nvarchar,@Id) + ' to ' + @Name;
			
		SET @ModifiedTitle = @Title;
		SET @ModifiedBody = @Body;
		SET @Sid = NEWID();
		
		IF (@TestEmail is null)
			INSERT INTO dbo.[Sid] (Created, [Type], Data, Sid, EntityId, [Status])
			VALUES (GETUTCDATE(), @Type, @TemplateName, @Sid, @Id, @Flag);
		
		SET @ModifiedTitle = REPLACE(@ModifiedTitle,'[Name]',@Name);
		SET @ModifiedBody = REPLACE(@ModifiedBody,'[Name]',@Name);
		SET @ModifiedBody = REPLACE(@ModifiedBody,'[Sid]',@Sid);
		
		if @TestEmail is not null set @Email = @TestEmail
		
        exec dbo.MailInsert
			@FromEmail = @from_email,
			@ToEmail = @Email,
			@ToUserId = null,
			@Subject = @ModifiedTitle,
			@Body = @ModifiedBody,
			@IsBodyHtml = 1,
			@Priority = 0,
			@Type = 0,
			@Status = 0;
		
		/*IF @TestEmail is null
			UPDATE [dbo].[tblVisitor]			
				SET Status = (Status | @Flag)
				where VisitorId = @Id;*/

		FETCH NEXT FROM UsersCursor
		INTO @Email, @Name, @Id, @Type;

		END;/*Fetch*/
		CLOSE UsersCursor;
		DEALLOCATE UsersCursor;

	DROP TABLE #tmpList;

	END;/*IF (@Body... */
END

