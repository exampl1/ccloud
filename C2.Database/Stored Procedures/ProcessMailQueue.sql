﻿CREATE PROCEDURE [dbo].[ProcessMailQueue]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @id int
	DECLARE @from nvarchar(256)
	DECLARE @to nvarchar(256)
	DECLARE @subject nvarchar(256)
	DECLARE @body nvarchar(max)
	DECLARE @ishtml bit
	DECLARE @format nvarchar(20)
	
	DECLARE @cursorVariable CURSOR
	DECLARE @counter int
	DECLARE @i int
	DECLARE @retstate int

	--exec [dbo].[CreateSpamVisitor] 	@TemplateName = 'okiss-mail', @Flag = 0, @send_cnt = 10
	IF exists (select 1 from [dbo].Job WHERE [Status] = 1 and StartDate <= getutcdate())
	BEGIN

		DECLARE @templateName nvarchar(256);
		DECLARE @startDate datetime;

		DECLARE spamCursor CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
		FOR SELECT [TemplateName] FROM [dbo].Job WHERE [Status] = 1 and StartDate <= getutcdate();

		SET @cursorVariable = spamCursor

		SELECT @counter = COUNT(*) FROM [dbo].Job WHERE [Status] = 1 and StartDate <= getutcdate();
		SET @i = 1

		OPEN @cursorVariable;

		WHILE @i <= @counter
		BEGIN
			FETCH @cursorVariable INTO @templateName;
			PRINT 'running ' + @templateName;
			exec [dbo].[CreateSpamVisitor] 	@TemplateName = @templateName, @Flag = 1, @send_cnt = 2;
			SET @i = @i+1;
		END
	END;


	DECLARE myCursor CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
	FOR SELECT [Id], [FromEmail], [ToEmail], [Subject], [Body], [IsBodyHtml] FROM [dbo].[MailQueue] WHERE [Status] = 0;

	SET @cursorVariable = myCursor

	SELECT @counter = COUNT(*) FROM [dbo].[MailQueue]	WHERE [Status] = 0;
	SET @i = 1

	OPEN @cursorVariable;

	WHILE @i <= @counter
	BEGIN
		FETCH @cursorVariable INTO @id, @from, @to, @subject, @body, @ishtml
		IF @to IS NOT NULL AND @to like '%@%' BEGIN
			SET @format = (CASE WHEN @ishtml = 1 THEN 'HTML' ELSE 'TEXT' END);
			
			EXEC @retstate = msdb.dbo.sp_send_dbmail @recipients=@to, @subject = @subject, @body = @body,  @body_format = @format, @profile_name = @from

			IF (@retstate = 0) 
			BEGIN	
				UPDATE [dbo].[MailQueue] SET [Status] = 1, [Sent] = getdate() WHERE [Id] = @id;	
			END	
		END ELSE BEGIN
			UPDATE [dbo].[MailQueue] SET [Status] = 2, [Sent] = getdate() WHERE [Id] = @id;	
		END

		SET @i = @i+1
	END
	
	CLOSE @cursorVariable;

RETURN 0
END