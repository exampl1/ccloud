﻿CREATE TABLE [dbo].[Subject]
(
	[Id] [dbo].[EntityId] NOT NULL,
	[Code] [dbo].[Code] NOT NULL DEFAULT NEWID(),
    [Status] INT NOT NULL,
    [SubjectType] TINYINT NOT NULL,
    [Email] [dbo].[UserName] NULL, 
    [Phone] [dbo].[Phone] NULL, 
    CONSTRAINT [PK_Subject] PRIMARY KEY ([Id])
)
