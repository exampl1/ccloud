﻿CREATE TABLE [dbo].[SubjectLocalization]
(
	[SubjectId] [dbo].[EntityId] NOT NULL , 
    [CultureCode] [dbo].[CultureCode] NOT NULL, 
    [ShortName] [dbo].[Name] NOT NULL, 
    [Name] [dbo].[Name] NOT NULL, 
    [FullName] [dbo].[FullName] NOT NULL, 
    [Description] [dbo].[Description] NOT NULL, 
    CONSTRAINT [PK_SubjectLocalization] PRIMARY KEY ([SubjectId], [CultureCode]), 
    CONSTRAINT [FK_SubjectLocalization_ToSubject] FOREIGN KEY ([SubjectId]) REFERENCES [Subject]([Id]) 
)
