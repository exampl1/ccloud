﻿CREATE TABLE [dbo].[Subscriber]
(
	[Id] [dbo].[EntityId] NOT NULL PRIMARY KEY, 
    [Name] [dbo].[FullName] NOT NULL, 
    [Email] [dbo].[UserName] NOT NULL, 
    [Created] SMALLDATETIME NOT NULL DEFAULT getdate()
)

GO

CREATE INDEX [IX_Subscriber_Email] ON [dbo].[Subscriber] ([Email])
