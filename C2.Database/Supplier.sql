﻿CREATE TABLE [dbo].[Supplier]
(
    [SubjectId] [dbo].[EntityId] NOT NULL, 
    [Domain] [dbo].[Url] NOT NULL, 
    [PhoneBlock] [dbo].[Description] NOT NULL DEFAULT 'PhoneBlock', 
    [FirstFooterBlock] [dbo].[Description] NOT NULL DEFAULT 'FirstFooterBlock', 
    [SecondFooterBlock] [dbo].[Description] NOT NULL DEFAULT 'SecondFooterBlock', 
    [ThirdFooterBlock] [dbo].[Description] NOT NULL DEFAULT 'ThirdFooterBlock', 
    [Logo] [dbo].[Url] NOT NULL DEFAULT 'LOGO', 
    [Banner] [dbo].[Description] NULL, 
    [PartnerCode] [dbo].[EntityId] NULL, 
    [LeftFooterBlock] [dbo].[Description] NULL, 
    [YaId] VARCHAR(15) NULL, 
    CONSTRAINT [FK_Supplier_Subject] FOREIGN KEY ([SubjectId]) REFERENCES [Subject]([Id]), 
    PRIMARY KEY ([SubjectId])
)
