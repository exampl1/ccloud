﻿CREATE TYPE [dbo].[NodePath]
	FROM varchar(340) NOT NULL

/* Max deep is 20 levels, 20 * 17 = 340 */