﻿CREATE TABLE [dbo].[UIdTable]
(
	[Id] [dbo].[EntityId] NOT NULL IDENTITY(1, 100),
    [Created] DATETIME NOT NULL, 
    CONSTRAINT [PK_UIdTable] PRIMARY KEY ([Id])
)
