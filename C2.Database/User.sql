﻿CREATE TABLE [dbo].[User]
(
    [UserId]   INT IDENTITY (1, 1) NOT NULL,
    [UserName] NVARCHAR (MAX) NULL,
    [FirstName] [dbo].[Name] NULL, 
    [MiddleName] [dbo].[Name] NULL, 
    [LastName] [dbo].[Name] NULL, 
    [Phone] NVARCHAR(50) NULL, 
    [MailFlag] TINYINT NULL, 
    CONSTRAINT [PK_User] PRIMARY KEY ([UserId] ASC)
)