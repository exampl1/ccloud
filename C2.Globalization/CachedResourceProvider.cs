﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Runtime.Caching;

namespace C2.Globalization
{
    /// <summary>
    /// Получение локализованных данных из кэша.
    /// </summary>
    public class CachedResourceProvider : IResourceProvider
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="internalProvider">Провайдер локализованных строк, служит для заполнения кэша.</param>
        /// <param name="cache">Кэш. По умолчанию - MemoryCache.</param>
        /// <param name="cachePolicyFactory">Фабрика политики кэширования, по умолчанию элементы кэшируются на 1 час.</param>
        public CachedResourceProvider(IResourceProvider internalProvider,
            ObjectCache cache = null,
            Func<string, CacheItemPolicy> cachePolicyFactory = null)
        {
            _internalProvider = internalProvider;
            _cache = cache ?? new MemoryCache("Resources");
            _cachePolicyFactory = cachePolicyFactory ?? CreateDefaultPolicy;
        }

        IResourceProvider _internalProvider;

        ObjectCache _cache;

        Func<string, CacheItemPolicy> _cachePolicyFactory;

        public bool TryGetString(string key,
            string culture,
            out string value)
        {
            Dictionary<string, string> values;
            if (TryGetStrings(culture, out values))
            {
                return values.TryGetValue(key, out value);
            }
            else
            {
                value = string.Empty;
                return false;
            }
        }

        public bool TryGetStrings(string culture,
            out Dictionary<string, string> values)
        {
            values = (Dictionary<string, string>)_cache.Get(culture);
            if (values == null)
            {
                if (_internalProvider.TryGetStrings(culture, out values))
                {
                    _cache.Add(culture, values, _cachePolicyFactory(culture));
                }
            }
            
            return values != null;
        }

        CacheItemPolicy CreateDefaultPolicy(string culture)
        {
            return new CacheItemPolicy()
            {
                AbsoluteExpiration = DateTime.Now.AddHours(1)
            };
        }
    }
}
