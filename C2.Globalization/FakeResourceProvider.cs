﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.Globalization
{
    public class FakeResourceProvider : IResourceProvider
    {
        public bool TryGetString(string key, string culture, out string value)
        {
            value = null;
            return false;
        }

        public bool TryGetStrings(string culture, out Dictionary<string, string> values)
        {
            values = null;
            return false;
        }
    }
}
