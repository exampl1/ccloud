﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace C2.Globalization
{
    /// <summary>
    /// Менеджер локализации.
    /// </summary>
    public static class ResourceManager
    {
        /// <summary>
        /// Инициализация менеджера.
        /// </summary>
        /// <param name="provider">Провайдер локализации.</param>
        /// <param name="supportedCultures">Список поддерживаемых культур.</param>
        /// <param name="defaultCulture">Культура, используемая по умолчанию.</param>
        /// <param name="missingKeyMessageFormat">Формат сообщения о пропущенном (не известном) ключе.</param>
        public static void Init(IResourceProvider provider,
            List<CultureInfo> supportedCultures,
            CultureInfo defaultCulture,
            string missingKeyMessageFormat = "{0}?")
        {
            _provider = provider;
            SupportedCultures = supportedCultures;
            DefaultCulture = defaultCulture;
            _missingKeyMessageFormat = missingKeyMessageFormat;
        }

        static IResourceProvider _provider;

        /// <summary>
        /// Список поддерживаемых культур.
        /// </summary>
        public static List<CultureInfo> SupportedCultures
        {
            get;
            private set;
        }

        /// <summary>
        /// Культура по умолчанию.
        /// </summary>
        public static CultureInfo DefaultCulture
        {
            get;
            private set;
        }

        static string _missingKeyMessageFormat;

        /// <summary>
        /// Получение локализованной строки.
        /// </summary>
        /// <param name="key">Ключ строки.</param>
        /// <param name="culture">Культура, для которой ищется локализация строки.</param>
        /// <returns></returns>
        public static string GetString(string key, CultureInfo culture)
        {
            bool supported = false;
            do
            {
                if (!(supported = SupportedCultures.Contains(culture)))
                {
                    culture = culture != CultureInfo.InvariantCulture ?
                        culture.Parent :
                        null;
                }
            }
            while (!supported && (culture != null));

            if (!supported)
            {
                culture = DefaultCulture;
            }

            return GetStringInternal(key, culture);
        }

        /// <summary>
        /// Получение локализованной строки. Используется текущая культура пользовательского интерфейса.
        /// </summary>
        /// <param name="key">Ключ строки.</param>
        /// <returns></returns>
        public static string GetString(string key)
        {
            return GetString(key, CultureInfo.CurrentUICulture);
        }

        static string GetStringInternal(string key, CultureInfo culture)
        {
            string value;
            if (_provider.TryGetString(key, culture.TwoLetterISOLanguageName, out value))
            {
                return value;
            }

            if (culture == DefaultCulture)
            {
                return string.Format(_missingKeyMessageFormat, key);
            }

            // Fallback
            culture = culture == CultureInfo.InvariantCulture ?
                DefaultCulture : culture.Parent;

            return GetStringInternal(key, culture);
        }
    }
}
