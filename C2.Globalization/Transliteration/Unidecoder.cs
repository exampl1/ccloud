﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.Globalization.Transliteration
{
    /// <summary>
    /// ASCII transliterations of Unicode text
    /// </summary>
    /// <remarks>
    /// Character transliteration tables:
    /// Copyright 2001, Sean M. Burke (sburke@cpan.org), all rights reserved.
    /// </remarks>
    public static partial class Unidecoder
    {
        /// <summary>
        /// Transliterate an Unicode object into an ASCII string
        /// </summary>
        /// <remarks>
        /// unidecode(u"\u5317\u4EB0") == "Bei Jing "
        /// </remarks>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string Unidecode(this string input)
        {
            if (String.IsNullOrEmpty(input)) return input;
            var output = new StringBuilder();
            foreach (var c in input.ToCharArray())
            {
                if (c < 0x80)
                {
                    output.Append(c);
                    continue;
                }
                var h = c >> 8;
                var l = c & 0xff;

                if (Characters.ContainsKey(h))
                {
                    output.Append(Characters[h][l]);
                }
                else
                {
                    output.Append("");
                }
            }

            return output.ToString();
        }

        /// <summary>
        /// Transliterate an Unicode string into an ASCII string with strong rules.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="whitespacePlaceholder"></param>
        /// <returns></returns>
        public static string UnidecodeStrong(this string input, char whitespacePlaceholder = '-')
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            var decodedInput = input.Unidecode();

            var output = new StringBuilder();

            bool prevWhitespase = false;
            foreach (var c in decodedInput)
            {
                if (char.IsWhiteSpace(c))
                {
                    // Replace whitespaces and remove whitespace's duplicates.
                    if (!prevWhitespase)
                    {
                        output.Append(whitespacePlaceholder);
                    }
                    prevWhitespase = true;
                }
                else
                {
                    prevWhitespase = false;
                    if (char.IsLetterOrDigit(c))
                    {
                        output.Append(c);
                    }
                    else
                    {
                        // Skip all other symbols.
                        continue;
                    }
                }
            }

            return output.ToString();
        }
    }
}
