﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace C2.Logging
{
    /// <summary>
    /// Вспомогательный класс-обёртка для логирования событий.
    /// </summary>
    public abstract class BaseLogger
    {
        /// <summary>
        /// Логирование событий.
        /// </summary>
        /// <param name="severity">Уровень важности логируемого события.</param>
        /// <param name="messageFormat">Формат сообщения.</param>
        /// <param name="formatParams">Параметры форматирования.</param>
        public abstract void Log(LogMessageSeverity severity,
            string messageFormat,
            params object[] formatParams);

        /// <summary>
        /// Логирование критических ошибок.
        /// </summary>
        /// <param name="messageFormat"></param>
        /// <param name="formatParams"></param>
        public void Fatal(string messageFormat,
            params object[] formatParams)
        {
            Log(LogMessageSeverity.Fatal, messageFormat, formatParams);
        }

        /// <summary>
        /// Логирование ошибок (не критических).
        /// </summary>
        /// <param name="messageFormat"></param>
        /// <param name="formatParams"></param>
        public void Error(string messageFormat,
            params object[] formatParams)
        {
            Log(LogMessageSeverity.Error, messageFormat, formatParams);
        }

        /// <summary>
        /// Логирование предупрежедний.
        /// </summary>
        /// <param name="messageFormat"></param>
        /// <param name="formatParams"></param>
        public void Warn(string messageFormat,
            params object[] formatParams)
        {
            Log(LogMessageSeverity.Warning, messageFormat, formatParams);
        }

        /// <summary>
        /// Логирование информационных сообщений.
        /// </summary>
        /// <param name="messageFormat"></param>
        /// <param name="formatParams"></param>
        public void Info(string messageFormat,
            params object[] formatParams)
        {
            Log(LogMessageSeverity.Information, messageFormat, formatParams);
        }

        /// <summary>
        /// Логирование отладочной информации.
        /// </summary>
        /// <param name="messageFormat"></param>
        /// <param name="formatParams"></param>
        [Conditional("TRACE")]
        public void Trace(string messageFormat,
            params object[] formatParams)
        {
            Log(LogMessageSeverity.Trace, messageFormat, formatParams);
        }

        /// <summary>
        /// Логирование отладочной информации.
        /// </summary>
        /// <param name="messageFormat"></param>
        /// <param name="formatParams"></param>
        [Conditional("DEBUG")]
        public void Debug(string messageFormat,
            params object[] formatParams)
        {
            Log(LogMessageSeverity.Debug, messageFormat, formatParams);
        }
    }
}
