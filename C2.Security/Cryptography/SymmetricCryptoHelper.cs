﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace C2.Security.Cryptography
{
    /// <summary>
    /// Helper for working with symmetric encryption/decription.
    /// </summary>
    public class SymmetricCryptoHelper
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="key">Crypto key.</param>
        /// <param name="iv">Initial vector.</param>
        /// <param name="algName">Name of crypto algorithm.</param>
        public SymmetricCryptoHelper(byte[] key, byte[] iv, string algName = "Rijndael")
        {
            _key = key;
            _iv = iv;
            _algName = algName;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="key">Crypto key as hex-string.</param>
        /// <param name="iv">Initial vector as hex-string.</param>
        /// <param name="algName">Name of crypto algorithm.</param>
        public SymmetricCryptoHelper(string key, string iv, string algName = "Rijndael")
            : this(HexToByte(key), HexToByte(iv), algName)
        {
        }

        byte[] _key;

        byte[] _iv;

        string _algName;

        /// <summary>
        /// Encrypt string to byte array.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public byte[] Encrypt(string value)
        {
            using (SymmetricAlgorithm alg = SymmetricAlgorithm.Create(_algName))
            {
                using (ICryptoTransform encryptor = alg.CreateEncryptor(_key, _iv))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter wr = new StreamWriter(cs))
                            {
                                wr.Write(value);
                            }
                            return ms.ToArray();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Encrypt string to base64-string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string EncryptToBase64(string value)
        {
            return Convert.ToBase64String(Encrypt(value));
        }

        /// <summary>
        /// Decrypt byte array.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string Decrypt(byte[] value)
        {
            using (SymmetricAlgorithm alg = SymmetricAlgorithm.Create(_algName))
            {
                using (ICryptoTransform decryptor = alg.CreateDecryptor(_key, _iv))
                {
                    using (MemoryStream ms = new MemoryStream(value))
                    {
                        using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader rd = new StreamReader(cs))
                            {
                                return rd.ReadToEnd();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Decrypt base64-string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string DecryptFromBase64(string value)
        {
            return Decrypt(Convert.FromBase64String(value));
        }

        static byte[] HexToByte(string hex)
        {
            byte[] result = new byte[hex.Length / 2];

            for (int i = 0; i < hex.Length / 2; i++)
            {
                result[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }

            return result;
        }
    }
}
