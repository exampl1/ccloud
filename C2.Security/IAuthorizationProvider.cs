﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;

namespace C2.Security
{
    public interface IAuthorizationProvider
    {
        bool IsAuthorized(IPrincipal principal, string ruleName);
    }
}
