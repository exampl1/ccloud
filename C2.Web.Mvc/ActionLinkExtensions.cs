﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace C2.Web.Mvc
{
    public static class ActionLinkExtensions
    {
        public static string AbsoluteUrl(this UrlHelper url, string action, string controller, object routeValues)
        {
            Uri requestUrl = url.RequestContext.HttpContext.Request.Url;

            string absoluteAction = string.Format("{0}://{1}{2}",
                                                  requestUrl.Scheme,
                                                  requestUrl.Authority,
                                                  url.Action(action, controller, routeValues));

            return absoluteAction;
        }
        
        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action.
        /// </summary>
        /// <param name="helper">The HTML helper instance that this method extends.</param>
        /// <param name="actionMetaData">The metadata of the action.</param>
        /// <returns>An anchor element (a element).</returns>
        public static MvcHtmlString ActionLink(this HtmlHelper helper, IActionMetaData actionMetaData)
        {
            return helper.ActionLink(actionMetaData.Title,
                actionMetaData.ActionName,
                actionMetaData.ControllerName);
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action or empty string if user is not authorized.
        /// </summary>
        /// <param name="helper">The HTML helper instance that this method extends.</param>
        /// <param name="actionMetaData">The metadata of the action.</param>
        /// <param name="principal">User's principal.</param>
        /// <returns>An anchor element (a element).</returns>
        public static MvcHtmlString SecureActionLink(this HtmlHelper helper, IActionMetaData actionMetaData, IPrincipal principal = null)
        {
            if (!actionMetaData.IsAuthorized(principal))
            {
                return MvcHtmlString.Empty;
            }
            else
            {
                return helper.ActionLink(actionMetaData);
            }
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action.
        /// </summary>
        /// <param name="helper">The HTML helper instance that this method extends.</param>
        /// <param name="actionMetaData">The metadata of the action.</param>
        /// <param name="routeValues">
        /// An object that contains the parameters for a route. The parameters are retrieved through reflection by
        /// examining the properties of the object. The object is typically created by using object initializer syntax.
        /// </param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        /// <returns>An anchor element (a element).</returns>
        public static MvcHtmlString ActionLink(this HtmlHelper helper, IActionMetaData actionMetaData, object routeValues, object htmlAttributes)
        {
            return helper.ActionLink(actionMetaData.Title,
                actionMetaData.ActionName,
                actionMetaData.ControllerName,
                routeValues,
                htmlAttributes);
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action or empty string if user is not authorized.
        /// </summary>
        /// <param name="helper">The HTML helper instance that this method extends.</param>
        /// <param name="actionMetaData">The metadata of the action.</param>
        /// <param name="routeValues">
        /// An object that contains the parameters for a route. The parameters are retrieved through reflection by
        /// examining the properties of the object. The object is typically created by using object initializer syntax.
        /// </param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        /// <param name="principal">User's principal.</param>
        /// <returns>An anchor element (a element).</returns>
        public static MvcHtmlString SecureActionLink(this HtmlHelper helper, IActionMetaData actionMetaData, object routeValues, object htmlAttributes, IPrincipal principal = null)
        {
            if (!actionMetaData.IsAuthorized(principal))
            {
                return MvcHtmlString.Empty;
            }
            else
            {
                return helper.ActionLink(actionMetaData, routeValues, htmlAttributes);
            }
        }

        /// <summary>
        /// Returns an anchor element that contains the virtual path of the specified action.
        /// </summary>
        /// <param name="helper">The HTML helper instance that this method extends.</param>
        /// <param name="actionMetaData">The metadata of the action.</param>
        /// <param name="protocol">The protocol for the URL, such as "http" or "https".</param>
        /// <param name="hostName">The host name for the URL.</param>
        /// <param name="fragment">The URL fragment name (the anchor name).</param>
        /// <param name="routeValues">
        /// An object that contains the parameters for a route. The parameters are retrieved through reflection by
        /// examining the properties of the object. The object is typically created by using object initializer syntax.
        /// </param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        /// <returns>An anchor element (a element).</returns>
        public static MvcHtmlString ActionLink(this HtmlHelper helper, IActionMetaData actionMetaData, string protocol, string hostName, string fragment, object routeValues, object htmlAttributes)
        {
            return helper.ActionLink(actionMetaData.Title,
                actionMetaData.ActionName,
                actionMetaData.ControllerName,
                protocol,
                hostName,
                fragment,
                routeValues,
                htmlAttributes);
        }

        /// <summary>
        /// Returns an anchor element that contains the virtual path of the specified action or empty string if user is not authorized.
        /// </summary>
        /// <param name="helper">The HTML helper instance that this method extends.</param>
        /// <param name="actionMetaData">The metadata of the action.</param>
        /// <param name="protocol">The protocol for the URL, such as "http" or "https".</param>
        /// <param name="hostName">The host name for the URL.</param>
        /// <param name="fragment">The URL fragment name (the anchor name).</param>
        /// <param name="routeValues">
        /// An object that contains the parameters for a route. The parameters are retrieved through reflection by
        /// examining the properties of the object. The object is typically created by using object initializer syntax.
        /// </param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
        /// <returns>An anchor element (a element).</returns>
        public static MvcHtmlString SecureActionLink(this HtmlHelper helper, IActionMetaData actionMetaData, string protocol, string hostName, string fragment, object routeValues, object htmlAttributes, IPrincipal principal = null)
        {
            if (!actionMetaData.IsAuthorized(principal))
            {
                return MvcHtmlString.Empty;
            }
            else
            {
                return helper.ActionLink(actionMetaData,
                    protocol,
                    hostName,
                    fragment,
                    routeValues,
                    htmlAttributes);
            }
        }
    }
}
