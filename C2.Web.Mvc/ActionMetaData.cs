﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using C2.Globalization;
using C2.Security;

namespace C2.Web.Mvc
{
    public interface IActionMetaData
    {
        string ControllerName
        {
            get;
        }

        string MethodName
        {
            get;
        }

        string ActionName
        {
            get;
        }

        string Title
        {
            get;
        }

        bool IsAuthorized(IPrincipal principal = null);
    }

    public class ActionMetaData<TController> : IActionMetaData
        where TController : Controller
    {
        static readonly Regex _controllerNameNormalize = new Regex("Controller$");

        public ActionMetaData(string methodName, string titleKey, string httpMethod = "GET")
        {
            // Controller
            ControllerType = typeof(TController);
            ControllerName = _controllerNameNormalize.Replace(ControllerType.Name, string.Empty);

            // Method
            MethodName = methodName;

            var methods = ControllerType
                .GetMethods()
                .Where(m => m.Name == methodName)
                .ToList();

            if (methods.Count == 0)
            {
                throw new ArgumentException("Method not found.");
            }

            MethodInfo method = null;
            for (int i = 0; i < methods.Count; i++)
            {
                var m = methods[i];
                var a = m.GetCustomAttributes(typeof(ActionMethodSelectorAttribute), true).FirstOrDefault();

                if ((a is HttpPostAttribute) && (httpMethod == "POST"))
                {
                    method = m;
                    break;
                }
                if ((a is HttpDeleteAttribute) && (httpMethod == "DELETE"))
                {
                    method = m;
                    break;
                }
                if ((a is HttpHeadAttribute) && (httpMethod == "HEAD"))
                {
                    method = m;
                    break;
                }
                if ((a is HttpOptionsAttribute) && (httpMethod == "OPTIONS"))
                {
                    method = m;
                    break;
                }
                if (httpMethod == "GET")
                {
                    method = m;
                    break;
                }
            }

            if (method == null)
            {
                throw new ArgumentException("Method not found.");
            }

            AuthorizeRule = method.GetCustomAttribute<AuthorizeRuleAttribute>(true) ?? ControllerType.GetCustomAttribute<AuthorizeRuleAttribute>();
            
            var actionNameAttribute = method.GetCustomAttribute<ActionNameAttribute>(true);
            if (actionNameAttribute != null)
            {
                ActionName = actionNameAttribute.Name;
            }
            else
            {
                ActionName = methodName;
            }

            // Title
            TitleKey = titleKey;
        }

        public Type ControllerType
        {
            get;
            private set;
        }

        public string ControllerName
        {
            get;
            private set;
        }

        public string MethodName
        {
            get;
            private set;
        }

        public string ActionName
        {
            get;
            private set;
        }

        public string TitleKey
        {
            get;
            private set;
        }

        public string Title
        {
            get
            {
                return ResourceManager.GetString(TitleKey);
            }
        }

        public AuthorizeRuleAttribute AuthorizeRule
        {
            get;
            private set;
        }

        public bool IsAuthorized(IPrincipal principal = null)
        {
            if (AuthorizeRule == null)
            {
                // Action is unsecure.
                return true;
            }

            return AuthorizeRule.IsAuthorized(principal ?? HttpContext.Current.User);
        }
    }
}
