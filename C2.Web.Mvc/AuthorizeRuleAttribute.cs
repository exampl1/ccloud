﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using C2.Security;

namespace C2.Web.Mvc
{
    public class AuthorizeRuleAttribute : FilterAttribute,
        IAuthorizationFilter
    {
        public AuthorizeRuleAttribute(string ruleName, string providerName = "Default")
        {
            _ruleName = ruleName;
            _providerName = providerName;
        }

        string _ruleName;

        string _providerName;

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!IsAuthorized(filterContext.HttpContext.User))
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        public bool IsAuthorized(IPrincipal principal)
        {
            IAuthorizationProvider authProvider;

            if (!AuthorizationProviderRepository.TryGetProvider(_providerName, out authProvider) || !authProvider.IsAuthorized(principal, _ruleName))
            {
                return false;
            }

            return true;
        }
    }
}
