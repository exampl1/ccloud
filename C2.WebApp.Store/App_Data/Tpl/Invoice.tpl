﻿<style>
  body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 11pt;}
  table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }
  table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }
  table.invoice_items { border: 1px solid; border-collapse: collapse;}
  table.invoice_items td, table.invoice_items th { border: 1px solid;}
</style>

<table width="100%">
  <tr>
    <td>&nbsp;</td>
    <td style="width: 155mm;">
      <div style="width:155mm; ">Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате  обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается по факту прихода денег на р/с Поставщика</div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <div style="text-align:center;  font-weight:bold;">
        Образец заполнения платежного поручения
      </div>
    </td>
  </tr>
</table>


<table width="100%" cellpadding="2" cellspacing="2" class="invoice_bank_rekv">
  <tr>
    <td colspan="2" rowspan="2" style="min-height:13mm; width: 105mm;">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 13mm;">
        <tr>
          <td valign="top">
            <div>ОАО "АЛЬФА-БАНК"</div>
          </td>
        </tr>
        <tr>
          <td valign="bottom" style="height: 3mm;">
            <div style="font-size:10pt;">Банк получателя        </div>
          </td>
        </tr>
      </table>
    </td>
    <td style="min-height:7mm;height:auto; width: 15mm;">
      <div>БИK</div>
    </td>
    <td rowspan="2" style="vertical-align: top; width: 50mm;">
      <div style=" height: 7mm; line-height: 7mm; vertical-align: middle;">044525593</div>
      <div>30101810200000000593 в ОПЕРУ МОСКВА</div>
    </td>
  </tr>
  <tr>
    <td style="width: 15mm;">
      <div>Сч. №</div>
    </td>
  </tr>
  <tr>
    <td style="min-height:6mm; height:auto; width: 50mm;">
      <div>ИНН 7728168971</div>
    </td>
    <td style="min-height:6mm; height:auto; width: 55mm;">
      <div>КПП 775001001</div>
    </td>
    <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 15mm;">
      <div>Сч. №</div>
    </td>
    <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 50mm;">
      <div>40702810702340000186</div>
    </td>
  </tr>
  <tr>
    <td colspan="2" style="min-height:13mm; height:auto;">

      <table border="0" cellpadding="0" cellspacing="0" style="height: 13mm; width: 105mm;">
        <tr>
          <td valign="top">
            <div>Общество с ограниченной ответственностью «СМАРТ»</div>
          </td>
        </tr>
        <tr>
          <td valign="bottom" style="height: 3mm;">
            <div style="font-size: 10pt;">Получатель</div>
          </td>
        </tr>
      </table>

    </td>
  </tr>
</table>
<br/>

<div style="font-weight: bold; font-size: 16pt; padding-left:5px;">
  Счет № {{Number}} от {{Date}}
</div>
<br/>

<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>

<table width="100%">
  <tr>
    <td style="width: 30mm;">
      <div style=" padding-left:2px;">Поставщик:    </div>
    </td>
    <td>
      <div style="font-weight:bold;  padding-left:2px;">
        Общество с ограниченной ответственностью «СМАРТ»
      </div>
    </td>
  </tr>
  <tr>
    <td style="width: 30mm;">
      <div style=" padding-left:2px;">Покупатель:    </div>
    </td>
    <td>
      <div style="font-weight:bold;  padding-left:2px;">
        {{OrgName}}
      </div>
    </td>
  </tr>
</table>

<br/>

<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
  <thead>
    <tr>
      <th style="width:10mm;">№</th>
      <th style="width:35mm;">Артикул</th>
      <th>Наименование товара</th>
      <th style="width:15mm;">Кол-во</th>
      <th style="width:15mm;">Ед.</th>
      <th style="width:25mm;">Цена</th>
      <th style="width:25mm;">Сумма</th>
    </tr>
  </thead>
  <tbody >
    {{foreach Items Items}}
  </tbody>
</table>

<table border="0" width="100%" cellpadding="1" cellspacing="1">
  <tr>
    <td></td>
    <td style="width:50mm; font-weight:bold;  text-align:right;">Итого:</td>
    <td style="width:27mm; font-weight:bold;  text-align:right;">{{Total}}</td>
  </tr>
  <tr>
    <td></td>
    <td style="width:50mm; font-weight:bold;  text-align:right;">Без налога (НДС).</td>
    <td style="width:27mm; font-weight:bold;  text-align:right;">-</td>
  </tr>
  <tr>
    <td></td>
    <td style="width:50mm; font-weight:bold;  text-align:right;">Всего к оплате:</td>
    <td style="width:27mm; font-weight:bold;  text-align:right;">{{Total}}</td>
  </tr>
</table>

<br />
<div>
  Всего наименований {{ItemsCount}} на сумму {{Total}} рублей.<br />
  {{SumText}}
</div>
<br />
<br />
<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>
<br/>

<div>Руководитель ______________________ (Боярова Ирина Анатольевна)</div>
<br/>

<div>Главный бухгалтер ______________________ (Боярова Ирина Анатольевна)</div>
<br/>

<div style="width: 85mm;text-align:center;">
  М.П.
</div>
<br/>


<div style="width:800px;text-align:left;font-size:10pt;margin-top: 40px;">Счет действителен к оплате в течении трех дней.</div>
