﻿<div style='font-family: Segoe UI, Segoe, Arial, sans-serif'>
  <p>Здравствуйте, {{VendorName}}!</p>
  <p>Принят заказ на сайте <a href='http://optodrom.ru'>http://optodrom.ru</a>,
    включающий следующую вашу продукцию:</p>
  <table border='1' cellpadding='2' cellspacing='1'>
    <tr>
      <th>№</th>
      <th>Артикул</th>
      <th>Название товара</th>
      <th>Количество</th>
    </tr>
    {{foreach Items Items}}
  </table>
  <p></p>
  <h2>Просим подготовить товар к отпарвке и выставить счет:</h2>
  <div>
    <p>Общество с ограниченной ответственностью «СМАРТ» г. Москва</p>

    <p>ИНН 7728168971</p>
    <p>КПП 775001001</p>
    <p>БИК 044525593</p>
    <p>р/с №40702810702340000186 в Сибирском банке Сбербанка России</p>
    <p>к/с 30101810200000000593 в ОПЕРУ МОСКВА</p>
  </div>
  {{include Footer}}
</div>