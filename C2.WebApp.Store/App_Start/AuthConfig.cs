﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;

namespace C2.WebApp.Store
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            var scope = new[] { "email", "user_birthday", "user_groups", "user_hometown", "user_likes", "user_location", "user_notes", "user_about_me" };
            OAuthWebSecurity.RegisterFacebookClient(
                appId: "249868411720288",
                appSecret: "af854d6a72b75ba159ef14b319d04e52",
                displayName: "Facebook",
                extraData: new Dictionary<string, object>()
                );

            OAuthWebSecurity.RegisterGoogleClient();

            //var roleProvider = new WebMatrix.WebData.SimpleRoleProvider();
            //roleProvider.CreateRole("Admin");


        }
    }
}
