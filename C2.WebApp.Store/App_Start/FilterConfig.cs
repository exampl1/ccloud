﻿using C2.WebApp.Store.Controllers;
using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Store
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new ProductPageDataLoader());
            filters.Add(new Filters.PartnerAttribute());
            filters.Add(new Filters.SidAttribute());
        }
    }
}