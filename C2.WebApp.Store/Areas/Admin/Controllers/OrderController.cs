﻿using C2.AppController;
using C2.DataManager.Store;
using C2.DataMapping;
using C2.DataMapping.Store;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.WebApp.Models;
using C2.WebApp.Store.Areas.Admin.Models;
using C2.WebApp.Store.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace C2.WebApp.Store.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OrderController : Controller
    {
        public OrderController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;

        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }

        //
        // GET: /Admin/Order/

        public ActionResult Index(OrderViewParameters parameters)
        {
            parameters.Status = OrderStatus.Created;

            OrderViewModel outModel = new OrderViewModel()
            {
                Parameters = parameters,
                Orders = DoSearch(parameters)
            };

            return View(outModel);
        }

        [HttpPost]
        public ActionResult Search(OrderViewParameters parameters)
        {
            return PartialView("_OrderList", DoSearch(parameters));
        }

        public ActionResult Item(long id)
        {
            var order = _dataContext.LoadOrderDetails(id);

            if (order == null)
            {
                return RedirectToAction("Index");
            }

            return View(order);
        }

        [HttpPost]
        public ActionResult AddOfferToOrder(long orderId, long offerId)
        {
            var _order = _dataContext.LoadOrderDetails(orderId);
            if (_order != null)
            {
                var _offer = _dataContext.LoadOfferDetails(offerId);
                if (_offer != null)
                {
                    var orderItem = _order.Items.FirstOrDefault(oi => (oi.OfferId == offerId));
                    var customerInfo = _dataContext.CustomerInfos.FirstOrDefault(c => c.UserName == _order.UserProfile.UserName);
                    bool isWholesale = (customerInfo != null && customerInfo.Status == CustomerStatus.IsWholesale);
                    if (orderItem == null)
                    {
                        orderItem = new OrderItem()
                        {
                            Id = BaseContext.GetId(),
                            Order = _order,
                            Offer = _offer,
                            PriceValue = isWholesale ? _offer.PriceOpt40.GetValueOrDefault(_offer.Price) : _offer.PriceOpt20.HasValue ? _offer.Price * (1 - _offer.PriceOpt20.Value / 100) : _offer.Price,
                            Quantity = 0
                        };
                        
                        _order.Items.Add(orderItem);
                    }
                    orderItem.Quantity++;
                    if (orderItem.Quantity > _offer.AvailQuantity)
                    {
                        // Нельзя превышать запасы.
                        orderItem.Quantity = _offer.AvailQuantity;
                    }
                    _order.UpdateTotals(isWholesale: isWholesale);

                    _dataContext.SaveChanges();

                    return View("Item", _order);
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult SetQtyAsAvailable(long id)
        {
            var order = _dataContext.LoadOrderDetails(id);

            if (order == null)
            {
                return RedirectToAction("Index");
            }

            foreach (var item in order.Items)
            {
                item.Quantity = item.AdminQuantity.GetValueOrDefault();
            }

            _dataContext.SaveChanges();

            return View("Item", order);
        }

        [HttpPost]
        public ActionResult UpdateOrder(Order order)
        {
            var _order = _dataContext.LoadOrderDetails(order.Id);
            foreach (var oi in order.Items)
            {
                var _orderItem = _dataContext.OrderItems.Include(i => i.Order).FirstOrDefault(o => o.Id == oi.Id);
                if (_orderItem != null)
                {

                    if (oi.Quantity <= 0)
                    {
                        //установили в 0 - удалим
                        _dataContext.OrderItems.Remove(_orderItem);
                    }
                    else if (oi.Quantity != _orderItem.Quantity || oi.DiscountTotal != _orderItem.DiscountTotal)
                    {
                        //установили новое значение
                        var offer = _dataContext.LoadOfferDetails(_orderItem.OfferId);

                        if (offer != null)
                        {
                            _orderItem.Quantity = oi.Quantity;

                            // нельзя отгрузить в убыток
                            if (_orderItem.DiscountTotal > _orderItem.PriceValue)
                            {
                                _orderItem.DiscountTotal = _orderItem.PriceValue;
                            }
                        }
                    }

                    if (!oi.AdminQuantity.HasValue || oi.AdminQuantity < 0)
                    {
                        oi.AdminQuantity = 0;
                    }
                    
                    _orderItem.AdminQuantity = oi.AdminQuantity;
                    _orderItem.AdminComment = oi.AdminComment;
                }

            }
            
            _order.AdminComment = order.AdminComment;

            var customerInfo = _dataContext.CustomerInfos.FirstOrDefault(c => c.UserName == _order.UserProfile.UserName);
            bool isWholesale = (customerInfo != null && customerInfo.Status == CustomerStatus.IsWholesale);

            _order.UpdateTotals(isWholesale: isWholesale);
            _dataContext.SaveChanges();

            return RedirectToAction("Item", new { id = _order.Id });
        }

        /// <summary>
        /// Принять заказ без участия пользователя или на доработку менеджеру
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Created(long id)
        {
            var order = _dataContext.Orders.Find(id);

            if (order == null)
            {
                return RedirectToAction("Index");
            }

            order.OrderStatus = OrderStatus.Created;
            _dataContext.SaveChanges();

            return RedirectToAction("Item", new { id = order.Id });
        }

        /// <summary>
        /// Запрос поставщикам
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult InvoiceRequest(long id, bool isInternal = false)
        {
            var order = _dataContext.Orders.Find(id);
            if (order == null)
            {
                return RedirectToAction("Index");
            }

            _dataManager.SendOrderItemsBySuppliers(order, Properties.Settings.Default.OrderEmailFrom, isInternal ? Properties.Settings.Default.OrderBillingEmail : null);
            _dataContext.SaveChanges();

            return RedirectToAction("Item", new { id = order.Id });
        }

        /// <summary>
        /// Отправить коментарии на почту
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SendWithComments(long id)
        {
            var order = _dataContext.LoadOrderDetails(id);
            if (order == null)
            {
                return RedirectToAction("Index");
            }

            var customerInfo = _dataContext.CustomerInfos.Where(p => p.UserName == order.CustomerId).FirstOrDefault();
            var tpl = new OrderMailTemplate("OrderComment", "OrderItemComments", order, customerInfo);

            MailQueue mailToCustomer = new MailQueue()
            {
                Body = tpl.Execute(),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.OrderEmailFrom,
                IsBodyHtml = true,
                Subject = string.Format(Properties.Settings.Default.OrderEmailSubject, order.Id),
                ToEmail = customerInfo != null ? customerInfo.Email : order.CustomerId
            };

            MailQueue mailToManager = new MailQueue()
            {
                Body = tpl.Execute(),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.OrderEmailFrom,
                IsBodyHtml = true,
                Subject = string.Format(Properties.Settings.Default.OrderEmailSubject, order.Id),
                ToEmail = Properties.Settings.Default.OrderBillingEmail
            };

            MailController.SendMail(mailToManager, mailToCustomer);

            return RedirectToAction("Item", new { id = order.Id });
        }

        /// <summary>
        /// Принять заявку.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Accept(long id)
        {
            var order = _dataContext.Orders.Find(id);

            if (order == null)
            {
                return RedirectToAction("Index");
            }

            order.OrderStatus = OrderStatus.Accepted;
            _dataContext.SaveChanges();

            return RedirectToAction("Item", new { id = order.Id });
        }

        /// <summary>
        /// Отклонить заявку.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Reject(long id)
        {
            var order = _dataContext.Orders.Find(id);

            if (order == null)
            {
                return RedirectToAction("Index");
            }

            order.OrderStatus = OrderStatus.Rejected;

            _dataContext.SaveChanges();

            return RedirectToAction("Item", new { id = order.Id });
        }

        /// <summary>
        /// Выписка счёта для заявки.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Billing(long id, bool isInvoice = false)
        {
            var order = _dataContext.LoadOrderDetails(id);

            if (order == null)
            {
                return RedirectToAction("Index");
            }

            order.OrderStatus = OrderStatus.PaymentWaiting;

            // Send message to customer.
            var customerInfo = _dataContext.CustomerInfos.Where(p => p.UserName == order.CustomerId).FirstOrDefault();
            var tpl = new OrderMailTemplate(isInvoice ? "Invoice" : "Order", "OrderItem", order, customerInfo);

            MailQueue mailToCustomer = new MailQueue()
            {
                Body = tpl.Execute(),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.OrderEmailFrom,
                IsBodyHtml = true,
                Subject = string.Format(Properties.Settings.Default.OrderEmailSubject, order.Id),
                ToEmail = customerInfo != null? customerInfo.Email : order.CustomerId
            };

            MailQueue mailToManager = new MailQueue()
            {
                Body = tpl.Execute(),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.OrderEmailFrom,
                IsBodyHtml = true,
                Subject = string.Format(Properties.Settings.Default.OrderEmailSubject, order.Id),
                ToEmail = Properties.Settings.Default.OrderBillingEmail
            };

            MailController.SendMail(mailToManager, mailToCustomer);

            _dataContext.SaveChanges();

            return RedirectToAction("Item", new { id = order.Id });
        }

        /// <summary>
        /// Отметка о полной оплате.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult PaymentReceived(long id)
        {
            var order = _dataContext.Orders.Find(id);

            if (order == null)
            {
                return RedirectToAction("Index");
            }

            if (order.OrderStatus.HasFlag(OrderStatus.PaymentWaiting))
            {
                order = _dataContext.LoadOrderDetails(order.Id);

                order.OrderStatus = OrderStatus.Payed;
                order.TotalPayment = order.Total;

                if (order.UserProfile != null && order.UserProfile.Affiliate != null)
                {
                    var customerInfo = _dataContext.CustomerInfos.Where(p => p.UserName == order.CustomerId).FirstOrDefault();

                    if (customerInfo == null || customerInfo.Status != CustomerStatus.IsWholesale)
                    {
                        order.UserProfile.Affiliate.PaidSum += order.TotalPayment;
                    }
                }

                foreach (var item in order.Items)
                {
                    if (order.IsPreorder)
                        item.Offer.PreorderQuantity += item.Quantity;
                    else
                        item.Offer.AvailQuantity -= item.Quantity;
                }

                _dataContext.SaveChanges();
            }

            return RedirectToAction("Item", new { id = order.Id });
        }

        /// <summary>
        /// Отметка о доставке и завершении обработки заказа.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Complete(long id)
        {
            var order = _dataContext.Orders.Find(id);

            if (order == null)
            {
                return RedirectToAction("Index");
            }

            order.OrderStatus = OrderStatus.Completed;
            order.TotalPayment = order.Total;

            foreach (var item in order.Items)
            {
                if (order.IsPreorder)
                    item.Offer.PreorderQuantity -= item.Quantity;
            }

            _dataContext.SaveChanges();

            return RedirectToAction("Item", new { id = order.Id });
        }

        List<Order> DoSearch(OrderViewParameters parameters)
        {
            var orders = _dataContext.Orders.AsQueryable();

            if (!parameters.From.HasValue && !parameters.To.HasValue)
            {
                parameters.From = DateTime.UtcNow.AddDays(-7);
                parameters.To = DateTime.UtcNow;
            }
            else
            {
                if (!parameters.From.HasValue)
                {
                    parameters.From = parameters.To.Value.AddDays(-7);
                }
                else
                {
                    if (!parameters.To.HasValue)
                    {
                        parameters.To = parameters.From.Value.AddDays(7);
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(parameters.CustomerId) || parameters.OrderNumber.HasValue)
            {
                if (!string.IsNullOrWhiteSpace(parameters.CustomerId))
                {
                    orders = orders.Where(o => o.CustomerId.Contains(parameters.CustomerId));
                }
                else
                {
                    orders = orders.Where(o => o.Id == parameters.OrderNumber);
                }
            }
            else if ((int)parameters.Status != -1)
            {
                orders = orders.Where(o => o.OrderStatus == parameters.Status);
            }
            
            return orders.OrderByDescending(x => x.CreateDate).ToList();
        }
    }
    #region Mail templates
    public class OrderMailTemplate : BaseMailTemplate
    {
        
        public OrderMailTemplate(string template, string itemTemplate, Order order, CustomerInfo customerInfo)
            : base(template)
        {
            _order = order;
            _customerInfo = customerInfo;
            _itemTemplate = itemTemplate;
        }

        Order _order;
        string _itemTemplate;
        CustomerInfo _customerInfo;

        int _index = 0;

        protected override string GetValue(string name)
        {
            switch (name)
            {
                case "CustomerInfo":
                    return _customerInfo != null ? _customerInfo.ContactName : _order.UserProfile.FirstName + " " + _order.UserProfile.LastName;
                case "Number":
                    return _order.Id.ToString();
                case "Date":
                    return _order.CreateDate.ToShortDateString();
                case "Total":
                    return _order.Total.ToString("N2");
                case "TotalAdmin":
                    return _order.Items.Sum(i => i.AdminQuantity.HasValue ? i.AdminQuantity.Value * i.PriceValue : i.Total).ToString("N2");
                case "ItemsCount":
                    return _order.Items.Count().ToString("N0");
                case "SumText":
                    return СуммаПрописью.Сумма.Пропись(_order.Total, СуммаПрописью.Валюта.Рубли);
                case "OrgName":
                    return _customerInfo != null ? _customerInfo.Organization : "";
            }

            throw new ArgumentException();
        }

        protected override IEnumerable GetEnumeration(string name)
        {
            _index = 0;
            switch (name)
            {
                case "Items":
                    return _order.Items;
            }

            throw new ArgumentException();
        }

        protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
        {
            _index++;
            return new OrderItemMailTemplate(_itemTemplate, model as OrderItem, _index);
        }
    }

    public class OrderItemMailTemplate : BaseMailTemplate
    {
        public OrderItemMailTemplate(string template, OrderItem item, int index)
            : base(template)
        {
            _item = item;
            _index = index;
        }

        OrderItem _item;
        int _index;

        protected override string GetValue(string name)
        {
            switch (name)
            {
                case "Num":
                    return _index.ToString();
                
                case "Articul":
                    return _item.Offer.Product.VendorCode;
                
                case "ProductTitle":
                    return _item.Offer.Product.CurrentLocalization.Title;
                
                case "Properties":
                    if (_item.Offer.Properties.Count > 0)
                    {
                        StringBuilder builder = new StringBuilder(": ");
                        builder.Append(string.Join(", ", _item.Offer.Properties.Select(p => string.Format("{0} {1}", p.PropertyType.CurrentLocalization.Title, p.StringValue))));
                        return builder.ToString();
                    }
                    else
                    {
                        return "";
                    }
                
                case "Price":
                    return _item.PriceValue.ToString("N2");
                
                case "Quantity":
                    return _item.Quantity.ToString("N");
                
                case "Total":
                    return _item.Total.ToString("N2");

                case "Unit":
                    return _item.Offer.Unit.CurrentLocalization.Title;

                case "AdminComment":
                    return _item.AdminComment;
                
                case "AdminQuantity":
                    return _item.AdminQuantity.HasValue ? _item.AdminQuantity.Value.ToString("N") : "-";
            }

            throw new ArgumentException();
        }

        protected override IEnumerable GetEnumeration(string name)
        {
            throw new NotImplementedException();
        }

        protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
        {
            throw new NotImplementedException();
        }
    }

    #endregion
}
