﻿using C2.DataModel.StoreModel;
using C2.WebApp.Store.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C2.WebApp.Models;
using C2.DataMapping;
using C2.DataMapping.Store;
using C2.DataManager;
using System.Data.Entity;
using C2.DataModel;
using C2.DataManager.Store;
using System.Data;

namespace C2.WebApp.Store.Areas.Admin.Controllers
{
    [Authorize(Roles="Admin")]
    public class UserController : Controller
    {
        StoreContext _storeContext;

        public UserController()
        {
            _storeContext = new StoreContext("DefaultConnection");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _storeContext.Dispose();
            }

            base.Dispose(disposing);
        }

        //
        // GET: /Admin/User/
        public ActionResult Index(UserViewParameters parameters)
        {
            UserViewModel outModel = new UserViewModel()
            {
                Parameters = parameters,
                CustomerUserInfos = DoSearch(parameters)
            };

            return View(outModel);
        }

        //
        // GET: /Admin/User/Item
        public ActionResult Item(string username)
        {
            var customerInfo = _storeContext.CustomerInfos.Include(ci => ci.DeliveryAddress).FirstOrDefault(ci => ci.UserName == username.ToLower());

            return View(customerInfo);
        }

        IList<CustomerViewModel> DoSearch(UserViewParameters parameters)
        {
            var users = from u in _storeContext.UserProfiles
                        join m in _storeContext.MembershipRecords on u.UserId equals m.UserId
                        join c in _storeContext.CustomerInfos on u.UserName equals c.UserName into info
                        from c in info.DefaultIfEmpty()
                        select new 
                        {
                            UserProfile = u,
                            CustomerInfo = c,
                            MembershipRecord = m,
                        };

            return users.ToList().Select(s => new CustomerViewModel {
                UserProfile = s.UserProfile,
                CustomerInfo = s.CustomerInfo
            }).OrderByDescending(x => x.UserProfile.MembershipRecord.CreateDate).ToList();
        }

        public ActionResult SetWholesaleOption(string username, bool isWholesaleCustomer = false)
        {
            var user = _storeContext.CustomerInfos.Include(c => c.DeliveryAddress).Where(c => c.UserName == username).FirstOrDefault();
            if (user != null)
            {
                user.Status = isWholesaleCustomer ? CustomerStatus.IsWholesale : CustomerStatus.Normal;
                _storeContext.SaveChanges();
            }

            return RedirectToAction("Index");
        }


        //
        // GET: /Admin/Spam/Edit/5

        public ActionResult Edit(int id = 0)
        {
            UserProfile userprofile = _storeContext.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // POST: /Admin/Spam/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                _storeContext.Entry(userprofile).State = EntityState.Modified;
                _storeContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userprofile);
        }
    }
}
