﻿using C2.DataManager.Store;
using C2.DataMapping;
using C2.DataMapping.Store;
using C2.DataModel;
using C2.DataModel.Spam;
using C2.DataModel.StoreModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace C2.WebApp.Store.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        public CustomerController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;

        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Unsubscribe(long id)
        {
            var dataContext = new UsersContext();

            var sid = dataContext.Sids.Where(s => s.Id == id).FirstOrDefault();
            if (sid != null)
            {
                switch (sid.Type)
                {
                    case 0:
                        // User
                        var user = dataContext.UserProfiles.Where(u => u.UserId == sid.EntityId).FirstOrDefault();
                        if (user != null)
                        {
                            if (!user.MailFlag.HasValue)
                            {
                                user.MailFlag = MailFlag.Unsubcribed;
                            }
                            else
                            {
                                user.MailFlag |= MailFlag.Unsubcribed;
                            }
                        }
                        break;

                    case 1:
                        // Visitor
                        var visitor = dataContext.Visitors.Where(u => u.Id == sid.EntityId).FirstOrDefault();
                        if (visitor != null)
                        {
                            visitor.Status |= (int)MailFlag.Unsubcribed;
                        }
                        break;
                    default:
                        break;
                }
                dataContext.SaveChanges();
            }

            return View("UnsubscribeOk");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Unsubscribe(string id)
        {
            var userContext = new UsersContext();

            var sid = userContext.Sids.Where(s => s.Sid == new Guid(id)).FirstOrDefault();
            if (sid != null)
            {
                ViewBag.Email = "";

                switch (sid.Type)
                {
                    case 0:
                        // User
                        var user = userContext.UserProfiles.Where(u => u.UserId == sid.EntityId).FirstOrDefault();
                        if (user != null)
                        {
                            ViewBag.Email = user.UserName;
                        }
                        break;

                    case 1:
                        // Visitor
                        var visitor = userContext.Visitors.Where(v => v.Id == sid.EntityId).FirstOrDefault();
                        if (visitor != null)
                        {
                            ViewBag.Email = visitor.Email;
                        }
                        break;
                    default:
                        break;
                }
            }

            return View(sid);
            
        }

        //
        // GET: /Customer/

        public ActionResult Index()
        {
            var customerInfo = _dataContext.CustomerInfos.Include(ci => ci.DeliveryAddress).FirstOrDefault(ci => ci.UserName == WebSecurity.CurrentUserName);
            if (customerInfo == null)
            {
                var user = _dataContext.UserProfiles.FirstOrDefault(u => u.UserName == WebSecurity.CurrentUserName);
                customerInfo = new CustomerInfo()
                {
                    DeliveryAddress = new DataModel.Address(),
                    ContactName = user.FirstName + " " + user.LastName,
                    Phone = user.Phone,
                    Email = user.UserName,
                };
            }

            return View(customerInfo);
        }

        //
        // POST: /Customer/Apply

        public ActionResult Apply(CustomerInfo customerInfo)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpException(404, "ИНН"); //
            }

            var exsistsModel = _dataContext.CustomerInfos.Include(ci => ci.DeliveryAddress).FirstOrDefault(ci => ci.UserName == WebSecurity.CurrentUserName);

            if (exsistsModel != null)
            {
                exsistsModel.DeliveryAddress.Building = customerInfo.DeliveryAddress.Building;
                exsistsModel.DeliveryAddress.City = customerInfo.DeliveryAddress.City;
                exsistsModel.DeliveryAddress.Country = customerInfo.DeliveryAddress.Country;
                //customerInfo.DeliveryAddress.Location = model.DeliveryAddress.Location;
                exsistsModel.DeliveryAddress.Notes = customerInfo.DeliveryAddress.Notes;
                exsistsModel.DeliveryAddress.PostCode = customerInfo.DeliveryAddress.PostCode;
                exsistsModel.DeliveryAddress.Region = customerInfo.DeliveryAddress.Region;
                exsistsModel.DeliveryAddress.Room = customerInfo.DeliveryAddress.Room;
                exsistsModel.DeliveryAddress.Special = customerInfo.DeliveryAddress.Special;
                exsistsModel.DeliveryAddress.Street = customerInfo.DeliveryAddress.Street;
                exsistsModel.ContactName = customerInfo.ContactName;
                exsistsModel.Email = customerInfo.Email;
                exsistsModel.Phone = customerInfo.Phone;
                exsistsModel.Requisites = customerInfo.Requisites;
                exsistsModel.INN = customerInfo.INN;
                exsistsModel.KPP = customerInfo.KPP;
                exsistsModel.OGRN = customerInfo.OGRN;
                exsistsModel.Organization = customerInfo.Organization;
            }
            else
            {
                exsistsModel = customerInfo;
                exsistsModel.UserName = WebSecurity.CurrentUserName;
                customerInfo.DeliveryAddress.Id = BaseContext.GetId();
                _dataContext.CustomerInfos.Add(customerInfo);
                _dataContext.Addresses.Add(customerInfo.DeliveryAddress);
            }


            exsistsModel.Status = CustomerStatus.IsWholesale;

            _dataContext.SaveChanges();

            return PartialView("_CustomerInfo", customerInfo);
        }
    }
}
