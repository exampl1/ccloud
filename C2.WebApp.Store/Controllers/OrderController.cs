﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C2.DAL;
using C2.DAL.Entity;
using C2.DataMapping.Store;
using C2.DataModel.StoreModel;
using Newtonsoft.Json;
using C2.WebApp.Store.Models;
using C2.DataManager.Store;
using System.Globalization;
using C2.DataModel;
using C2.DataMapping;
using WebMatrix.WebData;
using System.Text;
using C2.AppController;
using System.Collections;
using C2.WebApp.Controllers;
using System.Security.Principal;


namespace C2.WebApp.Store.Controllers
{
    public class OrderController : Controller
    {
        const string OrderModelKey = "882B7630-EC21-490B-A1F8-990F76E2F460";

        public OrderController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;

        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }
        
        //
        // GET: /Order/

        /// <summary>
        /// Список заказов пользователя.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(OrderViewParameters parameters)
        {
            parameters.CustomerId = User.Identity.Name;

            OrderViewModel outModel = new OrderViewModel()
            {
                Parameters = parameters,
                Orders = DoSearch(parameters)
            };

            return View(outModel);
        }

        [HttpPost]
        public ActionResult Search(OrderViewParameters parameters)
        {
            return PartialView("_OrderList", DoSearch(parameters));
        }

        List<Order> DoSearch(OrderViewParameters parameters)
        {
            var orders = _dataContext.Orders.AsQueryable();

            if (!parameters.From.HasValue && !parameters.To.HasValue)
            {
                parameters.From = DateTime.UtcNow.AddDays(-7);
                parameters.To = DateTime.UtcNow;
            }
            else
            {
                if (!parameters.From.HasValue)
                {
                    parameters.From = parameters.To.Value.AddDays(-7);
                }
                else
                {
                    if (!parameters.To.HasValue)
                    {
                        parameters.To = parameters.From.Value.AddDays(7);
                    }
                }
            }

            orders = orders.Where(o => o.CustomerId == WebSecurity.CurrentUserName);
            
            if (parameters.OrderNumber.HasValue)
            {
                orders = orders.Where(o => o.Id == parameters.OrderNumber);
            }

            return orders.OrderByDescending(x => x.CreateDate).ToList();
        }

        //
        // GET: /Order/{id}

        /// <summary>
        /// Просмотр заказа.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Item(long id)
        {
            var order = _dataContext.LoadOrderDetails(id);

            if (order == null)
            {
                return RedirectToAction("Index");
            }

            return View(order);
        }

        //
        // GET: /Order/BeginCreate

        /// <summary>
        /// Вывод инофрмации о текущем состоянии корзины и предложение оформить заказ.
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            // Check min order value.
            var basket = BasketController.GetOrder(_dataContext, Session);
            var customerInfo = ViewData.GetPageModel().CustomerInfo;
            bool isWholesale = (customerInfo != null && customerInfo.Status == CustomerStatus.IsWholesale);

            if (isWholesale && basket.Total < Properties.Settings.Default.MinOrderAmount)
            {
                return RedirectToAction("Index", "Product");
            }

            var model = new OrderCreationModel()
            {
                Stage = OrderCreationStage.CollectUserInfo
            };

            if (User != null)
            {
                model.UserMail = User.Identity.Name;
            }

            Session[OrderModelKey] = model;

            return View(model);
        }

        [HttpPost]
        public ActionResult BackTo(OrderCreationStage toStage)
        {
            var model = CreateModel();

            if (model == null)
            {
                model = new OrderCreationModel()
                {
                    Stage = OrderCreationStage.CollectUserInfo
                };

                if (User != null)
                {
                    model.UserMail = User.Identity.Name;
                }
            }
            else
            {
                if (model.Stage > OrderCreationStage.CollectUserInfo)
                {
                    model.Stage = (OrderCreationStage)(model.Stage - 1);
                }
            }

            Session[OrderModelKey] = model;

            return PartialView("_OrderForm", model);
        }

        [HttpPost]
        public ActionResult CollectUserInfo(string customerType,
            string userMail = null,
            string anonUserMail = null,
            string userPassword = null,
            string contactPhone = null,
            string contactDate = null)
        {
            var model = CreateModel();
          
            if (model == null)
            {
                return RedirectToAction("Create");
            }

            switch (customerType)
            {
                case "U":
                    model.UserIdentifyMethod = customerType;
                    if (WebSecurity.IsAuthenticated)
                    {
                        model.UserMail = WebSecurity.CurrentUserName;
                    }
                    else
                    {
                        model.UserMail = userMail;
                        if (!WebSecurity.Login(userMail, userPassword))
                        {
                            ModelState.AddModelError("", "Пользователь не существует или введён неверный пароль.");
                            return View("Create", model);
                        }
                        return RedirectToAction("Create");
                    }

                    model.Stage = OrderCreationStage.CollectDeliveryInfo;
                    break;
                case "A":
                    model.UserIdentifyMethod = customerType;
                    model.UserMail = anonUserMail;
                    if (string.IsNullOrWhiteSpace(anonUserMail))
                    {
                        ModelState.AddModelError("", "Укажите электронный адрес пользователя.");
                        return View("Create", model);
                    }
                    if (WebSecurity.UserExists(anonUserMail))
                    {
                        ModelState.AddModelError("", "Пользователь уже существует, укажите другой адрес или войдите как зарегистрированный пользователь.");
                        model.UserIdentifyMethod = "U";
                        return View("Create", model);
                    }
                    model.Stage = OrderCreationStage.CollectDeliveryInfo;
                    break;
                default:
                    return RedirectToAction("Create");
            }

            // Найдём настройки по доставке:
            var deliveryInfo = _dataContext.CustomerInfos.Include(ci => ci.DeliveryAddress).FirstOrDefault(ci => ci.UserName == model.UserMail);
            if (deliveryInfo != null)
            {
                model.DeliveryAddress = new DeliveryAddressModel
                {
                    Contacts = deliveryInfo.Phone,
                    ContactPerson = deliveryInfo.ContactName,
                    City = string.Format("{0} {1}",
                        deliveryInfo.DeliveryAddress.Country,
                        deliveryInfo.DeliveryAddress.City).Trim(),
                    Region =deliveryInfo.DeliveryAddress.Region,
                    ZIP = deliveryInfo.DeliveryAddress.PostCode,
                    Address = string.Format("{0}, {1}-{2} {3}",
                        deliveryInfo.DeliveryAddress.Street,
                        deliveryInfo.DeliveryAddress.Building,
                        deliveryInfo.DeliveryAddress.Room,
                        deliveryInfo.DeliveryAddress.Special).Trim()
                };
            }

            Session[OrderModelKey] = model;

            return View("Create", model);
        }

        [HttpPost]
        public ActionResult CollectDeliveryInfo(DeliveryAddressModel deliveryAddress)
        {
            var model = CreateModel();

            if (model == null)
            {
                return RedirectToAction("Create");
            }

            if (ModelState.IsValid)
            {
                model.DeliveryAddress = deliveryAddress;
                model.Stage = OrderCreationStage.Confirmation;
                var basket = BasketController.GetOrder(_dataContext, Session);
                model.TotalAmount = basket.Total;

                Session[OrderModelKey] = model;
            }

            return View("Create", model);
        }


        /// <summary>
        /// Создание заказа.
        /// </summary>
        /// <returns></returns>
        public ActionResult DoCreate(string notes)
        {
            var model = CreateModel();
            if (model == null)
            {
                return RedirectToAction("Create");
            }

            if (model.UserIdentifyMethod == "A")
            {
                // Create user
                var tempPassword = Guid.NewGuid().ToString();
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserMail, tempPassword, new { FirstName = model.DeliveryAddress.ContactPerson, Phone = model.DeliveryAddress.Contacts });
                    C2.WebApp.Controllers.AccountController.Affiliate(new WebApp.Models.RegisterModel
                    {
                        UserName = model.UserMail
                    }, HttpContext);
                }
                catch
                {
                    // TODO: 
                    return View("Create", model);
                }

                // And login
                WebSecurity.Login(model.UserMail, tempPassword);
                HttpContext.User = new GenericPrincipal(new GenericIdentity(model.UserMail), new string[] { });
                
            }

            Order order = BasketController.GetOrder(_dataContext, Session);

            if (order.OrderStatus.HasFlag(OrderStatus.Preorder))
            {
                order.IsPreorder = true;
                order.AdminComment = "Пердзаказ!";
            }

            order.OrderStatus = OrderStatus.Created;
            
            var customerInfo = ViewData.GetPageModel().CustomerInfo;
            bool isWholesale = (customerInfo != null && customerInfo.Status == CustomerStatus.IsWholesale);

            if (isWholesale && order.Total < Properties.Settings.Default.MinOrderAmount)
            {
                return RedirectToAction("Index", "Product");
            }

            order.UpdateTotals(isWholesale: isWholesale);
            order.Delivery = model.DeliveryAddress.ToString();
            order.UserComment = notes;

            _dataContext.SaveChanges();

            // Очищаем корзину
            Session.Remove("basket");
            
            // Также необходимо очистить пред-загруженные данные:
            var pageModel = ViewData.GetPageModel();
            pageModel.Basket = new Order();

            Session[OrderModelKey] = null;

            model.Stage = OrderCreationStage.Complete;
            model.OrderId = order.Id;

            // Send message to customer.
            MailQueue mailToCustomer = new MailQueue()
            {
                Body = GetCustomerMailBody(order),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.OrderEmailFrom,
                IsBodyHtml = true,
                Subject = string.Format(Properties.Settings.Default.OrderEmailSubject, model.OrderId),
                ToEmail = model.UserMail
            };

            MailQueue mailToManager = new MailQueue()
            {
                Body = GetManagerMailBody(order),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.OrderEmailFrom,
                IsBodyHtml = true,
                Subject = string.Format(Properties.Settings.Default.OrderEmailSubject, model.OrderId),
                ToEmail = Properties.Settings.Default.ManagerEmail
            };

            MailQueue mailToSMS = new MailQueue()
            {
                Body = string.Format("заказ от {0} на сумму {1}", order.CustomerId, order.Total.ToString("N2")),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.OrderEmailFrom,
                IsBodyHtml = false,
                Subject = string.Format(Properties.Settings.Default.OrderEmailSubject, model.OrderId),
                ToEmail = Properties.Settings.Default.SMS2Email,
            };

            MailController.SendMail(mailToCustomer, mailToManager, mailToSMS);

            return View("Create", model);
        }

        #region Mail templates
        public class OrderMailTemplate : BaseMailTemplate
        {
            public OrderMailTemplate(Order order, CustomerInfo customerInfo)
                : base("Preorder")
            {
                _order = order;
                _customerInfo = customerInfo;
            }

            Order _order;

            CustomerInfo _customerInfo;

            int _index = 0;

            protected override string GetValue(string name)
            {
                switch (name)
                {
                    case "CustomerInfo":
                        return _customerInfo != null ? _customerInfo.ContactName : _order.CustomerId;
                    case "Number":
                        return _order.Id.ToString();
                    case "Date":
                        return _order.CreateDate.ToShortDateString();
                    case "Total":
                        return _order.Total.ToString("N2");
                    case "ItemsCount":
                        return _order.Items.Count().ToString("N0");
                    case "SumText":
                        return СуммаПрописью.Сумма.Пропись(_order.Total, СуммаПрописью.Валюта.Рубли);
                    case "OrgName":
                        return _customerInfo != null ? _customerInfo.Organization : "";
                }

                throw new ArgumentException();
            }

            protected override IEnumerable GetEnumeration(string name)
            {
                switch (name)
                {
                    case "Items":
                        return _order.Items;
                }

                throw new ArgumentException();
            }

            protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
            {
                _index++;
                return new OrderItemMailTemplate(model as OrderItem, _index);
            }
        }

        class OrderItemMailTemplate : BaseMailTemplate
        {
            public OrderItemMailTemplate(OrderItem item, int index)
                : base("OrderItem")
            {
                _item = item;
                _index = index;
            }

            OrderItem _item;

            int _index;

            protected override string GetValue(string name)
            {
                switch (name)
                {
                    case "Num":
                        return _index.ToString();
                    case "Articul":
                        return _item.Offer.Product.VendorCode;
                    case "ProductTitle":
                        return _item.Offer.Product.CurrentLocalization.Title;
                    case "Properties":
                        if (_item.Offer.Properties.Count > 0)
                        {
                            StringBuilder builder = new StringBuilder("<ul>");
                            foreach (var p in _item.Offer.Properties)
                            {
                                builder.AppendFormat("<li>{0} {1}</li>", p.PropertyType.CurrentLocalization.Title, p.StringValue);
                            }
                            builder.Append("</ul>");
                            return builder.ToString();
                        }
                        else
                        {
                            return "";
                        }
                    case "Price":
                        return _item.PriceValue.ToString("N2");

                    case "Quantity":
                        return _item.Quantity.ToString("N");

                    case "Total":
                        return _item.Total.ToString("N2");

                    case "Unit":
                        return _item.Offer.Unit.CurrentLocalization.Title;

                    case "AdminComment":
                        return _item.AdminComment;
                }

                throw new ArgumentException();
            }

            protected override IEnumerable GetEnumeration(string name)
            {
                throw new NotImplementedException();
            }

            protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        public string GetCustomerMailBody(Order order)
        {
            // Найдём доп. информацию о заказчике:
            var customerInfo = _dataContext.CustomerInfos.FirstOrDefault(ci => ci.UserName == order.CustomerId);

            var tpl = new OrderMailTemplate(order, customerInfo);
            return tpl.Execute();
        }

        string GetManagerMailBody(Order order)
        {
            return GetCustomerMailBody(order);
        }

        OrderCreationModel CreateModel()
        {
            return Session[OrderModelKey] as OrderCreationModel;
        }
    }
}
