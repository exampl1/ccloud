﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C2.DAL;
using C2.DAL.Entity;
using C2.DataMapping.Store;
using C2.DataModel.StoreModel;
using Newtonsoft.Json;
using C2.WebApp.Store.Models;
using C2.DataManager.Store;
using System.Globalization;
using C2.DataModel;
using C2.AppModel;

namespace C2.WebApp.Store.Controllers
{
    public class ProductController : Controller
    {
        public ProductController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;

        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }


        /// <summary>
        /// Метод для изменение веса товара в списке. 
        /// TODO: перенести в админский контрол
        /// </summary>
        /// <param name="id"></param>
        /// <param name="order"></param>
        /// <param name="categoryNodeId"></param>
        /// <param name="page"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles="Admin")]
        public ActionResult SetOrder(long id, int order, SelectProductParams @params)
        {
            var product = _dataContext.Products.Where(p => p.Id == id).FirstOrDefault();
            if (product != null)
            {
                product.Order = order;
                _dataContext.SaveChanges();
            }

            if (Request.IsAjaxRequest())
            {
                ExtendSelectParams(@params);

                var productModel = ViewData.GetPageModel();
                productModel.SelectResult = _dataManager.SelectProducts(@params);
                productModel.CategoryNodeId = @params.CategoryNodeId;
                productModel.SelectParams = @params;

                Request.RequestContext.RouteData.Values["action"] = "Index";

                return PartialView("Index", productModel);
            }

            return RedirectToAction("Item", new { id = id });
        }

        [HttpGet]
        public ActionResult Selling(SelectProductParams @params)
        {
            @params.Selling = 30;
            return Index(@params);
        }

        //
        // GET: /Product/{SEO-Name-of-Product-Group}/{categoryNodeId}/?{pageIndex=...}
        [HttpGet]
        public ActionResult Index(SelectProductParams @params)
        {
            ExtendSelectParams(@params);

            var productModel = ViewData.GetPageModel();
            var tree = _dataManager.GetProductGroupClassifier();
            var selected = tree.AllNodes.Where(n => n.NodeId == @params.CategoryNodeId).ToList();
            productModel.CategoryNodeId = @params.CategoryNodeId;

            productModel.SelectParams = @params;

            // Загрузка каталога
            productModel.Catalog = new WebApp.Models.TreeViewModel("_ProductGroupHeader",
                tree.Roots,
                null,
                selected);

            if (selected.Count() > 0)
            {
                var classifier = selected.First();
                var group = classifier.Value as ProductGroup;
                var code = group.Code;

                productModel.PageContent = new PageContent
                {
                    Title = group.CurrentLocalization.Title,
                    Text = group.CurrentLocalization.Description,
                    HtmlDescription = group.CurrentLocalization.Description,
                    HtmlTitle = group.CurrentLocalization.Title,
                    HtmlKeywords = group.CurrentLocalization.Title,
                };

                if (classifier.Parent != null)
                {
                    productModel.PageContent.Title = classifier.Parent.Caption + ". " + productModel.PageContent.Title;
                    productModel.PageContent.HtmlTitle = classifier.Parent.Caption + " " + productModel.PageContent.HtmlTitle;
                    productModel.PageContent.HtmlKeywords = classifier.Parent.Caption + ", " + productModel.PageContent.HtmlKeywords;
                }

                productModel.AddPageContent = _dataContext.PageContents.Where(p => p.HtmlId == code).FirstOrDefault();
                if (productModel.AddPageContent != null)
                {
                    productModel.PageContent.Title += " " + productModel.AddPageContent.Title;
                    productModel.PageContent.HtmlTitle += " " + productModel.AddPageContent.HtmlTitle;
                    productModel.PageContent.HtmlKeywords += ", " + productModel.PageContent.HtmlKeywords;
                }
            }
            else
            {
                var action = RouteData.Values["action"].ToString().ToLower();
                productModel.PageContent = _dataContext.PageContents.Where(p => p.HtmlId == action).FirstOrDefault();
            }

            productModel.SelectResult = _dataManager.SelectProducts(@params);

            return View("Index", productModel);
        }

        //
        // POST: /Product/Search

        [HttpPost]
        public ActionResult Search(SelectProductParams @params)
        {
            ExtendSelectParams(@params);

            return PartialView("~/Views/Product/ProductCollection.cshtml", _dataManager.SelectProducts(@params));
        }

        //
        // GET: /Product/Item/SEO-Name-of-Product/{id}
        public ActionResult Item(long id)
        {
            if (id < 0)
            {
                //По большей части для тестирования обработки ошибок на сайте
                throw new ArgumentException();
            }

            Product product, next, prev;
            ICollection<ClassifierNode> selectedNodes;
            if (_dataManager.TryFindProduct(id, out product, out selectedNodes, out next, out prev))
            {
                var productModel = ViewData.GetPageModel();
                
                var tree = _dataManager.GetProductGroupClassifier();
                productModel.Catalog = new WebApp.Models.TreeViewModel("_ProductGroupHeader",
                            tree.Roots,
                            null,
                            tree.AllNodes.Where(n => selectedNodes.Any(sn => sn.Id == n.NodeId)).ToList());

                var model = new ProductItemViewModel
                {
                    Product = product,
                    Next = next,
                    Previous = prev
                };

                return View(model);
            }
            else
            {
                throw new HttpException(404, string.Format("Продукт {0} не найден", id));
            }
        }

        void ExtendSelectParams(SelectProductParams @params)
        {
            @params.CurrencyCode = Properties.Settings.Default.DefaultCurrencyCode;
            @params.DefaultCultureCode = Properties.Settings.Default.DefaultCultureCode;
            @params.UserCultureCode = CultureInfo.CurrentUICulture.TwoLetterISOLanguageName;
        }
    }
}
