﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Store.Models
{
    public class ContentPageModel
    {
        public List<PageContent> AllPagesContent { get; set; }
    }
}