﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace C2.WebApp.Store.Models
{
    public class OrderCreationModel
    {
        public OrderCreationStage Stage
        {
            get;
            set;
        }

        public string UserMail
        {
            get;
            set;
        }

        public string UserIdentifyMethod
        {
            get;
            set;
        }

        public DeliveryAddressModel DeliveryAddress
        {
            get;
            set;
        }

        public decimal TotalAmount
        {
            get;
            set;
        }

        public long OrderId
        {
            get;
            set;
        }
    }

    public enum OrderCreationStage
    {
        CollectUserInfo,

        CollectDeliveryInfo,

        //CollectPaymentInfo,

        Confirmation,

        Complete
    }

    public class DeliveryAddressModel
    {
        [Required(ErrorMessage = "Укажите город доставки")]
        [Display(Name = "Город")]
        [StringLength(50)]
        public string City
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Укажите область")]
        [StringLength(50)]
        [Display(Name = "Область")]
        public string Region
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Индекс")]
        [StringLength(10)]
        [Display(Name = "Индекс")]
        public string ZIP
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Укажите адрес доставки")]
        [Display(Name = "Адрес")]
        [StringLength(200)]
        public string Address
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Укажите телефон для связи")]
        [StringLength(50)]
        [Display(Name = "Телефон")]
        public string Contacts
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Получатель")]
        [StringLength(100)]
        [Display(Name = "Получатель (ФИО полностью)")]
        public string ContactPerson
        {
            get;
            set;
        }

        public override string ToString()
        {
            return string.Format("Область: {1}, Город: {2},{0}Адрес: {3}{0}{4},{0} Телефон: {5},{0}Контактное лицо: {6}{0}", Environment.NewLine, Region, City, Address, ZIP, Contacts, ContactPerson);
        }

        public string ToHtmlString()
        {
            return string.Format("<p>Область: {0},</p><p>Город: {1},</p><p>Адрес: {2},</p><p>Индекс: {3}</p><p>Телефон: {4},</p><p>Контактное лицо: {5}</p>", Region, City, Address, ZIP, Contacts, ContactPerson);
        }
    }
}