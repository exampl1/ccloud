﻿using C2.DataModel.MediaModel;
using C2.DataModel.StoreModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Store.Models
{
    public class ProductViewModel
    {
        public Product Product
        {
            get;
            set;
        }

        public Offer Offer
        {
            get;
            set;
        }

        public decimal InBasketQuantity
        {
            get;
            set;
        }
    }

    public class ProductViewModelMetadata
    {

    }
}