﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C2.DataManager;
using C2.DataMapping;
using C2.DataModel;
using C2.WebApp.Models;

namespace C2.WebApp.Store.Areas.Admin.Controllers
{
    public class SupplierController : Controller
    {
        public SupplierController()
        {
            _dataContext = new AppContext("DefaultConnection");
            _dataManager = new AppDataManager(_dataContext);
        }

        AppContext _dataContext;
        AppDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }


        //
        // GET: /Admin/Supplier/
        public ActionResult Index()
        {
            return View(_dataContext.Subjects.ToList());
        }

        //
        // GET: /Admin/Supplier/Create
        public ActionResult Create(long? id)
        {
            var model = new SupplierViewModel();
            if (id.HasValue) {
                model.Supplier = _dataContext.Suppliers.Find(id.Value);
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult Create(SupplierViewModel model)
        {
            //model.Supplier.

            //model.Localization.Add(new SubjectLocalization { CultureCode = "ru-RU" });
            
            return View(model);
        }


    }
}
