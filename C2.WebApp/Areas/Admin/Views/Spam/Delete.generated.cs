﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18047
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace C2.WebApp.Areas.Admin.Views.Spam
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "1.5.4.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Areas/Admin/Views/Spam/Delete.cshtml")]
    public partial class Delete : System.Web.Mvc.WebViewPage<C2.DataModel.UserProfile>
    {
        public Delete()
        {
        }
        public override void Execute()
        {
            
            #line 3 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
  
    ViewBag.Title = "Delete";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<h2>Delete</h2>\r\n\r\n<h3>Are you sure you want to delete this?</h3>\r\n<fieldset>" +
"\r\n    <legend>UserProfile</legend>\r\n\r\n    <div");

WriteLiteral(" class=\"display-label\"");

WriteLiteral(">\r\n");

WriteLiteral("         ");

            
            #line 14 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.UserName));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n    <div");

WriteLiteral(" class=\"display-field\"");

WriteLiteral(">\r\n");

WriteLiteral("        ");

            
            #line 17 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
   Write(Html.DisplayFor(model => model.UserName));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n\r\n    <div");

WriteLiteral(" class=\"display-label\"");

WriteLiteral(">\r\n");

WriteLiteral("         ");

            
            #line 21 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.FirstName));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n    <div");

WriteLiteral(" class=\"display-field\"");

WriteLiteral(">\r\n");

WriteLiteral("        ");

            
            #line 24 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
   Write(Html.DisplayFor(model => model.FirstName));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n\r\n    <div");

WriteLiteral(" class=\"display-label\"");

WriteLiteral(">\r\n");

WriteLiteral("         ");

            
            #line 28 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.MiddleName));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n    <div");

WriteLiteral(" class=\"display-field\"");

WriteLiteral(">\r\n");

WriteLiteral("        ");

            
            #line 31 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
   Write(Html.DisplayFor(model => model.MiddleName));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n\r\n    <div");

WriteLiteral(" class=\"display-label\"");

WriteLiteral(">\r\n");

WriteLiteral("         ");

            
            #line 35 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.LastName));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n    <div");

WriteLiteral(" class=\"display-field\"");

WriteLiteral(">\r\n");

WriteLiteral("        ");

            
            #line 38 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
   Write(Html.DisplayFor(model => model.LastName));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n\r\n    <div");

WriteLiteral(" class=\"display-label\"");

WriteLiteral(">\r\n");

WriteLiteral("         ");

            
            #line 42 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Phone));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n    <div");

WriteLiteral(" class=\"display-field\"");

WriteLiteral(">\r\n");

WriteLiteral("        ");

            
            #line 45 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Phone));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n</fieldset>\r\n");

            
            #line 48 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
 using (Html.BeginForm()) {
    
            
            #line default
            #line hidden
            
            #line 49 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
Write(Html.AntiForgeryToken());

            
            #line default
            #line hidden
            
            #line 49 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
                            

            
            #line default
            #line hidden
WriteLiteral("    <p>\r\n        <input");

WriteLiteral(" type=\"submit\"");

WriteLiteral(" value=\"Delete\"");

WriteLiteral(" /> |\r\n");

WriteLiteral("        ");

            
            #line 52 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
   Write(Html.ActionLink("Back to List", "Index"));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </p>\r\n");

            
            #line 54 "..\..\Areas\Admin\Views\Spam\Delete.cshtml"
}

            
            #line default
            #line hidden
        }
    }
}
#pragma warning restore 1591
