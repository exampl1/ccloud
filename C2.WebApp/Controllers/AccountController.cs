﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using C2.WebApp.Models;
using C2.DataModel;
using C2.DataMapping;
using C2.AppController;
using System.Web.Routing;
using System.Net;
using System.IO;
using System.Text;
using System.Collections;
using C2.DataManager;
using System.Data.Entity;

namespace C2.WebApp.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {

        public ActionResult Partner()
        {
            
            using (var context = new UsersContext())
            {
                var aff = context.Affiliates
                    .Include(t => t.AffiliateUsers)
                    //.Include(t => t.User)
                    .Where(t => t.UserId == WebSecurity.CurrentUserId).FirstOrDefault();

                if (aff == null)
                {
                    aff = context.Affiliates.Add(new Affiliate
                    {
                        Code = AppDataManager.GetId(),
                        UserId = WebSecurity.CurrentUserId,
                        ParentUserId = null,
                    });

                    context.SaveChanges();
                }
                else
                {
                    // используем поля модели текущего аффилейта для отображения суммарных данным по аффилейтам
                    aff.BasketCount += aff.AffiliateUsers.Sum(a => a.BasketCount);
                    aff.BasketSum += aff.AffiliateUsers.Sum(a => a.BasketSum);
                    
                    // суммируем к собственным покупкам, покупки аффилейтов
                    aff.PaidSum += aff.AffiliateUsers.Sum(a => a.PaidSum);
                }

                return View(aff);
            }
        }


        //
        // GET: /Account/ForgotPassword

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(string email)
        {
            bool success = false;
            if (WebSecurity.UserExists(email))
            {
                string token = WebSecurity.GeneratePasswordResetToken(email);
                
                // todo: send token to user.
                RestorePasswordMail mailTemplate = new RestorePasswordMail(Url.Action("ResetPassword", "Account", new RouteValueDictionary { { "token", token } }, Request.Url.Scheme, Request.Url.Host));

                MailQueue mail = new MailQueue
                    {
                        Body = mailTemplate.Execute(),
                        Created = DateTime.UtcNow,
                        FromEmail = Properties.Settings.Default.FromEmail,
                        IsBodyHtml = true,
                        Priority = 0,
                        Status = 0,
                        Subject = string.Format("Ключ для восстановления пароля", Request.Url.Host),
                        ToEmail = email
                    };
                MailController.SendMail(mail);

                string resetPasswordUrl = Url.Action("ResetPassword", new { token = token });
                success = true;
            }
            ViewBag.Success = success;

            return View("ForgotPasswordComplete");
        }

        class RestorePasswordMail : BaseMailTemplate
        {
            public RestorePasswordMail(string url)
                : base("RestorePassword")
            {
                _url = url;
            }

            string _url;

            protected override string GetValue(string name)
            {
                switch (name)
                {
                    case "Url":
                        return _url;
                }

                throw new ArgumentException();
            }

            protected override IEnumerable GetEnumeration(string name)
            {
                throw new NotImplementedException();
            }

            protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
            {
                throw new NotImplementedException();
            }
        }

        //
        // GET: /Account/ResetPassword

        [AllowAnonymous]
        public ActionResult ResetPassword(string token)
        {
            ViewBag.Token = token;
            
            return View("ResetPassword");
        }

        //
        // POST: /Account/ResetPassword
        
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ResetPassword(string token, string newPassword)
        {
            ViewBag.Success = WebSecurity.ResetPassword(token, newPassword);

            return View("ResetPasswordComplete");
        }

        [ActionName("Profile")]
        public ActionResult ProfileManagement()
        {
            using (var context = new UsersContext())
            {
                var user = context.UserProfiles.Find(WebSecurity.CurrentUserId);

                return View(user);
            }
        }

        [HttpPost]
        [ActionName("Profile")]
        public ActionResult ProfileManagement(UserProfile userProfile)
        {
            using (var context = new UsersContext())
            {
                var currentProfile = context.UserProfiles.Find(WebSecurity.CurrentUserId);
                currentProfile.FirstName = userProfile.FirstName;
                currentProfile.LastName = userProfile.LastName;
                currentProfile.MiddleName = userProfile.MiddleName;
                currentProfile.Phone = userProfile.Phone;

                context.SaveChanges();

                return View(currentProfile);
            }
        }

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Адрес e-mail или пароль некорректны.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { FirstName = model.FirstName, MiddleName = model.MiddleName, LastName = model.LastName, Phone = model.Phone });
                    WebSecurity.Login(model.UserName, model.Password);

                    Affiliate(model, HttpContext);

                    SendWelcomeMail(model);

                    return RedirectToAction("Get", "Home", new { id = "wholesale" });
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public static void Affiliate(RegisterModel model, HttpContextBase httpContext)
        {
            if (httpContext.Request.Cookies != null && httpContext.Request.Cookies.AllKeys.Contains("partner"))
            {
                string partnerValue = httpContext.Request.Cookies["partner"].Value;
                long partner = 0;
                long.TryParse(partnerValue, out partner);
                if (partner > 0)
                {
                    using (var context = new UsersContext())
                    {
                        var parentAff = context.Affiliates.FirstOrDefault(a => a.Code == partner);
                        if (parentAff != null)
                        {
                            parentAff.RegistrationCount++;

                            var aff = context.Affiliates.Add(new Affiliate
                            {
                                Code = AppDataManager.GetId(),
                                UserId = WebSecurity.GetUserId(model.UserName),
                                ParentUserId = parentAff.UserId,
                            });

                            if (aff.UserId > 0)
                            {
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
        }

        public static void SendWelcomeMail(RegisterModel model)
        {
            var userName = string.IsNullOrWhiteSpace(model.FirstName) ? model.UserName : string.Format("{0} {1}", model.FirstName, model.MiddleName);
            string subject = Properties.Settings.Default.WelcomeMailSubject;

            WelcomeMailTemplate mailTemplate = new WelcomeMailTemplate(userName);
            
            MailController.SendMail(new MailQueue()
            {
                Body = mailTemplate.Execute(),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.FromEmail,
                IsBodyHtml = true,
                Priority = 0,
                Status = 0,
                Subject = subject,
                ToEmail = model.UserName
            },
            new MailQueue()
            {
                Body = mailTemplate.Execute(),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.FromEmail,
                IsBodyHtml = true,
                Priority = 0,
                Status = 0,
                Subject = subject,
                ToEmail = Properties.Settings.Default.RegistrationEmail
            },
            new MailQueue()
            {
                Body = new WelcomeSMSTemplate(userName, model.Phone).Execute(),
                Created = DateTime.UtcNow,
                FromEmail = Properties.Settings.Default.FromEmail,
                IsBodyHtml = false,
                Priority = 0,
                Status = 0,
                Subject = "Новая регистрация",
                ToEmail = Properties.Settings.Default.SMS2Email
            });

        }

        class WelcomeMailTemplate : BaseMailTemplate
        {
            public WelcomeMailTemplate(string userName)
                : base("Welcome")
            {
                _userName = userName;
            }

            string _userName;

            protected override string GetValue(string name)
            {
                switch (name)
                {
                    case "UserName":
                        return _userName;
                }

                throw new ArgumentException();
            }

            protected override IEnumerable GetEnumeration(string name)
            {
                throw new NotImplementedException();
            }

            protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
            {
                throw new NotImplementedException();
            }
        }

        class WelcomeSMSTemplate : BaseMailTemplate
        {
            public WelcomeSMSTemplate(string name, string phone)
                : base("WelcomeSMS")
            {
                _Name = name;
                _Phone = phone;
            }

            string _Name;
            string _Phone;

            protected override string GetValue(string name)
            {
                switch (name)
                {
                    case "UserName":
                        return _Name;
                        break;
                    case "Phone":
                        return _Phone;
                        break;
                }

                throw new ArgumentException();
            }

            protected override IEnumerable GetEnumeration(string name)
            {
                throw new NotImplementedException();
            }

            protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
            {
                throw new NotImplementedException();
            }
        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Пароль успешно изменён."
                : message == ManageMessageId.SetPasswordSuccess ? "Пароль установлен."
                : message == ManageMessageId.RemoveLoginSuccess ? "Внешний сервис подключен."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Указан неверный текущий пароль или новый пароль некорректен.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                return RedirectToLocal(returnUrl);
            }

            if (User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        {
            string provider = null;
            string providerUserId = null;

            if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Insert a new user into the database
                using (UsersContext db = new UsersContext())
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                    // Check if user already exists
                    if (user == null)
                    {
                        // Insert name into the profile table
                        db.UserProfiles.Add(new UserProfile { UserName = model.UserName });
                        db.SaveChanges();

                        OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                        OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("UserName", "Пользователь с указанной электонной почтой уже зарегистрирован - выберите другой адрес. Если этот адрес принадлежит вам, то авторизуйтесь на сайте используя этот адрес");
                    }
                }
            }

            ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Пользоватеь с таким адресом уже существует, пожалуйста, выберите другой адрес.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Пароль ошибочен. Пожалйуста, ввдеите правильный пароль.";

                case MembershipCreateStatus.InvalidEmail:
                    return "Адрес e-mail ошибочен. Пожалйуста, исправьте и попробуйте ещё раз.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
