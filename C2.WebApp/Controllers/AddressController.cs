﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Controllers
{
    public class AddressController : Controller
    {
        //
        // POST: /Address/Create

        [HttpPost]
        public ActionResult Create(Address address)
        {
            return View();
        }

        //
        // GET: /Address/Get/{id}

        public ActionResult Get(long id)
        {
            return View();
        }

        //
        // POST: /Address/Update/

        [HttpPost]
        public ActionResult Update(Address address)
        {
            return View();
        }

    }
}
