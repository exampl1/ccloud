﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Web;

namespace C2.WebApp.Filters
{
    public class CompressFilter
    {

        public static void CompressResponse(HttpRequestBase request, HttpResponseBase response)
        {

            var AcceptEncoding = request.Headers["Accept-Encoding"];
            AcceptEncoding = AcceptEncoding.ToUpperInvariant();

            if (!String.IsNullOrEmpty(AcceptEncoding))
            {
                if (AcceptEncoding.Contains("GZIP"))
                {
                    response.AddHeader("Content-Encoding", "gzip");
                    response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
                }
                else if (AcceptEncoding.Contains("DEFLATE"))
                {
                    response.AddHeader("Content-Encoding", "deflate");
                    response.Filter = new DeflateStream(response.Filter, CompressionMode.Compress);
                };
            };
        }
    }
}