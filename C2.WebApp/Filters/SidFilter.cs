﻿using C2.DataMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Filters
{
    public class SidAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (filterContext.HttpContext.Request.Unvalidated["sid"] != null)
            {
                Guid guid = Guid.Empty;
                Guid.TryParse(filterContext.HttpContext.Request.Unvalidated["sid"], out guid);

                if (guid != Guid.Empty)
                {
                    using (var context = new AppContext("DefaultConnection"))
                    {
                        var firedSid = context.Sids.Where(s => s.Sid == guid).FirstOrDefault();
                        if (firedSid != null)
                        {
                            firedSid.Status = DataModel.EntityStatus.Used;
                            context.SaveChanges();
                        }
                    }
                }
                filterContext.HttpContext.Response.Redirect(Regex.Replace(filterContext.HttpContext.Request.Unvalidated.RawUrl, @"sid=[\w\-]+", ""), endResponse: true);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}