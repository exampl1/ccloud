﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;

namespace C2.WebApp.Models
{

    public class RegisterExternalLoginModel
    {
        [Required(ErrorMessage = "Укажите {0}.")]
        [Display(Name = "Адрес электронной почты")]
        [RegularExpression(@"^\S+@\S+\.\S+$", ErrorMessage = "Нужно ввести реальный email")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required(ErrorMessage = "Укажите {0}.")]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Укажите {0}.")]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Проверка нового пароля")]
        [Compare("NewPassword", ErrorMessage = "Пароль и его проверочное значение должны совпадать.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessage = "Укажите {0}.")]
        [Display(Name = "Адрес электронной почты")]
        [RegularExpression(@"^\S+@\S+\.\S+$", ErrorMessage = "Нужно ввести реальный email")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Укажите {0}.")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "Укажите {0}.")]
        [Display(Name = "Адрес электронной почты *")]
        [RegularExpression(@"^\S+@\S+\.\S+$", ErrorMessage = "Нужно ввести реальный email")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Укажите {0}.")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль *")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Проверка пароля *")]
        [Compare("Password", ErrorMessage = "Пароль и его проверочное значение должны совпадать.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Имя *")]
        [Required(ErrorMessage = "{0} обязательное поле")]
        public string FirstName { get; set; }

        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        [Display(Name = "Фамилия *")]
        [Required(ErrorMessage = "{0} обязательное поле")]
        public string LastName { get; set; }

        [Display(Name = "Телефон *")]
        [Required(ErrorMessage = "{0} обязательное поле")]
        public string Phone { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}