﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Models
{

    public enum CkEditorToolbarset
    {
        Basic,
        Full,
    }

    public class CkEditorModel
    {
        public CkEditorToolbarset Toolbarset { get; set; }
    }

    public class CkEditorViewModel
    {
        public string TagId { get; set; }
    }

}