﻿using C2.AppModel;
using System;
using System.Collections.Generic;
namespace C2.WebApp.Models
{
    /// <summary>
    /// Модель данных для отображения древовидной структуры.
    /// </summary>
    public interface ITreeViewModel
    {
        /// <summary>
        /// Название представления, отвечающего за отображение элемента узла.
        /// </summary>
        string NodeViewName { get; }

        /// <summary>
        /// Узлы.
        /// </summary>
        IEnumerable<TreeNodeModel> Nodes { get; }

        /// <summary>
        /// Клонирование, с переопределением перечисления узлов.
        /// </summary>
        /// <param name="byNode"></param>
        /// <returns></returns>
        ITreeViewModel Clone(TreeNodeModel byNode);

        /// <summary>
        /// Проверка, что узел должен отображаться развёрнутым.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        bool IsExpanded(TreeNodeModel node);

        /// <summary>
        /// Проверка, что узел должен отображаться активным.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        bool IsSelected(TreeNodeModel node);

        /// <summary>
        /// Поиск узла по идентифкатору.
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        bool TryFind(long nodeId, out TreeNodeModel node);
    }
}
