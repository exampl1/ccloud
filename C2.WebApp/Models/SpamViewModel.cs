﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using C2.DataModel;
using C2.DataMapping;

namespace C2.WebApp.Models
{
    public class SpamViewModel
    {
        public SpamViewModel()
        {

        }

        public IEnumerable<PageContent> Content { get; set; }
        public IQueryable<SidEntity> Sids { get; set; }
        public IQueryable<Job> Jobs { get; set; }

        public DateTime? FirstSending(string templateName)
        {
            if (SentTotal(templateName) == 0)
                return null;

            return Sids.Where(s => s.Data == templateName).Min(s => s.Created);
        }

        public DateTime? LastSending(string templateName)
        {
            if (SentTotal(templateName) == 0)
                return null;

            return Sids.Where(s => s.Data == templateName).Max(s => s.Created);
        }

        public int SentTotal(string templateName)
        {
            return Sids.Where(s => s.Data == templateName).Count();
        }

        public int SentLastDay(string templateName)
        {
            var time = DateTime.Now.AddDays(-1);
            return Sids.Where(s => s.Data == templateName && s.Created> time).Count();
        }

        public int SentLastWeek(string templateName)
        {
            var time = DateTime.Now.AddDays(-7);
            return Sids.Where(s => s.Data == templateName && s.Created > time).Count();
        }

        public int FiredTotal(string templateName)
        {
            return Sids.Where(s => s.Data == templateName && (s.Status & EntityStatus.Used) > 0).Count();
        }

        public int FiredLastDay(string templateName)
        {
            var time = DateTime.Now.AddDays(-1);
            return Sids.Where(s => s.Data == templateName && (s.Status & EntityStatus.Used) > 0 && s.Created > time).Count();
        }

        public int FiredLastWeek(string templateName)
        {
            var time = DateTime.Now.AddDays(-7);
            return Sids.Where(s => s.Data == templateName && (s.Status & EntityStatus.Used) > 0 && s.Created > time).Count();
        }

        public Job GetJobData(string templateName)
        {
            var time = DateTime.Now.AddDays(-7);
            return Jobs.Where(s => s.TemplateName == templateName).SingleOrDefault();
        }

    }
}