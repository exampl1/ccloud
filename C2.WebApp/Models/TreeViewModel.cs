﻿using C2.AppModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Models
{
    public class TreeViewModel : ITreeViewModel
    {
        public TreeViewModel(string nodeViewName,
            IEnumerable<TreeNodeModel> nodes,
            ICollection<TreeNodeModel> expandedNodes,
            ICollection<TreeNodeModel> selectedNodes)
        {
            NodeViewName = nodeViewName;
            Nodes = nodes;
            _expandedNodes = expandedNodes;
            _selectedNodes = selectedNodes;
        }

        ICollection<TreeNodeModel> _expandedNodes;
        
        ICollection<TreeNodeModel> _selectedNodes;

        public string NodeViewName
        {
            get;
            private set;
        }

        public IEnumerable<TreeNodeModel> Nodes
        {
            get;
            private set;
        }

        public bool IsExpanded(TreeNodeModel node)
        {
            return ((_expandedNodes != null) && (_expandedNodes.Contains(node))) 
                || ((_selectedNodes != null) && (node.Nodes.Any(n=> _selectedNodes.Contains(n))));
        }

        public bool IsSelected(TreeNodeModel node)
        {
            return (_selectedNodes != null) && (_selectedNodes.Contains(node));
        }

        public ITreeViewModel Clone(TreeNodeModel byNode)
        {
            return new TreeViewModel(NodeViewName, byNode.Nodes, _expandedNodes, _selectedNodes);
        }

        public bool TryFind(long nodeId, out TreeNodeModel node)
        {
            node = Nodes.FirstOrDefault(n => n.NodeId == nodeId);
            return node != null;
        }
    }
}