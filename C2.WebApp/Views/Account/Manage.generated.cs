﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace C2.WebApp.Views.Account
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "1.4.1.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Account/Manage.cshtml")]
    public class Manage : System.Web.Mvc.WebViewPage<C2.WebApp.Models.LocalPasswordModel>
    {
        public Manage()
        {
        }
        public override void Execute()
        {


            
            #line 2 "..\..\Views\Account\Manage.cshtml"
  
    ViewBag.Title = "Управление пользователем";


            
            #line default
            #line hidden
WriteLiteral("\r\n<hgroup class=\"title\">\r\n    <h1>");


            
            #line 7 "..\..\Views\Account\Manage.cshtml"
   Write(ViewBag.Title);

            
            #line default
            #line hidden
WriteLiteral(".</h1>\r\n</hgroup>\r\n\r\n<p class=\"message-success\">");


            
            #line 10 "..\..\Views\Account\Manage.cshtml"
                      Write(ViewBag.StatusMessage);

            
            #line default
            #line hidden
WriteLiteral("</p>\r\n\r\n<p>Адрес электронной почты <strong>");


            
            #line 12 "..\..\Views\Account\Manage.cshtml"
                              Write(User.Identity.Name);

            
            #line default
            #line hidden
WriteLiteral("</strong>.</p>\r\n\r\n");


            
            #line 14 "..\..\Views\Account\Manage.cshtml"
 if (ViewBag.HasLocalPassword)
{
    
            
            #line default
            #line hidden
            
            #line 16 "..\..\Views\Account\Manage.cshtml"
Write(Html.Partial("_ChangePasswordPartial"));

            
            #line default
            #line hidden
            
            #line 16 "..\..\Views\Account\Manage.cshtml"
                                           
}
else
{ 
    
            
            #line default
            #line hidden
            
            #line 20 "..\..\Views\Account\Manage.cshtml"
Write(Html.Partial("_SetPasswordPartial"));

            
            #line default
            #line hidden
            
            #line 20 "..\..\Views\Account\Manage.cshtml"
                                        
}

            
            #line default
            #line hidden
WriteLiteral("\r\n<section id=\"externalLogins\">\r\n    ");


            
            #line 24 "..\..\Views\Account\Manage.cshtml"
Write(Html.Action("RemoveExternalLogins"));

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n    <h3>Разрешить вход с помощью внешних сервисов</h3>\r\n    ");


            
            #line 27 "..\..\Views\Account\Manage.cshtml"
Write(Html.Action("ExternalLoginsList", new { ReturnUrl = ViewBag.ReturnUrl }));

            
            #line default
            #line hidden
WriteLiteral("\r\n</section>\r\n\r\n");


DefineSection("Scripts", () => {

WriteLiteral("\r\n    ");


            
            #line 31 "..\..\Views\Account\Manage.cshtml"
Write(Scripts.Render("~/bundles/jqueryval"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");


});

WriteLiteral("\r\n");


        }
    }
}
#pragma warning restore 1591
