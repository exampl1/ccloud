﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace C2.WebApp.Views.Account
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "1.5.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Account/ResetPassword.cshtml")]
    public class ResetPassword : System.Web.Mvc.WebViewPage<dynamic>
    {
        public ResetPassword()
        {
        }
        public override void Execute()
        {
            
            #line 1 "..\..\Views\Account\ResetPassword.cshtml"
  
    ViewBag.Title = "Восстановление пароля";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<h2>Восстановление утраченного пароля</h2>\r\n\r\n");

            
            #line 7 "..\..\Views\Account\ResetPassword.cshtml"
 using (Html.BeginForm())
{

            
            #line default
            #line hidden
WriteLiteral("    <fieldset>\r\n        <legend>Данные для восстановления пароля</legend>\r\n");

WriteLiteral("        ");

            
            #line 11 "..\..\Views\Account\ResetPassword.cshtml"
   Write(Html.Hidden("token", (string)ViewBag.Token));

            
            #line default
            #line hidden
WriteLiteral("\r\n        <ol>\r\n            <li");

WriteLiteral(" class=\"name\"");

WriteLiteral(">\r\n                <label");

WriteLiteral(" for=\"password\"");

WriteLiteral(">Введите новый пароль</label>\r\n");

WriteLiteral("                ");

            
            #line 15 "..\..\Views\Account\ResetPassword.cshtml"
           Write(Html.Password("newPassword"));

            
            #line default
            #line hidden
WriteLiteral("\r\n            </li>\r\n        </ol>\r\n        <button");

WriteLiteral(" type=\"submit\"");

WriteLiteral(" class=\"button\"");

WriteLiteral(">Продолжить</button>\r\n    </fieldset>\r\n");

            
            #line 20 "..\..\Views\Account\ResetPassword.cshtml"
}
            
            #line default
            #line hidden
        }
    }
}
#pragma warning restore 1591
