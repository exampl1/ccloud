﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace C2
{
    public abstract class SimpleTemplateProcessor
    {
        static Regex Directive = new Regex(@"\{\{(.+?)\}\}", RegexOptions.Compiled);

        protected SimpleTemplateProcessor(string template)
        {
            _template = template;
        }

        string _template;

        public string Execute()
        {
            return Directive.Replace(_template, HandleDirective);
        }

        string HandleDirective(Match m)
        {
            var directive = m.Groups[1].Value;
            if (directive.StartsWith("foreach "))
            {
                StringBuilder buidelr = new StringBuilder();
                var options = directive.Split(' ');
                foreach (var item in GetEnumeration(options[1]))
                {
                    SimpleTemplateProcessor p = CreateSubProcessor(options[2], item);
                    buidelr.Append(p.Execute());
                }
                return buidelr.ToString();
            }

            if (directive.StartsWith("include "))
            {
                string includeValue;
                ResolveInclude(directive.Split(' ')[1], out includeValue);
                return includeValue;
            }
            
            return GetValue(directive);
        }

        protected abstract string GetValue(string name);

        protected abstract IEnumerable GetEnumeration(string name);

        protected abstract SimpleTemplateProcessor CreateSubProcessor(string name, object model);

        protected virtual bool ResolveInclude(string name, out string result)
        {
            result = string.Empty;
            return false;
        }
    }
}
