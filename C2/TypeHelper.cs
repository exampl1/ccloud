﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2
{
    public static class TypeHelper
    {
        /// <summary>
        /// Determinate when the type is subclass of some generic type.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="generic"></param>
        /// <returns></returns>
        /// <remarks>Only for classes, not for interfaces. See datails on <see cref="http://stackoverflow.com/questions/457676/check-if-a-class-is-derived-from-a-generic-class"/>.</remarks>
        public static bool IsSubclassOfGeneric(this Type type, Type generic)
        {
            while (type != typeof(object))
            {
                var cur = type.IsGenericType ? type.GetGenericTypeDefinition() : type;
                if (generic == cur)
                {
                    return true;
                }
                type = type.BaseType;
            }
            return false;
        }

        /// <summary>
        /// Determinate when the type is subclass of Nullable.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsNullable(this Type type)
        {
            return type.IsSubclassOfGeneric(typeof(Nullable<>));
        }
    }
}
