﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel.StoreModel;

namespace C2.DataMapping.Store
{
    public class CustomerProfileMap : EntityTypeConfiguration<CustomerProfile>
    {
        public CustomerProfileMap()
        {
            ToTable("CustomerProfile");

            HasKey(t => t.CustomerId);

            HasRequired(t => t.Customer)
                .WithOptional();
        }
    }
}
