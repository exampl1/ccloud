﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataMapping.Store
{
    internal class DictionaryType
    {
        static int CodeBase
        {
            get
            {
                return Properties.Settings.Default.DictionaryCodeBase;
            }
        }

        public static int CustomerGroup
        {
            get
            {
                return CodeBase + 0;
            }
        }

        public static int ProductGroup
        {
            get
            {
                return CodeBase + 1;
            }
        }

        public static int Unit
        {
            get
            {
                return CodeBase + 2;
            }
        }

        public static int ProductProperty
        {
            get
            {
                return CodeBase + 3;
            }
        }
    }
}
