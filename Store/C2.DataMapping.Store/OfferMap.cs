﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel.StoreModel;

namespace C2.DataMapping.Store
{
    public class OfferMap : EntityTypeConfiguration<Offer>
    {
        public OfferMap()
        {
            ToTable("Offer");

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasRequired(t => t.Supplier)
                .WithMany()
                .HasForeignKey(d => d.SupplierId);

            HasMany(t => t.Properties)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("OfferPropertyValue");
                    m.MapLeftKey("OfferId");
                    m.MapRightKey("PropertyValueId");
                });
        }
    }
}
