﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel.StoreModel;

namespace C2.DataMapping.Store
{
    public class OrderDeliveryMap : EntityTypeConfiguration<OrderDelivery>
    {
        public OrderDeliveryMap()
        {
            ToTable("OrderDelivery");

            HasKey(t => t.OrderId);

            HasOptional(t => t.PostProvider)
                .WithMany();
        }
    }
}
