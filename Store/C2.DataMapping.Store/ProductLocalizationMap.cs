﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel.StoreModel;

namespace C2.DataMapping.Store
{
    public class ProductLocalizationMap : EntityTypeConfiguration<ProductLocalization>
    {
        public ProductLocalizationMap()
        {
            ToTable("ProductLocalization");
            HasKey(p => new { p.ProductId, p.CultureCode });
        }
    }
}
