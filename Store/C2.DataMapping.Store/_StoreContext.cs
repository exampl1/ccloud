﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;

namespace C2.DataMapping.Store
{
    public class StoreContext : BaseContext
    {
        static StoreContext()
        {
            Database.SetInitializer<StoreContext>(null);
        }

        public StoreContext()
        {
        }

        public StoreContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public StoreContext(DbCompiledModel model)
            : base(model)
        {
        }

        public StoreContext(string nameOrConnectionString, DbCompiledModel model)
            : base(nameOrConnectionString, model)
        {
        }

        public StoreContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
        }

        public StoreContext(ObjectContext objectContext, bool dbContextOwnsObjectContext)
            : base(objectContext, dbContextOwnsObjectContext)
        {
        }

        public StoreContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection)
            : base(existingConnection, model, contextOwnsConnection)
        {
        }

        public DbSet<CustomerGroup> CustomerGroups
        {
            get;
            set;
        }

        public DbSet<CustomerProfile> CustomerProfiles
        {
            get;
            set;
        }

        public DbSet<CustomerInfo> CustomerInfos
        {
            get;
            set;
        }

        public DbSet<Offer> Offers
        {
            get;
            set;
        }

        /// <summary>
        /// Загрузка детальнйо информации о предложении.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Offer LoadOfferDetails(long id)
        {
            return Offers
                .Include(o => o.Product)
                .Include(o => o.Product.Localization)
                .Include(o => o.Unit)
                .Include(o => o.Unit.Localization)
                .FirstOrDefault(o => (o.Id == id) && (o.Status == DataModel.EntityStatus.Normal));
        }

        public DbSet<Order> Orders
        {
            get;
            set;
        }

        /// <summary>
        /// Загрузка детальной информации о заявке.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Order LoadOrderDetails(long id)
        {
            var order = Orders
                .Include(o => o.Items)
                .Include(o => o.Items.Select(i => i.Offer))
                .Include(o => o.Items.Select(i => i.Offer.Unit))
                .Include(o => o.Items.Select(i => i.Offer.Unit.Localization))
                .Include(o => o.Items.Select(i => i.Offer.Product))
                .Include(o => o.Items.Select(i => i.Offer.Product.Vendor))
                .Include(o => o.Items.Select(i => i.Offer.Product.Vendor.Localization))
                .Include(o => o.Items.Select(i => i.Offer.Product.Localization))
                .Include(o => o.Items.Select(i => i.Offer.Product.MediaCollection))
                .Include(o => o.Items.Select(i => i.Offer.Product.MediaCollection.Select(m => m.MediaItem)))
                .Include(o => o.Items.Select(i => i.Offer.Product.MediaCollection.Select(m => m.MediaItem.Localization)))
                .Include(o => o.Items.Select(i => i.Offer.Properties))
                .Include(o => o.Items.Select(i => i.Offer.Properties.Select(p => p.PropertyType)))
                .Include(o => o.Items.Select(i => i.Offer.Properties.Select(p => p.PropertyType.Localization)))
                .FirstOrDefault(o => o.Id == id);

            order.UserProfile = UserProfiles.Include(u => u.Affiliate).Where(u => u.UserName == order.CustomerId).FirstOrDefault();

            var items = order.Items
                .OrderBy(i => i.Offer.Product.VendorId)
                .ThenBy(i => i.Offer.Product.CurrentLocalization.Title)
                .ToList();
            order.Items.Clear();
            order.Items.AddRange(items);

            return order;
        }

        public DbSet<OrderDelivery> OrderDeliveries
        {
            get;
            set;
        }

        public DbSet<OrderItem> OrderItems
        {
            get;
            set;
        }

        public DbSet<Product> Products
        {
            get;
            set;
        }

        public DbSet<TopProduct> TopProducts
        {
            get;
            set;
        }

        public DbSet<ProductGroup> ProductGroups
        {
            get;
            set;
        }


        public DbSet<ProductLocalization> ProductLocalizations
        {
            get;
            set;
        }

        public DbSet<Unit> Units
        {
            get;
            set;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations
                .Add(new CustomerProfileMap())
                .Add(new CustomerInfoMap())
                .Add(new OfferMap())
                .Add(new OrderMap())
                .Add(new OrderDeliveryMap())
                .Add(new OrderItemMap())
                .Add(new ProductMap())
                .Add(new ProductLocalizationMap());

            modelBuilder.Entity<DictionaryItem>()
                .Map<CustomerGroup>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.CustomerGroup))
                .Map<ProductGroup>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.ProductGroup))
                .Map<Unit>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.Unit));
        }
    }
}
