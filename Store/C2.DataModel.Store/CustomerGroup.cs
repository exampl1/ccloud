﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Пользовательская группа.
    /// </summary>
    /// <remarks>
    /// Пользовательские группы могут создавать иерархию.
    /// </remarks>
    public class CustomerGroup : DictionaryItem
    {
    }
}
