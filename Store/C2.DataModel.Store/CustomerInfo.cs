﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Информация о заказчике.
    /// </summary>
    public class CustomerInfo
    {
        /// <summary>
        /// Пользователь.
        /// </summary>
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Имя контактного лица.
        /// </summary>
        [Display(Name = "Контактное лицо")]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string ContactName
        {
            get;
            set;
        }

        /// <summary>
        /// Почта.
        /// </summary>
        [Display(Name = "Электронная почта")]
        [Required(ErrorMessage = "Укажите {0}.")]
        [RegularExpression(@"^\S+@\S+\.\S+$", ErrorMessage = "Нужно ввести реальный email")]
        public string Email
        {
            get;
            set;
        }

        /// <summary>
        /// Телефон.
        /// </summary>
        [Display(Name = "Телефон")]
        [StringLength(15)]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string Phone
        {
            get;
            set;
        }

        /// <summary>
        /// Реквизиты.
        /// </summary>
        [Display(Name = "Реквизиты")]
        [DataType(DataType.MultilineText)]
        public string Requisites
        {
            get;
            set;
        }

        [Display(Name = "Название организацииm, ИП")]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string Organization
        {
            get;
            set;
        }

        [Display(Name = "ИНН")]
        [INNValidate()]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string INN
        {
            get;
            set;
        }

        [Display(Name = "КПП")]
        [KPPValidate()]
        public string KPP
        {
            get;
            set;
        }

        [Display(Name = "ОГРН(ИП)")]
        [OGRNValidate()]
        [Required(ErrorMessage = "Укажите {0}.")]
        public string OGRN
        {
            get;
            set;
        }
        /// <summary>
        /// Идентфикатор адреса доставки.
        /// </summary>
        public long DeliveryAddressId
        {
            get;
            set;
        }

        /// <summary>
        /// Адрес доставки.
        /// </summary>
        [Required(ErrorMessage = "Укажите {0}.")]
        public Address DeliveryAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Оптовый покупатель. Показ оптовых цен
        /// </summary>
        public CustomerStatus Status
        {
            get;
            set;
        }
    }
    
    public enum CustomerStatus : byte
    {
        NeedToCheck = 0,
        Normal = 1,
        IsWholesale = 2,
    }

    public class OGRNValidateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            CustomerInfo obj = validationContext.ObjectInstance as CustomerInfo;
            bool isValid = System.Threading.Thread.CurrentPrincipal.IsInRole("Admin")
                || (obj.INN.Length == 10 && obj.OGRN.Length == 13) || (obj.INN.Length == 12 && obj.OGRN.Length == 15);

            return isValid ? ValidationResult.Success : new ValidationResult("ОГРН указан не верно", new string[] { "OGRN" });
        }

    }
    public class INNValidateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //для админа не проверяем.
            bool isValid = System.Threading.Thread.CurrentPrincipal != null && System.Threading.Thread.CurrentPrincipal.IsInRole("Admin");

            // если не админ
            if (!isValid)
            {
                CustomerInfo obj = validationContext.ObjectInstance as CustomerInfo;

                if (!string.IsNullOrEmpty(obj.INN))
                {
                    if (string.IsNullOrEmpty(obj.KPP))
                    {
                        isValid = obj.INN.Length == 12;
                    }
                    else
                    {
                        isValid = obj.INN.Length == 10;
                    }
                }
            }

            return isValid ? ValidationResult.Success : new ValidationResult("ИНН указан не верно", new string[] { "INN" });
        }

    }

    public class KPPValidateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool isValid = System.Threading.Thread.CurrentPrincipal.IsInRole("Admin");
            if (!isValid)
            {
                CustomerInfo obj = validationContext.ObjectInstance as CustomerInfo;
                isValid = ((obj.INN.Length == 10 && obj.KPP.Length == 9) || (obj.INN.Length == 12 && string.IsNullOrEmpty(obj.KPP)));
            }

            return isValid ? ValidationResult.Success : new ValidationResult("КПП указан не верно", new string[] { "KPP" });
        }

    }
}
