﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Профиль заказчика.
    /// </summary>
    public class CustomerProfile
    {
        /// <summary>
        /// Идентфикатор заказчика.
        /// </summary>
        public long CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Заказчик.
        /// </summary>
        public Subject Customer
        {
            get;
            set;
        }

        /// <summary>
        /// Идентфикатор группы заказчиков.
        /// </summary>
        public long CustomerGroupId
        {
            get;
            set;
        }

        /// <summary>
        /// Группа заказчиков, к которой принадлежит заказчик.
        /// </summary>
        public CustomerGroup CustomerGroup
        {
            get;
            set;
        }
    }
}
