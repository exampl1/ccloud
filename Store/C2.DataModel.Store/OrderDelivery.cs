﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Информация о доставке заказа.
    /// </summary>
    public class OrderDelivery
    {
        /// <summary>
        /// Идентификатор заказа.
        /// </summary>
        public long OrderId
        {
            get;
            set;
        }

        /// <summary>
        /// Заказ.
        /// </summary>
        public Order Order
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор адреса.
        /// </summary>
        public long AddressId
        {
            get;
            set;
        }

        /// <summary>
        /// Адрес.
        /// </summary>
        public Address Address
        {
            get;
            set;
        }

        /// <summary>
        /// Идентифкатор получателя.
        /// </summary>
        public string ReceiverId
        {
            get;
            set;
        }

        /// <summary>
        /// Поставщик почтовых услуг.
        /// </summary>
        public long? PostProviderId
        {
            get;
            set;
        }

        public Subject PostProvider
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор доставки в системе поставщика почтовых услуг.
        /// </summary>
        public string PostProviderUID
        {
            get;
            set;
        }

        /// <summary>
        /// Запланированное начало периода доставки.
        /// </summary>
        public DateTime? PlannedStartDate
        {
            get;
            set;
        }

        /// <summary>
        /// Запланированное окончание периода доставки.
        /// </summary>
        public DateTime? PlannedEndDate
        {
            get;
            set;
        }

        /// <summary>
        /// Фактическое начало периода доставки.
        /// </summary>
        public DateTime? RealStartDate
        {
            get;
            set;
        }

        /// <summary>
        /// Фактическое окончание периода доставки.
        /// </summary>
        public DateTime? RealEndDate
        {
            get;
            set;
        }
    }
}
