﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Позиция заказа.
    /// </summary>
    public class OrderItem
    {
        /// <summary>
        /// Идентфикатор позиции.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор заказа.
        /// </summary>
        public long OrderId
        {
            get;
            set;
        }

        /// <summary>
        /// Заказ.
        /// </summary>
        public Order Order
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор предложения.
        /// </summary>
        public long OfferId
        {
            get;
            set;
        }

        /// <summary>
        /// Предложение.
        /// </summary>
        public Offer Offer
        {
            get;
            set;
        }

        /// <summary>
        /// Количество товара в выбранных единицах.
        /// </summary>
        public decimal Quantity
        {
            get { return quantity; }
            set { AdminQuantity = quantity = value; }
        }
        private decimal quantity;

        /// <summary>
        /// Цена за единицу.
        /// </summary>
        public decimal PriceValue
        {
            get;
            set;
        }

        /// <summary>
        /// Промежуточный итог.
        /// </summary>
        public decimal SubTotal
        {
            get;
            set;
        }

        /// <summary>
        /// Комментарий админа.
        /// </summary>
        public string AdminComment
        {
            get;
            set;
        }

        /// <summary>
        /// Количество товара (для админа).
        /// </summary>
        [DefaultValue(0)]
        public decimal? AdminQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Сумма скидки.
        /// </summary>
        public decimal DiscountTotal
        {
            get;
            set;
        }

        /// <summary>
        /// Сумма налогов.
        /// </summary>
        public decimal TaxTotal
        {
            get;
            set;
        }

        /// <summary>
        /// Итог.
        /// </summary>
        public decimal Total
        {
            get;
            set;
        }

        public OrderItem UpdateSubTotal()
        {
            SubTotal = Quantity * PriceValue;
            return this;
        }

        public OrderItem UpdateTotal()
        {
            Total = SubTotal - DiscountTotal + TaxTotal;
            return this;
        }

        public OrderItem UpdateAll()
        {
            UpdateSubTotal();
            UpdateTotal();
            return this;
        }
    }
}
