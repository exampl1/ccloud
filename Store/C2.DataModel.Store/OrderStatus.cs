﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Состояние заказа.
    /// </summary>
    [Flags]
    public enum OrderStatus
    {
        /// <summary>
        /// Заказ оформляется заказчиком.
        /// </summary>
        [Display(Name = "Создаётся")]
        Creation = 0,

        /// <summary>
        /// Заказ создан заказчиком.
        /// </summary>
        [Display(Name = "Создан")]
        Created = 1,

        /// <summary>
        /// Заказ подтверждён и принят.
        /// </summary>
        [Display(Name = "Подтверждён и принят")]
        Accepted = Created << 1,

        /// <summary>
        /// Заказ комплектуется.
        /// </summary>
        [Display(Name = "Комплектуется")]
        Packaging = Accepted << 1,

        /// <summary>
        /// Заказ укомлектован.
        /// </summary>
        [Display(Name = "Укомплектован")]
        Packed = Packaging << 1,

        /// <summary>
        /// Заказ в процессе доставки.
        /// </summary>
        [Display(Name = "Доставляется")]
        Delivery = Packed << 1,

        /// <summary>
        /// Заказ доставлен.
        /// </summary>
        [Display(Name = "Доставлен")]
        Delivered = Delivery << 1,

        /// <summary>
        /// Ожидание оплаты.
        /// </summary>
        [Display(Name = "Ожидается оплата")]
        PaymentWaiting = Delivered << 1,

        /// <summary>
        /// Заказ полностью оплачен.
        /// </summary>
        [Display(Name = "Оплачен")]
        Payed = PaymentWaiting << 1,

        /// <summary>
        /// Отклонён.
        /// </summary>
        [Display(Name = "Отклонён")]
        Rejected = Payed << 1,

        /// <summary>
        /// Заказ завершён.
        /// </summary>
        [Display(Name = "Завершён")]
        @Completed = Created | Packed | Delivered | Payed,

        [Display(Name = "Предзаказ")]
        Preorder = Rejected << 2,
    }
}
