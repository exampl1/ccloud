﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel.MediaModel;
using C2.Globalization;
using System.ComponentModel.DataAnnotations;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Товар (изделие, услуга).
    /// </summary>
    public class Product : IBaseDataEntity,
        ILocalizable<ProductLocalization>,
        IExpando,
        IMediaCollection,
        ICommentable
    {
        public Product()
        {
            Units = new List<Unit>();
            Localization = new List<ProductLocalization>();
            MediaCollection = new List<MediaCollectionEntry>();
            Comments = new List<Comment>();
            Properties = new List<PropertyValue>();
            Offers = new List<Offer>();
            ProductGroups = new List<ProductGroup>();
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Товарные группы.
        /// </summary>
        public ICollection<ProductGroup> ProductGroups
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор производителя.
        /// </summary>
        public long? VendorId
        {
            get;
            set;
        }

        /// <summary>
        /// Производитель.
        /// </summary>
        public Subject Vendor
        {
            get;
            set;
        }

        /// <summary>
        /// Код продукта, определяемый производителем.
        /// </summary>
        public string VendorCode
        {
            get;
            set;
        }

        /// <summary>
        /// Свойства товара.
        /// </summary>
        public ICollection<PropertyValue> Properties
        {
            get;
            private set;
        }

        /// <summary>
        /// Сгруппированные свойства товара.
        /// </summary>
        [NotMapped]
        public ICollection<ProductPropertyGroup> PropertyGroups
        {
            get
            {
                return (from p in Properties
                        group p by p.PropertyType into g
                        select new ProductPropertyGroup { PropertyType = g.Key, Values = g.ToList() }).ToList();
            }
        }

        /// <summary>
        /// Дата внесения товара в базу (UTC).
        /// </summary>
        public DateTime CreateDate
        {
            get;
            set;
        }

        /// <summary>
        /// Дата последнего обновления товара в базе (UTC).
        /// </summary>
        public DateTime LastUpdateDate
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация.
        /// </summary>
        public ICollection<ProductLocalization> Localization
        {
            get;
            private set;
        }

        /// <summary>
        /// Локализация для текущей культуры.
        /// </summary>
        [NotMapped]
        public ProductLocalization CurrentLocalization
        {
            get
            {
                return this.GetLocalization();
            }
        }

        /// <summary>
        /// Допустимые единицы измерения (многие-ко-многим).
        /// </summary>
        public ICollection<Unit> Units
        {
            get;
            private set;
        }

        /// <summary>
        /// Коллекция медиа-ресурсов.
        /// </summary>
        public ICollection<MediaCollectionEntry> MediaCollection
        {
            get;
            private set;
        }

        /// <summary>
        /// Коллекция комментариев.
        /// </summary>
        public ICollection<Comment> Comments
        {
            get;
            private set;
        }

        
        public int Order
        {
            get;
            set;
        }

        /// <summary>
        /// Предложения товара.
        /// </summary>
        public ICollection<Offer> Offers
        {
            get;
            private set;
        }
    }
}
