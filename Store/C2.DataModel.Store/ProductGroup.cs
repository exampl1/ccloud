﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Группа товаров.
    /// </summary>
    public class ProductGroup : DictionaryItem
    {
        /// <summary>
        /// Получение типизированного флага.
        /// </summary>
        public ProductGroupFlag GetFlags()
        {
            return (ProductGroupFlag)Flags;
        }

        /// <summary>
        /// Установка типизированного флага.
        /// </summary>
        /// <param name="value"></param>
        public void SetFlags(ProductGroupFlag value)
        {
            Flags = (int)value;
        }

        public const string DefaultClassifierCode = "ProductGroup.Default";
    }

    /// <summary>
    /// Флаги товарных групп.
    /// </summary>
    [Flags]
    public enum ProductGroupFlag : int
    {
        /// <summary>
        /// Признак, что группа не может содержать товары, а только другие товарные группы.
        /// </summary>
        Abstract = 1,

        /// <summary>
        /// Признак, что группа не может содержать другие товарные группы.
        /// </summary>
        Leaf = (Abstract << 1)
    }
}
