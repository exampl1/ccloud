﻿using C2.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    public class ProductLocalization : ILocalization
    {
        [C2Required]
        [C2MaxLength(MaxLengths.CultureCode)]
        public string CultureCode
        {
            get;
            set;
        }

        public long ProductId
        {
            get;
            set;
        }

        public Product Product
        {
            get;
            set;
        }

        [C2Required]
        [C2MaxLength(MaxLengths.Title)]
        [Display(ResourceType = typeof(ProductLocalizationMetadataProvider), Name = "TitleDisplayName")]
        public string Title
        {
            get;
            set;
        }

        [C2MaxLength(MaxLengths.Description)]
        [C2DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(ProductLocalizationMetadataProvider), Name = "DescriptionDisplayName")]
        public string Description
        {
            get;
            set;
        }
    }

    public class ProductLocalizationMetadataProvider
    {
        public static string TitleDisplayName
        {
            get
            {
                return ResourceManager.GetString("ProductLocalization.TitleDisplayName");
            }
        }

        public static string DescriptionDisplayName
        {
            get
            {
                return ResourceManager.GetString("ProductLocalization.DescriptionDisplayName");
            }
        }
    }
}
