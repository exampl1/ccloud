﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    public class ProductPropertyGroup
    {
        public PropertyType PropertyType
        {
            get;
            set;
        }

        public ICollection<PropertyValue> Values
        {
            get;
            set;
        }
    }
}
