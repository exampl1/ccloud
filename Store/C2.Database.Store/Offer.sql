﻿CREATE TABLE [dbo].[Offer]
(
	[Id] [dbo].[EntityId] NOT NULL,
    [Status] INT NOT NULL,
	[SupplierId] [dbo].[EntityId] NOT NULL,
    [ProductId] [dbo].[EntityId] NOT NULL,
    [UnitId] [dbo].[EntityId] NOT NULL, 
    [CurrencyCode] [dbo].[CurrencyCode] NOT NULL, 
    [Price] MONEY NOT NULL, 
    [AvailQuantity] [dbo].[Quantity] NOT NULL DEFAULT 0, 
    [StartDate] DATETIME NOT NULL, 
    [ExpiredDate] DATETIME NOT NULL, 
    [PriceOpt20] MONEY NULL, 
    [PriceOpt40] MONEY NULL, 
    [PreorderQuantity] [dbo].[Quantity] NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_Offer] PRIMARY KEY ([Id]),
	CONSTRAINT [FK_Offer_ToSupplier] FOREIGN KEY ([SupplierId]) REFERENCES [Subject]([Id]),
	CONSTRAINT [FK_Offer_ToProduct] FOREIGN KEY ([ProductId]) REFERENCES [Product]([Id]),
    CONSTRAINT [FK_Offer_ToUnitDictionary] FOREIGN KEY ([UnitId]) REFERENCES [DictionaryItem]([Id])
)

GO

CREATE NONCLUSTERED INDEX [IX_Offer_ProductId] 
ON [dbo].[Offer] ([ProductId])
INCLUDE ([Id],[Status],[SupplierId],[UnitId],[CurrencyCode],[Price],[AvailQuantity],[StartDate],[ExpiredDate])
GO

CREATE NONCLUSTERED INDEX [IX_Offer_Status] 
ON [dbo].[Offer] ([Status])
INCLUDE ([ProductId],[CurrencyCode])
GO