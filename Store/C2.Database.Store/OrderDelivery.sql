﻿CREATE TABLE [dbo].[OrderDelivery]
(
	[OrderId] [dbo].[EntityId] NOT NULL,
    [DeliveryStatus] INT NOT NULL,
    [AddressId] [dbo].[EntityId] NOT NULL,
    [ReceiverId] [dbo].[EntityId] NOT NULL,
    [PostProviderId] [dbo].[EntityId] NULL,
    [PostProviderUID] [dbo].[Code] NULL,
    [PlannedStartDate] DATETIME NULL,
    [PlannedEndDate] DATETIME NULL,
    [RealStartDate] DATETIME NULL,
    [RealEndDate] DATETIME NULL,
    CONSTRAINT [PK_OrderDelivery] PRIMARY KEY ([OrderId]),
	CONSTRAINT [FK_OrderDelivery_ToOrder] FOREIGN KEY ([OrderId]) REFERENCES [Order]([Id]),
	CONSTRAINT [FK_OrderDelivery_ToAddress] FOREIGN KEY ([AddressId]) REFERENCES [Address]([Id]),
	CONSTRAINT [FK_OrderDelivery_ToSubjectReceiver] FOREIGN KEY ([ReceiverId]) REFERENCES [Subject]([Id]),
	CONSTRAINT [FK_OrderDelivery_ToSubjectPostProvider] FOREIGN KEY ([PostProviderId]) REFERENCES [Subject]([Id])
)
