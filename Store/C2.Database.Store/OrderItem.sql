﻿CREATE TABLE [dbo].[OrderItem]
(
	[Id] [dbo].[EntityId] NOT NULL,
    [OrderId] [dbo].[EntityId] NOT NULL,
	[OfferId] [dbo].[EntityId] NOT NULL,
    [Quantity] [dbo].[Quantity] NOT NULL,
	[PriceValue] MONEY NOT NULL,
    [SubTotal] MONEY NOT NULL,
    [DiscountTotal] MONEY NOT NULL,
    [TaxTotal] MONEY NOT NULL,
    [Total] MONEY NOT NULL,
    [AdminQuantity] [dbo].[Quantity] NULL, 
    [AdminComment] [dbo].[Title] NULL, 
    CONSTRAINT [PK_OrderItem] PRIMARY KEY ([Id]),
	CONSTRAINT [FK_OrderItem_ToOrder] FOREIGN KEY ([OrderId]) REFERENCES [Order]([Id])
)