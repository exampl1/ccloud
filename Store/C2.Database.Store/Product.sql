﻿CREATE TABLE [dbo].[Product]
(
	[Id] [dbo].[EntityId] NOT NULL,
    [Status] INT NOT NULL,
    [VendorId] [dbo].[EntityId] NULL,
    [VendorCode] [dbo].[Code] NULL,
    [CreateDate] DATETIME NOT NULL DEFAULT GETUTCDATE(),
	[LastUpdateDate] DATETIME NOT NULL,
    [Order] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_Product] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Product_ToManufacturer] FOREIGN KEY ([VendorId]) REFERENCES [Subject]([Id])
)