﻿CREATE TABLE [dbo].[ProductLocalization]
(
	[CultureCode] [dbo].[CultureCode] NOT NULL,
    [ProductId] [dbo].[EntityId] NOT NULL,
    [Title] [dbo].[Title] NOT NULL,
    [Description] [dbo].[Description] NULL,
    CONSTRAINT [PK_ProductLocalization] PRIMARY KEY ([CultureCode], [ProductId]),
	CONSTRAINT [FK_ProductLocalization_ToProduct] FOREIGN KEY ([ProductId]) REFERENCES [Product]([Id])
)
