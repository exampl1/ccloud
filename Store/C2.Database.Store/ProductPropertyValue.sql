﻿CREATE TABLE [dbo].[ProductPropertyValue]
(
	[ProductId] [dbo].[EntityId] NOT NULL , 
    [PropertyValueId] [dbo].[EntityId] NOT NULL, 
    CONSTRAINT [PK_ProductPropertyValue] PRIMARY KEY ([ProductId], [PropertyValueId]),
	CONSTRAINT [FK_ProductPropertyValue_ToProduct] FOREIGN KEY ([ProductId]) REFERENCES [Product]([Id]), 
    CONSTRAINT [FK_ProductPropertyValue_ToPropertyValue] FOREIGN KEY ([PropertyValueId]) REFERENCES [PropertyValue]([Id])
)
