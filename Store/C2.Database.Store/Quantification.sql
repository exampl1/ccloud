﻿CREATE TABLE [dbo].[Quantification]
(
	[ProductId] [dbo].[EntityId] NOT NULL,
    [UnitId] [dbo].[EntityId] NOT NULL,
    CONSTRAINT [PK_Quantification] PRIMARY KEY ([ProductId], [UnitId]),
    CONSTRAINT [FK_Quantification_ToProduct] FOREIGN KEY ([ProductId]) REFERENCES [Product]([Id]),
	CONSTRAINT [FK_Quantification_ToUnitDictionary] FOREIGN KEY ([UnitId]) REFERENCES [DictionaryItem]([Id])
)
