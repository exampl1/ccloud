﻿CREATE TABLE [dbo].[TopProduct]
(
	[ProductId] [dbo].[EntityId] NOT NULL PRIMARY KEY, 
    [Type] INT NOT NULL, 
    CONSTRAINT [FK_TopProducts_ToProduct] FOREIGN KEY ([ProductId]) REFERENCES [Product]([Id])
)
