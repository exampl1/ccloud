﻿<tr>
  <td style="text-align: center;">{{Num}}</td>
  <td>{{Articul}}</td>
  <td>
    <strong>{{ProductTitle}}</strong>{{Properties}}
  </td>
  <td style="text-align: center;">{{Quantity}}</td>
  <td style="text-align: center;">{{Unit}}</td>
  <td style="text-align: right;">{{Price}}</td>
  <td style="text-align: right;">{{Total}}</td>
</tr>