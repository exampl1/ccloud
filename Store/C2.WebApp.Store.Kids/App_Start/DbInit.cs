﻿using C2.AppController;
using C2.DataManager;
using C2.DataMapping;
using C2.DataMapping.Store;
using C2.DataModel;
using C2.DataModel.StoreModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Store.Kids
{
    public class DbIniter: IIniter
    {

        public void Init()
        {

            string code = "MISHKAMI";

            using (var dbContext = new StoreContext("DefaultConnection"))
            {
                var classifier = dbContext.Classifiers.Where(c => c.Code == "ProductGroup.Default").FirstOrDefault();
                if (classifier == null)
                {
                    classifier = dbContext.Classifiers.Add(new Classifier
                    {
                        Code = ProductGroup.DefaultClassifierCode,
                        Id = AppContext.GetId(),
                        Status = EntityStatus.Normal,
                        ClassifierType = 1,
                    });

                    classifier.Localization.Add(new ClassifierLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Классификатор товарных групп по умолчанию",
                        Title = "Группы товаров",
                    });
                }


                // настройки сайта
                var sub = dbContext.Subjects.Where(s => s.Code == code).FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = AppContext.GetId(),
                        Code = code,
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@mishkami.com",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "mishkami.com",
                        FullName = "mishkami.com",
                        ShortName = "mishkami.com",
                        Name = "mishkami.com",
                    });
                }

                // основные
                var suplier_data = new Supplier
                {
                    Logo = "mishkami.com <small>Мишка Ми</small>",
                    Domain = "mishkami.com",
                    PhoneBlock = "<span>8 (800) 555-55-55 звонок бесплатный</span><span>8 (383) 222-22-22 Новосибирск</span>",
                    FirstFooterBlock = "<li><a href='/'>Интернет-магазин</a></li><li><a href='/home/get/business/'>Оптовым клиентам</a></li><li><a href='/home/get/order/'>Сделать заказ</a></li><li><a href='/home/get/preorder/'>Заказ по предоплате</a></li><li><a href='/home/get/sizetable/'>Таблица размеров</a></li>",
                    SecondFooterBlock = "<li><a href='/product/'>Все товары</a></li><li><a href='/comment/'>Отзывы</a></li></li>",
                    ThirdFooterBlock = "<li>еще что то</li>",
                    LeftFooterBlock = @"<p>и тут что то</p>",
                    YaId = "",
                };

                var suplier = dbContext.Suppliers.Where(s => s.Domain == suplier_data.Domain).FirstOrDefault();
                if (suplier == null)
                {
                    suplier_data.SubjectId = sub.Id;
                    dbContext.Suppliers.Add(suplier_data);
                }
                else
                {
                    suplier.Banner = suplier_data.Banner;
                    suplier.Domain = suplier_data.Domain;
                    suplier.Logo = suplier_data.Logo;
                    suplier.PhoneBlock = suplier_data.PhoneBlock;
                    suplier.FirstFooterBlock = suplier_data.FirstFooterBlock;
                    suplier.SecondFooterBlock = suplier_data.SecondFooterBlock;
                    suplier.ThirdFooterBlock = suplier_data.ThirdFooterBlock;
                    suplier.PartnerCode = suplier_data.PartnerCode;
                    suplier.LeftFooterBlock = suplier_data.LeftFooterBlock;
                    suplier.YaId = suplier_data.YaId;
                }


                var node = dbContext.ClassifierNodes.FirstOrDefault();
                if (node == null)
                {
                    var pg = dbContext.ProductGroups.Add(new ProductGroup
                    {
                        Id = AppContext.GetId(),
                        Code = "root",
                        Status = EntityStatus.Normal,
                    });

                    pg.Localization.Add(new DictionaryItemLocalization
                    {
                        CultureCode = "ru-RU",
                        Title = "root",
                        Description = "root",
                    });

                    node = dbContext.ClassifierNodes.Add(new ClassifierNode
                    {
                        ClassifierId = classifier.Id,
                        Id = AppContext.GetId(),
                        Code = "root",
                        EntityId = pg.Id,
                        Status = EntityStatus.Normal,
                    });

                    node.SetParent(null);
                }

                dbContext.SaveChanges();

            }

        }

        public int Order
        {
            get { return -1; }
        }
    }

    public class InitConfig
    {
        public static void RegisterIniters(AppInitCollection initers)
        {
            initers.Add(new DbIniter());
        }
    }
}