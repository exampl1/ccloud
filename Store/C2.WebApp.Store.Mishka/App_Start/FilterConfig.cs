﻿using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Store.Mishka
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
