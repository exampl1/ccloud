﻿using C2.WebApp.Store.Controllers;
using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Store.OKiss
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ProductPageDataLoader());
        }
    }
}