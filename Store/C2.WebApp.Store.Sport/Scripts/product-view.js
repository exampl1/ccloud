﻿/// <reference path="jquery-1.8.1.js" />

var C2 = {};
C2.basket = {};
C2.basket.urlGet = '/basket/get';

C2.basket.updateComplete = function () {

    $.ajax({
        type: 'POST',
        url: C2.basket.url,
        success: function (result) {
            $("#basket-info").html(result);
        }
    });
}

C2.basket.updateBegin = function () {
    fadeBlock('#basket-items');
}

function switchToGrid(id) {
    $(id).addClass("grid-view");
}

function switchToTable(id) {
    $(id).removeClass("grid-view");
}

function fadeOutSearch() {
    $("#main").animate({ 'opacity': 0.3 }, 500);
}
function fadeInSearch() {
    $("#main").stop().animate({ 'opacity': 1 }, 300);
}

function fadeBlock(selector) {
    $('<div id="fadeBlock"></div>')
        .css({ width: $(selector).width(), height: $(selector).height(), top: $(selector).position().top })
        .appendTo(selector)
        .animate({ 'opacity': 0.3 }, 300);
    $('<span>Секундочку...</span>')
        .css({ top: ($(window).height() / 2), left: $(window).width() / 2 - 70 })
        .appendTo('#fadeBlock');
}

$(function () {
    $('.textInput-placeholder').each(function () {
        $(this).attr('data-placeholder', $(this).val());
        $(this).focusin(function () {
            if ($(this).val() == $(this).attr('data-placeholder')) {
                $(this).val('').removeClass('textInput-placeholder');
            };
        }).focusout(function () {
            if ($(this).val() == '') {
                $(this).val($(this).attr('data-placeholder')).addClass('textInput-placeholder');
            };
        });
    });

    // search focus
    $('header .input-wrapper input').focusin(function () {
        $(this).parent().addClass('input-focused');
    }).focusout(function () {
        $(this).parent().removeClass('input-focused');
    }).keydown(function (e) {
        if (e.which == 27) {
            $(this).blur();
        };
    });

    $(document).on('click', '.handler-buy', hanleAddToCart);
    $(document).on('click', '.plus', orderItem);
    $(document).on('click', '.minus', orderItem);
    //$(document).on('click', '.tree-node', toggleNode);
});

function toggleNode(e) {
    var target = $(e.target).parent().parent();
    if (!target.hasClass('leaf')) {
        e.preventDefault();
        target.toggleClass('expanded');
    }
}

var updateOrderItem = 0;
function orderItem(e) {
    var btn = $(this),
        item = btn.parent().find('.number'),
        sum = btn.parent().parent().find('.sum');

    var cnt = parseInt(item.html());
    var bound = parseInt(btn.attr('data-bound'));
    var preorder = parseInt(btn.attr('data-preorder'));

    if (btn.hasClass('plus')) {
        if (cnt < bound) {
            cnt++;
        } else {
            //предзаказ?
            if (preorder > 0) {
                //уже предзаказ и добавлять можно
                cnt++;
            } else {
                // спросим про предзаказ..
                if (confirm("Внимание: такого количества данной позиции нет на складе, хотите перейти к оформлению предзаказа?")) {
                    // и отправим на предзаказ, если согласен
                    $("#offerHeader").html('Сформируйте предзаказ *');

                    $(".plus").each(function () {
                        $(this).attr('data-preorder', 1);
                    });

                    $(".number").each(function () {
                        $(this).attr('data-preorder', 1);
                    });
                    $(".handler-buy").each(function () {
                        $(this).attr('data-preorder', 1);
                    });
                }
            }
        }
    } else {
        if (cnt > bound) {
            cnt--;
        }
    }

    if (sum.length == 1) {
        var price = parseInt(sum.attr('data-price'));
        //sum.html(cnt * price);

        if (cnt == 0) {
            if (updateOrderItem > 0) {
                clearTimeout(updateOrderItem);
            }
            basketUpdate(item, cnt);
        } else {

            if (updateOrderItem > 0) {
                clearTimeout(updateOrderItem);
            }

            updateOrderItem = setTimeout(function () {
                basketUpdate(item, cnt);
            }, 500);
        }

    }

    item.html(cnt);
};

function basketUpdate(item, cnt) {
    C2.basket.updateBegin();
    C2.basket.postQuantity(item, cnt);
}

C2.basket.postQuantity = function (item, cnt) {
    $.ajax({
        type: 'POST',
        url: item.attr("data-posturl"),
        data: {
            id: item.attr("data-id"),
            quantity: cnt,
            isPreorder: item.attr("data-preorder"),
        },
        dataType: "html",
        success: function (result) {
            C2.basket.updateComplete();
            $('#' + item.attr("data-UpdateTargetId")).html(result);
        }
    });
}

function hanleAddToCart(e) {
    var h = $(this);
    items = h.parent().find('.offer');
    var data = [];
    for (var i = 0; i < items.length; i++) {
        var item = $('.number', items[i]);
        var cnt = parseInt(item.html());

        if (cnt > 0 && $(items[i]).hasClass("visible")) {
            data[i] = { id: item.attr("data-id"), cnt: cnt };

            var f = function (x, item, cnt) {
                return function () {
                    addToCart(e, h, item.attr("data-id"), cnt)
                }
            }(i, item, cnt);

            setTimeout(f, 200 * (i + 1));
        }
    }

    var request = '';
    for (var i = 0; i < data.length; i++) {
        if (data[i]) {
            request += 'id=' + data[i].id + '&count=' + data[i].cnt + '&';
        }
    }

    if (request != '') {
        $.ajax({
            type: 'POST',
            url: h.attr("data-posturl"),
            data: request + "isPreorder=" + h.attr("data-preorder"),
            dataType: "html",
            success: function (result) {
                $('#' + h.attr("data-UpdateTargetId")).html(result);
            }
        });
    } else {
        var bezier_params = {
            start: {
                x: e.pageX - 18,
                y: e.pageY - 60,
                angle: Math.floor((Math.random() - Math.random()) * 100),
                length: 0.2
            },
            end: {
                x: $(".offers-container").find('h3').offset().left,
                y: $(".offers-container").find('h3').offset().top,
                angle: Math.floor((Math.random() - Math.random()) * 100) + Math.floor(Math.random() * 60),
                length: 0.75
            }
        };


        $('<strong class="warn">Выберите нужный размер</strong>')
            .appendTo('body')
            .animate({
                path: new $.path.bezier(bezier_params),
                opacity: 0.4
            },
                2000,
                function () {
                    $(this).remove();
                    $(".offers-container").find('h3').animate({ 'font-size': '2em' }, 400);
                });

    }

}

var updateBasket = 0;
function addToCart(e, h, offerid, count) {
    var cart = $('.cart');

    var bezier_params = {
        start: {
            x: e.pageX - 18,
            y: e.pageY - 60,
            angle: Math.floor((Math.random() - Math.random()) * 100),
            length: 0.2
        },
        end: {
            x: cart.find('i').offset().left,
            y: cart.find('i').offset().top + 10,
            angle: Math.floor((Math.random() - Math.random()) * 100) + Math.floor(Math.random() * 60),
            length: 0.75
        }
    };
    $('<strong class="plusone">+' + count + '</strong>')
        .appendTo('body')
        .animate({
            path: new $.path.bezier(bezier_params),
            opacity: 0.2
        },
            parseInt(e.pageY * 1.5),
            function () {
                $(this).remove();
                cart.find('i').css({ marginTop: 2 }).delay(75).animate({ marginTop: 0 }, 50);
            });

    return false;
};

function add_favorite(a) {

    title = document.title;
    url = document.location;
    try {
        // Internet Explorer 
        window.external.AddFavorite(url, title);
    }
    catch (e) {
        try {
            // Mozilla 
            window.sidebar.addPanel(title, url, "");
        }
        catch (e) {
            // Opera 
            if (typeof (opera) == "object") {
                a.rel = "sidebar";
                a.title = title;
                a.url = url;
                return true;
            }
            else {
                // Unknown 
                alert('Нажмите Ctrl-D чтобы добавить страницу в закладки');
            }
        }
    }
    return false;
}

(function ($) {
    $.path = {}
    var V = {
        rotate: function (p, degrees) {
            var radians = degrees * 3.141592654 / 180
            var c = Math.cos(radians), s = Math.sin(radians)
            return [c * p[0] - s * p[1], s * p[0] + c * p[1]]
        },
        scale: function (p, n) {
            return [n * p[0], n * p[1]]
        },
        add: function (a, b) {
            return [a[0] + b[0], a[1] + b[1]]
        },
        minus: function (a, b) {
            return [a[0] - b[0], a[1] - b[1]]
        }
    }

    $.path.bezier = function (params) {
        params.start = $.extend({ angle: 0, length: 0.3333 }, params.start)
        params.end = $.extend({ angle: 0, length: 0.3333 }, params.end)

        this.p1 = [params.start.x, params.start.y];
        this.p4 = [params.end.x, params.end.y];

        var v14 = V.minus(this.p4, this.p1)
        var v12 = V.scale(v14, params.start.length)
        v12 = V.rotate(v12, params.start.angle)
        this.p2 = V.add(this.p1, v12)

        var v41 = V.scale(v14, -1)
        var v43 = V.scale(v41, params.end.length)
        v43 = V.rotate(v43, params.end.angle)
        this.p3 = V.add(this.p4, v43)

        this.f1 = function (t) { return (t * t * t); }
        this.f2 = function (t) { return (3 * t * t * (1 - t)); }
        this.f3 = function (t) { return (3 * t * (1 - t) * (1 - t)); }
        this.f4 = function (t) { return ((1 - t) * (1 - t) * (1 - t)); }

        /* p from 0 to 1 */
        this.css = function (p) {
            var f1 = this.f1(p), f2 = this.f2(p), f3 = this.f3(p), f4 = this.f4(p)
            var x = this.p1[0] * f1 + this.p2[0] * f2 + this.p3[0] * f3 + this.p4[0] * f4;
            var y = this.p1[1] * f1 + this.p2[1] * f2 + this.p3[1] * f3 + this.p4[1] * f4;
            return { top: y + "px", left: x + "px" }
        }
    }

    $.path.arc = function (params) {
        for (var i in params)
            this[i] = params[i]

        this.dir = this.dir || 1

        while (this.start > this.end && this.dir > 0)
            this.start -= 360

        while (this.start < this.end && this.dir < 0)
            this.start += 360


        this.css = function (p) {
            var a = this.start * (p) + this.end * (1 - (p))
            a = a * 3.1415927 / 180 // to radians

            var x = Math.sin(a) * this.radius + this.center[0]
            var y = Math.cos(a) * this.radius + this.center[1]
            return { top: y + "px", left: x + "px" }
        }

    };

    $.fx.step.path = function (fx) {
        var css = fx.end.css(1 - fx.pos)
        for (var i in css)
            fx.elem.style[i] = css[i];
    }
})(jQuery);