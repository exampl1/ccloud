﻿<div style='font-family: Segoe UI, Segoe, Arial, sans-serif'>
  <p>Здравствуйте, {{CustomerInfo}}!</p>
  <p>Ваш заказ {{Number}} от {{Date}} на сайте <a href='http://optodrom.ru'>http://optodrom.ru</a> подтвержден менеджером и ожидает оплаты.</p>
  <h2>Состав заказа</h2>
  <table border='1' cellpadding='2' cellspacing='1'>
    <tr>
      <th>№</th>
      <th>Артикул</th>
      <th>Название товара</th>
      <th>Количество</th>
      <th>Ед.</th>
      <th>Цена</th>
      <th>Итого</th>
    </tr>
    {{foreach Items Items}}
  </table>
  <p>Итого: {{Total}} р.</p>

  <h2>Реквизиты для оплаты в Альфабанке</h2>
  <div>
    <p>ФИО: Горбатенко Олег Викторович</p>
    <p>Номер счета: 40817810704060004520 (RUR)</p>
    <P>Банк получателя: ОАО "Альфа-Банк"</P>
    <P>БИК: 044525593</P>
    <P>Кор. Счет: 30101810200000000593</P>
    <P>ИНН Банка: 7728168971</P>
    <P>КПП Банка: 775001001</P>
  </div>

{{include Footer}}
</div>