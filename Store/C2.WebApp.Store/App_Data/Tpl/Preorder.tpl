﻿<div style='font-family: Segoe UI, Segoe, Arial, sans-serif'>
  <p>Здравствуйте, {{CustomerInfo}}!</p>
  <p>Вы сделали заказ на сайте <a href='http://optodrom.ru'>http://optodrom.ru</a></p>
  <p>Заказ {{Number}} от {{Date}}</p>
  <table border='1' cellpadding='2' cellspacing='1'>
    <tr>
      <th>№</th>
      <th>Артикул</th>
      <th>Название товара</th>
      <th>Количество</th>
      <th>Ед.</th>
      <th>Цена</th>
      <th>Итого</th>
    </tr>
    {{foreach Items Items}}
  </table>
  <p>Итого: {{Total}} р.</p>
  <h1>После проверки заказа менеджером, вы получите счет для оплаты</h1>
  {{include Footer}}
</div>