﻿<table border='0' cellpadding='0' cellspacing='0'>
<tbody>
<tr>
<td style='width: 660px; margin-bottom: 12px'><p><strong>Приветствуем, {{UserName}}!</strong></p></td>
</tr>
<tr>
<td style='width: 660px; margin-bottom: 12px'>
  <p>Благодарим Вас за регистрацию на сайте.</p>
  <p>Приглашаем ознакомиться с ценами в <a href='http://optodrom.ru/'>каталоге</a> и принять участие в бонусной программе <a href='http://optodrom.ru/home/get/bonus'>Подарки за покупки</a>.</p> 
  <p>Если вы ИП или представляете организацию и хотите покупать оптом, то вам <a href='http://optodrom.ru/customer'>сюда</a>.</p>
</td>
</tr>
<tr><td style='width: 660px;'>{{include Footer}}</td></tr>
</tbody>
</table>