﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace C2.WebApp.Store
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "ProductItem",
            //    url: "Product/{seoName}/{id}",
            //    defaults: new { controller = "Product", action = "Item" }
            //);

            //routes.MapRoute(
            //    name: "Error",
            //    url: "Error/{action}",
            //    defaults: new { controller = "Error", action = "Http404" },
            //    namespaces: new[] { "C2.WebApp.Controllers" }
            //);


            routes.MapRoute(
                name: "ProductSelling",
                url: "Product/Selling",
                defaults: new { controller = "Product", action = "Selling" },
                namespaces: new[] { "C2.WebApp.Store.Controllers" }
            ); 
            routes.MapRoute(
                name: "ProductSearch",
                url: "Product/Search",
                defaults: new { controller = "Product", action = "Search" },
                namespaces: new[] { "C2.WebApp.Store.Controllers" }
            );

            routes.MapRoute(
                name: "ProductSetOrder",
                url: "Product/SetOrder",
                defaults: new { controller = "Product", action = "SetOrder" },
                namespaces: new[] { "C2.WebApp.Store.Controllers" }
            ); 
            routes.MapRoute(
                name: "ProductItem",
                url: "Product/Item/{id}",
                defaults: new { controller = "Product", action = "Item" },
                namespaces: new[] { "C2.WebApp.Store.Controllers" }
            );

            routes.MapRoute(
                name: "ProductCollection",
                url: "Product/{categorySeoName}/{categoryNodeId}",
                defaults: new { controller = "Product", action = "Index", categorySeoName = "all", categoryNodeId = 0},
                namespaces: new[] { "C2.WebApp.Store.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "C2.WebApp.Store.Controllers" }
            );
        }
    }
}