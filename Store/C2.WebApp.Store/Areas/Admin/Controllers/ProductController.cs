﻿using C2.DataManager.Store;
using C2.DataMapping.Store;
using C2.WebApp.Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Reflection;
using C2.DataModel.StoreModel;
using System.Text.RegularExpressions;
using C2.DAL;

namespace C2.WebApp.Store.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductController : Controller
    {
        public ProductController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;

        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View(_dataManager.SelectProducts(new SelectProductParams { }));
        }

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Update_title(string text, long id)
        {
            var product = GetProduct(id);
            if (product != null)
            {
                product.CurrentLocalization.Title = text;
                _dataContext.SaveChanges();
            }
            return new EmptyResult();   
        }

        private DataModel.StoreModel.Product GetProduct(long id)
        {
            return _dataContext.Products
                .Include(p => p.Localization)
                .Where(p => p.Id == id).FirstOrDefault();
        }

        private DataModel.StoreModel.Product GetProductWithOffers(long id)
        {
            return _dataContext.Products
                .Include(p => p.Localization)
                .Include(p => p.Offers)
                .Where(p => p.Id == id).FirstOrDefault();
        }

        [HttpPost]
        public ActionResult Update_description(string text, long id)
        {
            var product = GetProduct(id);
            if (product != null)
            {
                product.CurrentLocalization.Description = text;
                _dataContext.SaveChanges();
            }
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult Update_price(string text, long id)
        {
            SetPrice(text, id);
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult Update_priceOpt(string text, long id)
        {
            SetPrice(text, id, "opt");
            return new EmptyResult();
        }
        
        [HttpPost]
        public ActionResult Update_discount(string text, long id)
        {
            SetPrice(text, id, "discount");
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult removeOffer(long id)
        {
            var offer = _dataContext.Offers.Where(o => o.Id == id).FirstOrDefault();
            if (offer != null)
            {
                if (offer.Status.HasFlag(DataModel.EntityStatus.Removed))
                {
                    offer.Status = DataModel.EntityStatus.Normal;
                }
                else
                {
                    offer.Status = DataModel.EntityStatus.Removed;
                }
                _dataContext.SaveChanges();
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult Update_availQuantity(string text, long id, string data)
        {
            long oid = 0;
            int cnt = -1;

            long.TryParse(data, out oid);
            int.TryParse(text, out cnt);

            if (oid > 0 && cnt > -1)
            {
                var offer = _dataContext.Offers.Where(o => o.Id == oid && o.ProductId == id).FirstOrDefault();
                if (offer != null && offer.AvailQuantity != cnt)
                {
                    offer.AvailQuantity = cnt;
                    _dataContext.SaveChanges();
                }
            }

            return new EmptyResult();
        }

        private void SetPrice(string text, long id, string priceType = "normal")
        {
            var product = GetProductWithOffers(id);
            if (product != null)
            {
                int p = 0;
                int.TryParse(Regex.Replace(text, @"\D", ""), out p);
                if (p > 0 || priceType == "discount")
                {
                    foreach (var o in product.Offers)
                    {
                        switch (priceType)
                        {
                            case "discount":
                                if (p > 0)
                                {
                                    o.PriceOpt20 = p;
                                }
                                else
                                {
                                    o.PriceOpt20 = null;
                                }
                                break;
                            case "opt":
                                o.PriceOpt40 = p;
                                break;
                            default:
                                o.Price = p;
                                break;
                        }
                    }
                    _dataContext.SaveChanges();
                }
            }

        }

    }

}
