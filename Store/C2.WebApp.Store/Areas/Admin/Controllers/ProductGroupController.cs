﻿using C2.DataManager.Store;
using C2.DataManager.Store.Models;
using C2.DataMapping.Store;
using C2.DataModel.StoreModel;
using C2.WebApp.Store.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace C2.WebApp.Store.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductGroupController : Controller
    {
        public ProductGroupController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;
        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }


        //
        // GET: /Admin/Classifier/
        public ActionResult Index()
        {
            var model = new WebApp.Models.TreeViewModel("_ProductGroupHeader",
                _dataManager.GetProductGroupClassifier().Roots,
                null,
                null);
            
            return View(model);
        }

        [HttpPost]
        public EmptyResult GetTree(string root)
        {
            if (string.IsNullOrEmpty(root))
            {
                Helpers.JsonFormatter.WriteNodeList(Response.Output, _dataManager.GetProductGroupClassifier().Roots);
            }
            else
            {
                Helpers.JsonFormatter.WriteNodeList(Response.Output, _dataManager.GetProductGroupClassifier(ProductGroup.DefaultClassifierCode, root).Roots);
            }
            
            return new EmptyResult();
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult AddNode(ProductGroupModel model)
        {
            if (model.IsValid())
            {
                try
                {
                    _dataManager.AddProductGroupClassifier(model);
                    return Json(new { status = true, id = model.Id, path = model.Path, value = model.Title, code = model.Code });
                }
                catch (Exception ex)
                {
                    return Json(new { status = false, error = ex.Message });
                }
            }

            return Json(new { status = false, error = "Ошибка ввода данных" });
        }

        [HttpPost]
        public JsonResult AddProduct(ProductGroupModel model)
        {
            if (model.IsValid())
            {
                try
                {
                    _dataManager.AddProductGroupClassifier(model);
                    return Json(new { status = true, id = model.Id, path = model.Path, value = model.Title, code = model.Code });
                }
                catch (Exception ex)
                {
                    return Json(new { status = false, error = ex.Message });
                }
            }

            return Json(new { status = false, error = "Ошибка ввода данных" });
        }

        [HttpPost]
        public JsonResult CopyNode(ProductGroupModel model)
        {
            try
            {
                _dataManager.CopyProductGroupClassifier(model);
                return Json(new { status = true, id = model.Id, path = model.Path, value = model.Title });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, error = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult DeleteNode(ProductGroupModel model)
        {
            try
            {
                _dataManager.DelProductGroupClassifier(model);
                return Json(new { status = true, id = model.Id, path = model.Path, value = model.Title, code= model.Code });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, error = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetData(string path)
        {
            var pg = _dataManager.GetProductGroupByPath(path);
            if (pg != null) {
                return Json(new { title = pg.Title, description = pg.Description, code = pg.Code, path=pg.Path });
            }

            throw new ArgumentNullException();
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult UpdateNode(ProductGroupModel model)
        {
            if (model.IsValid())
            {
                try
                {
                    _dataManager.UpdateProductGroupClassifier(model);
                    return Json(new { status = true, id = model.Id, path = model.Path, value = model.Title, code=model.Code });
                }
                catch (Exception ex)
                {
                    return Json(new { status = false, error = ex.Message, stack = ex.StackTrace });
                }
            }

            return Json(new { status = false, error = "Ошибка ввода данных" });
        }
    }
}
