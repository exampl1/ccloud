﻿using C2.DataManager.Store;
using C2.DataManager.Store.Models;
using C2.DataMapping.Store;
using C2.DataModel.StoreModel;
using C2.WebApp.Store.Areas.Admin.Models;
using C2.WebApp.Store.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace C2.WebApp.Store.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ReportController : Controller
    {
        public ReportController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;
        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Store()
        {
            StoreReportFilter model = new StoreReportFilter();
            return View(model);
        }

        [HttpPost]
        public ActionResult Store(StoreReportFilter model)
        {
            return View(model);
        }

    }
}
