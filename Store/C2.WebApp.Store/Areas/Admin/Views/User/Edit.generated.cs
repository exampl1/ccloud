﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18047
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace C2.WebApp.Store.Areas.Admin.Views.User
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "1.5.4.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Areas/Admin/Views/User/Edit.cshtml")]
    public partial class Edit : System.Web.Mvc.WebViewPage<C2.DataModel.UserProfile>
    {
        public Edit()
        {
        }
        public override void Execute()
        {
            
            #line 3 "..\..\Areas\Admin\Views\User\Edit.cshtml"
  
    ViewBag.Title = "Edit";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<h2>Edit</h2>\r\n\r\n");

            
            #line 9 "..\..\Areas\Admin\Views\User\Edit.cshtml"
 using (Html.BeginForm()) {
    
            
            #line default
            #line hidden
            
            #line 10 "..\..\Areas\Admin\Views\User\Edit.cshtml"
Write(Html.AntiForgeryToken());

            
            #line default
            #line hidden
            
            #line 10 "..\..\Areas\Admin\Views\User\Edit.cshtml"
                            
    
            
            #line default
            #line hidden
            
            #line 11 "..\..\Areas\Admin\Views\User\Edit.cshtml"
Write(Html.ValidationSummary(true));

            
            #line default
            #line hidden
            
            #line 11 "..\..\Areas\Admin\Views\User\Edit.cshtml"
                                 


            
            #line default
            #line hidden
WriteLiteral("    <fieldset>\r\n        <legend>UserProfile</legend>\r\n\r\n");

WriteLiteral("        ");

            
            #line 16 "..\..\Areas\Admin\Views\User\Edit.cshtml"
   Write(Html.HiddenFor(model => model.UserId));

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n        <div");

WriteLiteral(" class=\"editor-label\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 19 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.LabelFor(model => model.UserName));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"editor-field\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 22 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.EditorFor(model => model.UserName));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 23 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.ValidationMessageFor(model => model.UserName));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n\r\n        <div");

WriteLiteral(" class=\"editor-label\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 27 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.LabelFor(model => model.FirstName));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"editor-field\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 30 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.EditorFor(model => model.FirstName));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 31 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.ValidationMessageFor(model => model.FirstName));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n\r\n        <div");

WriteLiteral(" class=\"editor-label\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 35 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.LabelFor(model => model.MiddleName));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"editor-field\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 38 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.EditorFor(model => model.MiddleName));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 39 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.ValidationMessageFor(model => model.MiddleName));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n\r\n        <div");

WriteLiteral(" class=\"editor-label\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 43 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.LabelFor(model => model.LastName));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"editor-field\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 46 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.EditorFor(model => model.LastName));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 47 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.ValidationMessageFor(model => model.LastName));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n\r\n        <div");

WriteLiteral(" class=\"editor-label\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 51 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.LabelFor(model => model.Phone));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"editor-field\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 54 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.EditorFor(model => model.Phone));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 55 "..\..\Areas\Admin\Views\User\Edit.cshtml"
       Write(Html.ValidationMessageFor(model => model.Phone));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n\r\n        <p>\r\n            <input");

WriteLiteral(" type=\"submit\"");

WriteLiteral(" value=\"Save\"");

WriteLiteral(" />\r\n        </p>\r\n    </fieldset>\r\n");

            
            #line 62 "..\..\Areas\Admin\Views\User\Edit.cshtml"
}

            
            #line default
            #line hidden
WriteLiteral("\r\n<div>\r\n");

WriteLiteral("    ");

            
            #line 65 "..\..\Areas\Admin\Views\User\Edit.cshtml"
Write(Html.ActionLink("Back to List", "Index"));

            
            #line default
            #line hidden
WriteLiteral("\r\n</div>\r\n\r\n");

        }
    }
}
#pragma warning restore 1591
