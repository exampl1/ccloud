﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Store.Controllers
{
    public class ErrorController : Controller
    {
        #region Http404
        public ActionResult Http404(Exception exception)
        {
            ActionResult result;

            if (!Request.IsAjaxRequest())
                result = View("Http404");
            else
                result = PartialView("Http404");

            // TODO: добавить реализацию ILogger
            return result;
        }
        #endregion

        #region Http500
        public ActionResult Http500(Exception exception)
        {
            ViewBag.Message = string.Format("Ref: {1}{0}IP: {2}{0}url: {3}{0}User: {4}{0}UserAgent: {5}{0}",
                Environment.NewLine, // 0
                HttpContext.Request.UrlReferrer != null ? HttpContext.Request.UrlReferrer.ToString() : "noref", // 1
                HttpContext.Request.UserHostAddress, // 2
                HttpContext.Request.Url.DnsSafeHost + HttpContext.Request.Unvalidated.RawUrl, // 3
                HttpContext.User.Identity.IsAuthenticated ? HttpContext.User.Identity.Name : "Anonymous", //4
                HttpContext.Request.UserAgent); //5
                
            if (exception != null)
            {
                ViewBag.Message += "\n\n" + exception.Message + "\n\n" + exception.StackTrace;
                if (exception.InnerException != null)
                {
                    ViewBag.Message += "\n\n" + exception.InnerException.Message + "\n\n" + exception.InnerException.StackTrace;
                }
            }


            using (SmtpClient _client = new SmtpClient("127.0.0.1"))
            {
                try
                {
                    _client.Send("root@" + C2.AppController.SupplierModel.Current.Domain, "ssysalov@yandex.ru", "Ошибка на сайте", ViewBag.Message);

                    //TODO: админу можно показать
                    if (!User.IsInRole("Admin"))
                    {
                        ViewBag.Message = "";
                    }
                }
                catch { }
            }

            ActionResult result;

            ViewBag.Message = ((string)ViewBag.Message).Replace("\n","<br/>\n");

            if (!Request.IsAjaxRequest())
                result = View("Http500");
            else
                result = PartialView("Http500");

            // TODO: добавить реализацию ILogger
            return result;
        }
        #endregion

    }
}
