﻿using C2.DataManager.Store;
using C2.DataMapping.Store;
using C2.WebApp.Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Store.Controllers
{
    public class ProductPageDataLoader : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            using (StoreContext dataContext = new StoreContext("DefaultConnection"))
            {
                var dataManager = new StoreDataManager(dataContext);
                ProductPageModel model = new ProductPageModel
                {
                    CustomerInfo = filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated ? dataContext.CustomerInfos.Where(c => c.UserName == filterContext.RequestContext.HttpContext.User.Identity.Name).FirstOrDefault() : null,
                    Basket = BasketController.GetOrder(dataContext, filterContext.HttpContext.Session),
                    Catalog = new WebApp.Models.TreeViewModel("_ProductGroupHeader",
                            dataManager.GetProductGroupClassifier().Roots,
                            null,
                            null)
                };
                filterContext.Controller.ViewData.SetPageModel(model);
            }
        }
    }
}