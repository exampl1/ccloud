﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Store.Models
{
    public class ContinuesCollectionModel
    {
        public long TotalCount
        {
            get;
            set;
        }

        public long LoadedCount
        {
            get;
            set;
        }
    }
}