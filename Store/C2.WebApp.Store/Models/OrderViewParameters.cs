﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using C2.DataModel.StoreModel;

namespace C2.WebApp.Store.Models
{
    public class OrderViewParameters
    {
        [Display(Name = "Заказчик")]
        public string CustomerId
        {
            get { return customerId != null ? customerId.Trim() : null; }
            set { customerId = value; }
        }
        private string customerId;

        [Display(Name = "Номер заказа")]
        public long? OrderNumber
        {
            get;
            set;
        }

        [Display(Name = "Период с")]
        public DateTime? From
        {
            get;
            set;
        }

        [Display(Name = "Период по")]
        public DateTime? To
        {
            get;
            set;
        }

        [Display(Name = "Состояние")]
        public OrderStatus Status
        {
            get;
            set;
        }

        [Display(Name = "Укомплектован")]
        public bool Complected
        {
            get;
            set;
        }

        [Display(Name = "Идёт комплектация")]
        public bool Uncomplected
        {
            get;
            set;
        }
    }
}