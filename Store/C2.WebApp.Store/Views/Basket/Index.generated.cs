﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace C2.WebApp.Store.Views.Basket
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "1.5.4.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Basket/Index.cshtml")]
    public partial class Index : System.Web.Mvc.WebViewPage<C2.WebApp.Store.Models.ProductPageModel>
    {
        public Index()
        {
        }
        public override void Execute()
        {
            
            #line 3 "..\..\Views\Basket\Index.cshtml"
  
    ViewBag.HtmlTitle = ViewBag.Title = "Корзина";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<h1>Корзина</h1>\r\n\r\n<div");

WriteLiteral(" id=\"basket-items\"");

WriteLiteral(">\r\n");

WriteLiteral("    ");

            
            #line 10 "..\..\Views\Basket\Index.cshtml"
Write(Html.Partial("~/Views/Basket/BasketItems.cshtml", Model.Basket));

            
            #line default
            #line hidden
WriteLiteral("\r\n</div>\r\n\r\n");

DefineSection("Scripts", () => {

WriteLiteral("\r\n    <script");

WriteLiteral(" type=\"text/javascript\"");

WriteLiteral(">\r\n        C2.basket.url = \"");

            
            #line 15 "..\..\Views\Basket\Index.cshtml"
                    Write(Url.Action("get", "basket"));

            
            #line default
            #line hidden
WriteLiteral(@""";

        $(function () {
            $(""body"").change(function (e) {
                if ($(e.target).hasClass(""offer-val"")) {
                    var offerId = $(e.target).attr(""data-offer-id"");
                    var quantity = $(e.target).val();
                    $.post(
                        """);

            
            #line 23 "..\..\Views\Basket\Index.cshtml"
                    Write(Url.Action("SetQuantity", "Basket"));

            
            #line default
            #line hidden
WriteLiteral(@"/"" + offerId,
                        {quantity: quantity},
                        function (data) {
                            $(""#basket-items"").html(data);
                            C2.basket.updateSucess();
                        }
                    );

                }
            });
        });
    </script>
");

});

        }
    }
}
#pragma warning restore 1591
