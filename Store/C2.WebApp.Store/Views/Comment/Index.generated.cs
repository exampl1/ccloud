﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace C2.WebApp.Store.Views.Comment
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "1.5.4.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Comment/Index.cshtml")]
    public partial class Index : System.Web.Mvc.WebViewPage<C2.DAL.SelectResult<C2.DataModel.Comment>>
    {
        public Index()
        {
        }
        public override void Execute()
        {
            
            #line 3 "..\..\Views\Comment\Index.cshtml"
  
    ViewBag.HtmlTitle = ViewBag.Title = "Отзывы";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<h1>Отзывы</h1>\r\n\r\n<div");

WriteLiteral(" id=\"comments\"");

WriteLiteral(">\r\n\r\n");

            
            #line 11 "..\..\Views\Comment\Index.cshtml"
 if (Model.TotalCount == 0)
{

            
            #line default
            #line hidden
WriteLiteral("    <p>Отзывов еще нет. Ваш отзыв может быть первым.</p>\r\n");

            
            #line 14 "..\..\Views\Comment\Index.cshtml"
} else {

            
            #line default
            #line hidden
WriteLiteral("    <div>\r\n");

            
            #line 16 "..\..\Views\Comment\Index.cshtml"
    
            
            #line default
            #line hidden
            
            #line 16 "..\..\Views\Comment\Index.cshtml"
     foreach (var item in Model.Items)
    {

            
            #line default
            #line hidden
WriteLiteral("        <div>");

            
            #line 18 "..\..\Views\Comment\Index.cshtml"
        Write(item.Created.ToShortDateString());

            
            #line default
            #line hidden
WriteLiteral(" от: <span>");

            
            #line 18 "..\..\Views\Comment\Index.cshtml"
                                                    Write(item.UserDisplayName);

            
            #line default
            #line hidden
WriteLiteral("</span></div>\r\n");

WriteLiteral("        <div>");

            
            #line 19 "..\..\Views\Comment\Index.cshtml"
        Write(item.Text);

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n");

WriteLiteral("        <div");

WriteLiteral(" style=\"border-bottom:dotted 1px black; margin-bottom:10px;\"");

WriteLiteral("></div>\r\n");

            
            #line 21 "..\..\Views\Comment\Index.cshtml"
    }

            
            #line default
            #line hidden
WriteLiteral("    </div>\r\n");

            
            #line 23 "..\..\Views\Comment\Index.cshtml"
    
    if (Model.PageCount > 1) {

            
            #line default
            #line hidden
WriteLiteral("    <div>\r\n");

WriteLiteral("        ");

            
            #line 26 "..\..\Views\Comment\Index.cshtml"
   Write(Html.Partial("_Pager"));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n");

            
            #line 28 "..\..\Views\Comment\Index.cshtml"
    }
}

            
            #line default
            #line hidden
WriteLiteral("\r\n");

            
            #line 31 "..\..\Views\Comment\Index.cshtml"
 if (User.Identity.IsAuthenticated) {
    
            
            #line default
            #line hidden
            
            #line 32 "..\..\Views\Comment\Index.cshtml"
Write(Html.Partial("_AddComment", new C2.DataModel.Comment { UserDisplayName = ViewBag.UserDisplayName as string }));

            
            #line default
            #line hidden
            
            #line 32 "..\..\Views\Comment\Index.cshtml"
                                                                                                                  
} else {

            
            #line default
            #line hidden
WriteLiteral("    <p>Чтобы оставить отзыв, вам необходимо ");

            
            #line 34 "..\..\Views\Comment\Index.cshtml"
                                       Write(Html.ActionLink("Авторизоваться", "Login", new { controller="Account", returnUrl=Request.RawUrl }));

            
            #line default
            #line hidden
WriteLiteral(" на сайте</p>\r\n");

            
            #line 35 "..\..\Views\Comment\Index.cshtml"
}

            
            #line default
            #line hidden
WriteLiteral("\r\n</div>\r\n");

        }
    }
}
#pragma warning restore 1591
