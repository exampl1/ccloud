﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Configuration;
using Util;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;
using System.Drawing;
using System.Net;
using C2.DataModel.MediaModel;

namespace StoreTestDataGenerator
{
    class Auto
    {
        public string Image { get; set; }
        public string Category { get; set; }
        public string VendorCode { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string Key
        {
            get
            {
                return GetKey(VendorCode);
            }
        }

        public string Collection { get; set; }
        public List<string> CollectionValue
        {
            get {
                var array = Collection.Split(',');
                List<string> collections = new List<string>();
                foreach(string col in collections)
                {
                    collections.Add(col);
                }

                return collections;
            }
        }


        public string PropertySet { get; set; }
        public string PropertyValue
        {
            get
            {
                string propertyValue = null;
                if (!string.IsNullOrEmpty(PropertySet))
                {

                    var propMatch = Regex.Matches(this.PropertySet, "<div>(.*?)</div>", RegexOptions.Singleline);
                    if (propMatch.Count > 0)
                    {
                        List<string> property = new List<string>();
                        for (int i = 0; i < propMatch.Count; i++)
                        {
                            property.Add(propMatch[i].Groups[1].Value.Trim());
                        }
                        propertyValue = string.Join(", ", property.ToArray());
                    }
                    else
                    {
                        propertyValue = PropertySet;
                    }
                }
                return propertyValue;
            }
        }


        private string price;
        public string Price
        {
            set { price = value; }
        }

        public int Qty
        {
            set;
            get;
        }
        
        public decimal PriceVal
        {
            get
            {
                decimal p = 0;
                decimal.TryParse(price, NumberStyles.Currency, CultureInfo.InvariantCulture, out p);
                return p;
            }
        }

        static IdGenerator idGenerator;
        static Subject subject;
        static PropertyType propertyComp;
        static Classifier root;
        static Unit itemUnits;


        static Auto()
        {
            idGenerator = IdGenerator.CreateIdentityGenerator(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString, "dbo.UidTable");

            using (var dbContext = new StoreContext("DefaultConnection"))
            {
                subject = dbContext.Subjects.Where(s => s.Code == Cfg.VendorCode).Single();
                propertyComp = GetPropertyComp(dbContext);
                root = dbContext.Classifiers.Where(c => c.Code == ProductGroup.DefaultClassifierCode).Single();
                itemUnits = GetUnit(dbContext);
                //SetupProductGroup(dbContext, "3M", null, "3M");
                //SetupProductGroup(dbContext, "Colad", null, "colad");
                //SetupProductGroup(dbContext, "Kimberly Clark", null, "kim");
                //SetupProductGroup(dbContext, "Hamach", null, "hamach");
                //SetupProductGroup(dbContext, "ProGLASS", null, "proglass");
                //SetupProductGroup(dbContext, "Katrin", null, "katrin");
                //SetupProductGroup(dbContext, "Adolf Bucher", null, "adolf");
                
            }

        }

        private void SaveImg(string url, Product product, StoreContext context, int n)
        {
            var web = new WebClient();
            var path = string.Format("/imgs/{1}/{0}/", GetProductFolderName(this.VendorCode), subject.Code);
            var localpath = "c:/inetpub/WebApp.Store" + path;
            var ext = Path.GetExtension(url);

            if (!Directory.Exists(localpath))
            {
                Directory.CreateDirectory(localpath);
            }

            var fileDef = string.Format("{1}{2}_full{0}", ext, localpath, n);
            var filePrv = string.Format("{1}{2}_prv{0}", ext, localpath, n);

            if (File.Exists(filePrv)) File.Delete(filePrv);
            File.Copy(url, fileDef, true);

            var bitmap = Bitmap.FromFile(fileDef);
            decimal x = (decimal)200 / bitmap.Height;
            bitmap.GetThumbnailImage((int)(bitmap.Width * x), 200, null, IntPtr.Zero).Save(filePrv, System.Drawing.Imaging.ImageFormat.Jpeg);

            var link = path + n.ToString() + "_{0}" + ext;

            var media = context.MediaItems.Where(m => m.PermaLink == link).FirstOrDefault();
            if (media == null)
            {
                media = context.MediaItems.Add(new C2.DataModel.MediaModel.MediaItem()
                {
                    Id = GetId(),
                    MediaType = C2.DataModel.MediaModel.MediaType.Picture,
                    PermaLink = link,
                });
                media.Localization.Add(new C2.DataModel.MediaModel.MediaItemLocalization()
                {
                    CultureCode = "ru-RU",
                    Title = this.Title,
                });
            }
            
            media.SetPictureMetaData(new List<PictureMetaData>
            {
                new PictureMetaData { Height = bitmap.Height, Width = bitmap.Width, PictureType = "full" },
                new PictureMetaData { Width = (int)(bitmap.Width * x), Height = 200, PictureType = "prv" }
            });
            bitmap.Dispose();

            var entry = context.MediaCollectionEntries.Where(e => e.CollectionHostId == product.Id && e.MediaItemId == media.Id).FirstOrDefault();
            if (entry == null)
            {
                entry = context.MediaCollectionEntries.Add(new C2.DataModel.MediaModel.MediaCollectionEntry()
                {
                    CollectionHostId = product.Id,
                    MediaItem = media
                });
            }

        }

        public void Update(StoreContext dbContext, Product product, string[] files)
        {
            dbContext.Classifiers.Attach(root);
            dbContext.Subjects.Attach(subject);
            dbContext.Units.Attach(itemUnits);

            product.LastUpdateDate = DateTime.UtcNow;
            if (!string.IsNullOrWhiteSpace(this.Description))
            {
                product.CurrentLocalization.Title = this.Title + " " + this.VendorCode;
                product.CurrentLocalization.Description = this.Description;
            }

            product.ProductGroups.Clear();
            SetupProductGroup(dbContext, product, Category, Collection, subject.Code);

            var propertyValue = PropertyValue;
            if (propertyValue != null)
            {
                //есть свойство "Состав"
                var propertyPropertyValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertyComp.Id && v.StringValue == propertyValue).FirstOrDefault();
                if (propertyPropertyValue == null)
                {
                    // св-ва нет в базе
                    propertyPropertyValue = new PropertyValue
                    {
                        Id = GetId(),
                        StringValue = propertyValue,
                        Status = EntityStatus.Normal,
                        PropertyTypeId = propertyComp.Id
                    };

                    product.Properties.Where(v => v.PropertyTypeId == propertyComp.Id).ToList().ForEach(
                        x => product.Properties.Remove(x));
                    
                    product.Properties.Add(propertyPropertyValue);
                }
            }


                var offer = product.Offers.FirstOrDefault();
                
                offer.Price = this.PriceVal;
                offer.StartDate = DateTime.UtcNow;
                offer.ExpiredDate = DateTime.MaxValue;
                offer.AvailQuantity = Qty;

            for (int i = 0; i < files.Count(); i++)
            {
                SaveImg(files[i], product, dbContext, i);
            }
            
            dbContext.SaveChanges();
        }

        public void Save(StoreContext dbContext, string[] files)
        {
            dbContext.Classifiers.Attach(root);
            dbContext.Subjects.Attach(subject);
            dbContext.Units.Attach(itemUnits);

            //foreach (var collection in this.CollectionValue)
            var category = SetupProductGroup(dbContext, Title, Collection);
            
            var product = dbContext.Products.Add(new Product()
            {
                Id = GetId(),
                Status = EntityStatus.Normal,
                VendorCode = this.VendorCode,
                ProductGroup = category,
                CreateDate = DateTime.UtcNow,
                LastUpdateDate = DateTime.UtcNow,
                Vendor = subject,
            });

            product.Localization.Add(dbContext.ProductLocalizations.Add(new ProductLocalization()
            {
                CultureCode = "ru-RU",
                Title = this.Title + " " + this.VendorCode,
                Description = this.Description
            }));

            product.Units.Add(itemUnits);

            var propertyValue = PropertyValue;
            if (propertyValue != null)
            {
                //есть свойство "Состав"
                var propertyPropertyValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertyComp.Id && v.StringValue == propertyValue).FirstOrDefault();
                if (propertyPropertyValue == null)
                {
                    propertyPropertyValue = new PropertyValue
                    {
                        Id = GetId(),
                        StringValue = propertyValue,
                        Status = EntityStatus.Normal,
                        PropertyTypeId = propertyComp.Id
                    };
                }

                product.Properties.Add(propertyPropertyValue);
            }

            var offer = dbContext.Offers.Add(new Offer()
            {
                Id = GetId(),
                CurrencyCode = "RUB",
                ProductId = product.Id,
                Status = EntityStatus.Normal,
                Unit = itemUnits,
                SupplierId = subject.Id,
                Price = this.PriceVal,
                StartDate = DateTime.UtcNow,
                ExpiredDate = DateTime.MaxValue,
                AvailQuantity = Qty,
            });

            for (int i = 0; i < files.Count(); i++)
            {
                SaveImg(files[i], product, dbContext, i);
            }
            
            dbContext.SaveChanges();
        }

        private static ProductGroup SetupProductGroup(StoreContext dbContext, string title, string collection, string code = null)
        {
            var catq = from cn in dbContext.ClassifierNodes
                       join pg in dbContext.ProductGroups on cn.EntityId equals pg.Id
                       select new { pg, cn };

            if (collection != null)
            {
                catq = from c in catq
                       where c.cn.Path.StartsWith(collection)
                       select c;
            }

            var category = catq.Where(g => g.pg.Localization.Any(l => l.Title == title)).Select(x => x.pg).FirstOrDefault();

            if (category == null)
            {
                Console.WriteLine("new ProductGroup: {0}{1}", collection, title);
                //если не нашли, то создаем
                category = dbContext.ProductGroups.Add(new ProductGroup
                {
                    Id = GetId(),
                    Status = EntityStatus.Normal,
                    Code = code,
                });
                category.Localization.Add(new DictionaryItemLocalization
                {
                    CultureCode = "ru-RU",
                    Title = title,
                    Description = title,
                });

                //небольшая защита от дурака, на предмет создания рутовых нод
                if (collection == null && code != null)
                {
                    var node = dbContext.ClassifierNodes.Add(new ClassifierNode
                    {
                        EntityId = category.Id,
                        Status = EntityStatus.Normal,
                        Id = GetId(),
                        Classifier = root,
                        Order = 0,
                        Code = code,
                    });

                    node.SetParent();

                    dbContext.SaveChanges();
                }
                else
                {
                    var pnode = dbContext.ClassifierNodes.Where(n => n.Path == collection).FirstOrDefault();
                    if (pnode != null)
                    {
                        var node = dbContext.ClassifierNodes.Add(new ClassifierNode
                        {
                            EntityId = category.Id,
                            Status = EntityStatus.Normal,
                            Id = GetId(),
                            Classifier = root,
                            Order = 0,
                        });

                        node.SetParent(pnode);

                        dbContext.SaveChanges();
                    }
                    else
                    {
                        throw new ArgumentException("ClassifierNode not found");
                    }
                }
            }
            return category;
        }

        private static Unit GetUnit(StoreContext dbContext)
        {
            var property = dbContext.Units.Where(t => t.Code == "unit").FirstOrDefault();
            if (property == null)
            {
                property = new Unit
                {
                    Code = "unit",
                    Id = GetId(),
                    Status = EntityStatus.Normal,
                };
                property.Localization.Add(new DictionaryItemLocalization
                {
                    CultureCode = "ru-RU",
                    Title = "шт.",
                    Description = "Штуки"
                });

                dbContext.Units.Add(property);
                dbContext.SaveChanges();
            }
            return property;
        }


        private static PropertyType GetPropertyComp(StoreContext dbContext)
        {
            var property = dbContext.PropertyTypes.Where(t => t.Code == "props").FirstOrDefault();
            if (property == null)
            {
                property = new PropertyType
                {
                    Code = "props",
                    Id = GetId(),
                    Status = EntityStatus.Normal,
                };
                property.Localization.Add(new DictionaryItemLocalization
                {
                    CultureCode = "ru-RU",
                    Title = "Характеристики",
                    Description = "Характеристики продукта"
                });
                dbContext.PropertyTypes.Add(property);
                dbContext.SaveChanges();
            }
            return property;
        }

        static long GetId()
        {
            return idGenerator.GetId();
        }

        public void TryRemove()
        {
        }

        public static string GetProductFolderName(string vendorCode)
        {
            foreach (char ch in Path.GetInvalidPathChars())
            {
                vendorCode = vendorCode.Replace(ch, '_');
            }
            
            return vendorCode;
        }


        public static string GetKey(string value)
        {
            return Regex.Replace(value, @"[^A-Za-z0-9А-Яа-я]", "").ToLower();
        }
    }
}
