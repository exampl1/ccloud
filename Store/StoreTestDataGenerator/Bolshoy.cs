﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Configuration;
using Util;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;
using System.Drawing;
using System.Net;
using C2.DataModel.MediaModel;

namespace StoreTestDataGenerator
{
    class Bolshoy
    {
        public string Image { get; set; }
        public string Category { get; set; }
        public string VendorCode { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        private string price;
        public string Price
        {
            set { price = value; }
        }

        public decimal PriceVal
        {
            get
            {
                decimal p = 0;
                decimal.TryParse(price, out p);
                return p;
            }
        }

        static IdGenerator idGenerator;
        static Subject subject;
        static Classifier root;

        static Bolshoy()
        {
            idGenerator = IdGenerator.CreateIdentityGenerator(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString, "dbo.UidTable");

            using (var dbContext = new StoreContext("DefaultConnection"))
            {
                subject = dbContext.Subjects.Where(s => s.Code == "bolshoy").FirstOrDefault();
                if (subject == null)
                {
                    subject = dbContext.Subjects.Add(new Subject()
                    {
                        Id = GetId(),
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Code = "bolshoy"
                    });

                    subject.Localization.Add(dbContext.SubjectLocalizations.Add(new SubjectLocalization()
                    {
                        CultureCode = "ru-RU",
                        FullName = "Большой праздник",
                        Name = "Большой праздник",
                        SubjectId = subject.Id,
                        Description = "Большой праздник",
                        ShortName = "bolshoy",
                    }));
                    
                    dbContext.SaveChanges();
                }
                root = dbContext.Classifiers.Where(c => c.Code == ProductGroup.DefaultClassifierCode).Single();
            }

        }

        private void SaveImg(string url, Product product, StoreContext context, int n)
        {
            var web = new WebClient();
            var path = string.Format("/imgs/bolshoy/{0}/", this.VendorCode.Replace("/", "_").Replace("\\", "_").Replace("+", "_").Replace(" ", "_"));
            var ext = Path.GetExtension(url);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var fileDef = string.Format("{1}{2}_full{0}", ext, path, n);
            var filePrv = string.Format("{1}{2}_prv{0}", ext, path, n);

            File.Copy(url, fileDef);

            var bitmap = Bitmap.FromFile(fileDef);
            decimal x = (decimal)200 / bitmap.Height;
            bitmap.GetThumbnailImage((int)(bitmap.Width * x), 200, null, IntPtr.Zero).Save(filePrv, System.Drawing.Imaging.ImageFormat.Jpeg);

            var media = context.MediaItems.Add(new C2.DataModel.MediaModel.MediaItem()
            {
                Id = GetId(),
                MediaType = C2.DataModel.MediaModel.MediaType.Picture,
                PermaLink = path + n.ToString() + "_{0}" + ext,
            });

            media.Localization.Add(new C2.DataModel.MediaModel.MediaItemLocalization()
            {
                CultureCode = "ru-RU",
                Title = this.Title,
            });


            var entry1 = context.MediaCollectionEntries.Add(new C2.DataModel.MediaModel.MediaCollectionEntry()
            {
                CollectionHostId = product.Id,
                MediaItem = media
            });

        }

        public void Save(StoreContext dbContext, string[] files)
        {
            dbContext.Classifiers.Attach(root);
            dbContext.Subjects.Attach(subject);
            var product = dbContext.Products.Where(p => p.VendorCode == this.VendorCode && p.Vendor.Id == subject.Id).FirstOrDefault();
            if (product != null)
            {
                //SaveImg(Grabber._base + this.Image, product, dbContext);
                return;
            }

            var catq = from cn in dbContext.ClassifierNodes
                       join pg in dbContext.ProductGroups on cn.EntityId equals pg.Id
                       where cn.Path.StartsWith(this.Collection)
                       select pg;

            var category = catq.Where(g => g.Localization.Any(l => l.Title == this.Category)).FirstOrDefault();

            if (category == null)
            {
                Console.WriteLine("new ProductGroup: {0}{1}", this.Collection, this.Category);
                //если не нашли, то создаем
                category = dbContext.ProductGroups.Add(new ProductGroup
                {
                    Id = GetId(),
                    Status = EntityStatus.Normal,
                });
                category.Localization.Add(new DictionaryItemLocalization
                {
                    CultureCode = "ru-RU",
                    Title = this.Category,
                    Description = this.Category,
                });
                var pnode = dbContext.ClassifierNodes.Where(n => n.Path == this.Collection).FirstOrDefault();
                if (pnode != null)
                {
                    var node = dbContext.ClassifierNodes.Add(new ClassifierNode
                    {
                        EntityId = category.Id,
                        Status = EntityStatus.Normal,
                        Id = GetId(),
                        Classifier = root,
                        Order = 0,
                    });

                    node.SetParent(pnode);
                }
                else
                {
                    throw new ArgumentException("ClassifierNode not found");
                }

                dbContext.SaveChanges();
            }

            var itemUnits = dbContext.Units.Where(u => u.Code == "unit").FirstOrDefault();

            product = dbContext.Products.Add(new Product()
            {
                Id = GetId(),
                Status = EntityStatus.Normal,
                VendorCode = this.VendorCode,
                //ProductGroup = category,
                CreateDate = DateTime.UtcNow,
                LastUpdateDate = DateTime.UtcNow,
                Vendor = subject,
            });

            product.Localization.Add(dbContext.ProductLocalizations.Add(new ProductLocalization()
            {
                CultureCode = "ru-RU",
                Title = this.Title,
                Description = this.Description
            }));

            product.Units.Add(itemUnits);

            var offer = dbContext.Offers.Add(new Offer()
            {
                Id = GetId(),
                CurrencyCode = "RUB",
                ProductId = product.Id,
                Status = EntityStatus.Normal,
                Unit = itemUnits,
                SupplierId = subject.Id,
                Price = this.PriceVal,
                StartDate = DateTime.UtcNow,
                ExpiredDate = DateTime.MaxValue,
                AvailQuantity = 0,
            });

            for (int i = 0; i < files.Count(); i++)
            {
                SaveImg(files[i], product, dbContext, i);
            }

        }

        static long GetId()
        {
            return idGenerator.GetId();
        }

        public void TryRemove()
        {
        }

        public string Collection { get; set; }
    }
}
