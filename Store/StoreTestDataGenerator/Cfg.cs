﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreTestDataGenerator
{
    static class Cfg
    {
        private static string vendorCode;
        public static string VendorCode
        {
            get { return vendorCode; }
            set
            {
                vendorCode = value;
                LookupPath = string.Format("/media/{0}/", vendorCode);
                
                if (VendorCode == "ROCK")
                {
                    DoPutInCommonCat = false;
                }

                if (VendorCode == "bbsw" || VendorCode == "okiss" || VendorCode == "hustler")
                {
                    Connection = "TNFConnection";
                    StoreRoot = "c:/inetpub/WebApp.Store.Sport";
                }

            }
        }

        public static string LookupPath;
        public static string ImportFile = "import.csv";
        public static string Watermark;
        public static bool DoPutInCommonCat = true;
        public static string ColorSetPattern = null;
        public static string SizeSetPattern = null;
        public static string CurrencyCode = "RUB";
        public static decimal PriceCorrection = 1;
        public static string StoreRoot = "c:/inetpub/WebApp.Store";
        public static string Connection = "DefaultConnection";
    }

}
