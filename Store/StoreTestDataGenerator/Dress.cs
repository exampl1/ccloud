﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Configuration;
using Util;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;
using System.Drawing;
using System.Net;
using System.Data.Entity;
using C2.DataModel.MediaModel;
using System.Data.Entity.Validation;

namespace StoreTestDataGenerator
{
    class Dress : IItem
    {
        public string [] Images { get; set; }
        /// <summary>
        /// Название категории
        /// </summary>
        public string Category { get; set; }
        public string VendorCode { get; set; }

        private string title;
        public string Title
        {
            get
            {
                if (string.IsNullOrWhiteSpace(title))
                {
                    return Category + "-" + VendorCode;
                }
                return title;
            }
            set { title = value; }
        }

        public string Brand { get; set; }
        public string Description { get; set; }
        public string SizeType { get; set; }

        public string Key
        {
            get
            {
                return GetKey(VendorCode);
            }
        }

        public string ExpectingFilename
        {
            get
            {
                return GetExpectingFilename(Key);
            }
        }

        /// <summary>
        /// Путь до коллекции типа D/
        /// </summary>
        public string Collection { get; set; }
        public List<string> CollectionValue
        {
            get {
                var array = Collection.Split(',');
                List<string> collections = new List<string>();
                if (array.Length > 0)
                {
                    foreach (string col in array)
                    {
                        collections.Add(col);
                    }
                }
                else
                {
                    collections.Add(Collection);
                }

                return collections;
            }
        }


        public string ColorSet { get; set; }
        public string[] Colors
        {
            get
            {
                if (string.IsNullOrEmpty(ColorSet))
                {
                    return null;
                }

                if (Cfg.ColorSetPattern != null)
                {
                    var match = Regex.Matches(this.ColorSet, Cfg.ColorSetPattern, RegexOptions.Singleline | RegexOptions.IgnoreCase);
                    if (match.Count > 0)
                    {
                        List<string> list = new List<string>();
                        for (int i = 0; i < match.Count; i++)
                        {
                            list.Add(match[i].Groups[1].Value.Trim());
                        }

                        return list.ToArray();
                    }
                }

                return ColorSet.Trim().Split(',');
            }
        }

        public string PropertySet { get; set; }
        public string PropertyValue
        {
            get
            {
                string propertyValue = null;
                if (!string.IsNullOrEmpty(PropertySet))
                {

                    var propMatch = Regex.Matches(this.PropertySet, "<div>(.*?)</div>", RegexOptions.Singleline);
                    if (propMatch.Count > 0)
                    {
                        List<string> property = new List<string>();
                        for (int i = 0; i < propMatch.Count; i++)
                        {
                            property.Add(propMatch[i].Groups[1].Value.Trim());
                        }
                        propertyValue = string.Join(", ", property.ToArray());
                    }
                    else
                    {
                        propertyValue = PropertySet;
                    }
                }
                return propertyValue;
            }
        }

        public string SizeSet { get; set; }
        public List<string> Sizes
        {
            get
            {
                if (string.IsNullOrEmpty(SizeSet))
                {
                    return null;
                }

                List<string> size = new List<string>();

                if (Cfg.SizeSetPattern != null)
                {
                    var match = Regex.Matches(this.SizeSet, Cfg.SizeSetPattern, RegexOptions.Singleline | RegexOptions.IgnoreCase);
                    if (match.Count > 0)
                    {
                        List<string> list = new List<string>();
                        for (int i = 0; i < match.Count; i++)
                        {
                            size.Add(match[i].Groups[1].Value.Trim());
                        }

                        return size;
                    }
                }

                var sizearray = SizeSet.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (sizearray.Length > 1)
                {
                    foreach (var s in sizearray)
                    {
                        size.Add(s.Trim());
                    }
                }
                else
                {
                    sizearray = SizeSet.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                    int begin_size = 36, end_size = 48;
                    if (sizearray.Length > 1)
                    {
                        int.TryParse(sizearray[0], out begin_size);
                        int.TryParse(sizearray[1], out end_size);

                        for (int i = begin_size; i < end_size; i = i + 2)
                        {
                            size.Add(i.ToString());
                        }
                    }
                    else
                    {
                        size.Add(SizeSet.Trim());
                    }

                }

                return size;
            }
        }

        private string price40;
        public string Price40
        {
            set { price40 = value; }
        }

        private string price20;
        public string Price20
        {
            set { price20 = value; }
        }

        private string price;
        public string Price
        {
            set { price = value; }
        }

        public int Qty
        {
            set;
            get;
        }

        
        public decimal PriceVal
        {
            get
            {
                return GetPrice(price) * Cfg.PriceCorrection;
            }
        }

        private decimal GetPrice(string price)
        {
            if (string.IsNullOrWhiteSpace(price)) return 0;
            decimal p = 0;
            decimal.TryParse(price.Replace(" ", ""), NumberStyles.Currency, CultureInfo.InvariantCulture, out p);
            return p;
        }
        
        public decimal? PriceVal20
        {
            get
            {
                var _price20 = GetPrice(price20);
                return _price20 == 0 ? null : (decimal?)_price20;
            }
        }
        
        public decimal PriceVal40
        {
            get
            {
                return GetPrice(price40);
            }
        }

        static PropertyType propertySize;
        static PropertyType propertyColor;
        static PropertyType propertyComp;
        static PropertyType propertySizeType;
        static Unit itemUnits;


        static Dress()
        {

            using (var dbContext = new StoreContext(Cfg.Connection))
            {
                propertySize = GetPropertySize(dbContext);
                propertyComp = GetPropertyComp(dbContext);
                propertyColor = GetPropertyColor(dbContext);
                propertySizeType = GetPropertySizeType(dbContext);
                itemUnits = GetUnit(dbContext);
            }

        }

        private void SaveImg(string url, Product product, StoreContext context, int n)
        {
            var web = new WebClient();
            var path = string.Format("/imgs/{1}/{0}/", DataManager.GetProductFolderName(this.VendorCode), DataManager.Subject.Code);
            var localpath = Cfg.StoreRoot + path;
            var ext = Path.GetExtension(url);
            if (string.IsNullOrEmpty(ext))
            {
                ext = ".jpg";
            }

            if (!Directory.Exists(localpath))
            {
                Directory.CreateDirectory(localpath);
            }

            var fileDef1 = string.Format("{1}{2}_full{0}", ext, localpath, n);
            var filePrv1 = string.Format("{1}{2}_prv{0}", ext, localpath, n);

            if (File.Exists(filePrv1)) File.Delete(filePrv1);
            if (File.Exists(fileDef1)) File.Delete(fileDef1);


            var fileDef = string.Format("{1}{2:0#}_full{0}", ext, localpath, n);
            var filePrv = string.Format("{1}{2:0#}_prv{0}", ext, localpath, n);

            if (File.Exists(filePrv)) File.Delete(filePrv);
            if (File.Exists(fileDef)) File.Delete(fileDef);
            web.DownloadFile(url, fileDef);
            //File.Copy(url, fileDef, true);

            //var bitmap = Bitmap.FromFile();
            var img = new Img(fileDef);

            decimal xF = (decimal)1200 / img.OriginalBitmap.Height;
            if (xF != 1)
            {
                img.DoResize((int)(img.OriginalBitmap.Width * xF), 1200);
                img.SaveAs(fileDef + ".tmp");
                img.Dispose();
                
                File.Delete(fileDef);
                File.Move(fileDef + ".tmp", fileDef);

                //bitmap = Bitmap.FromFile(fileDef);
                img = new Img(fileDef);
            }

            decimal xP = (decimal)200 / img.OriginalBitmap.Height;
            img.DoResize((int)(img.OriginalBitmap.Width * xP), 200);
            img.SaveAs(filePrv);

            var link = path + string.Format("{0:0#}", n) + "_{0}" + ext;

            var media = context.MediaItems.Where(m => m.PermaLink == link).FirstOrDefault();
            if (media == null)
            {
                media = context.MediaItems.Add(new C2.DataModel.MediaModel.MediaItem()
                {
                    Id = DataManager.GetId(),
                    MediaType = C2.DataModel.MediaModel.MediaType.Picture,
                    PermaLink = link,
                });
                media.Localization.Add(new C2.DataModel.MediaModel.MediaItemLocalization()
                {
                    CultureCode = "ru-RU",
                    Title = this.Title + " " + this.VendorCode,
                });
            }
            
            media.SetPictureMetaData(new List<PictureMetaData>
            {
                new PictureMetaData { Height = img.OriginalBitmap.Height, Width = img.OriginalBitmap.Width, PictureType = "full" },
                new PictureMetaData { Width = (int)(img.OriginalBitmap.Width * xP), Height = 200, PictureType = "prv" }
            });
            img.Dispose();

            var entry = context.MediaCollectionEntries.Where(e => e.CollectionHostId == product.Id && e.MediaItemId == media.Id).FirstOrDefault();
            if (entry == null)
            {
                entry = context.MediaCollectionEntries.Add(new C2.DataModel.MediaModel.MediaCollectionEntry()
                {
                    CollectionHostId = product.Id,
                    MediaItem = media
                });
            }

        }

        public void Update(StoreContext dbContext, Product product, string[] files)
        {
            dbContext.Classifiers.Attach(DataManager.Root);
            dbContext.Subjects.Attach(DataManager.Subject);
            dbContext.Units.Attach(itemUnits);

            product.LastUpdateDate = DateTime.UtcNow;

            if (product.CurrentLocalization == null)
            {
                product.Localization.Add(new ProductLocalization
                {
                    CultureCode = "ru-RU",
                });
            }

            if (!string.IsNullOrWhiteSpace(this.Description))
            {
                product.CurrentLocalization.Description = this.Description;
            }

            if (!string.IsNullOrWhiteSpace(this.Title))
            {
                product.CurrentLocalization.Title = this.Title;
            }

            if (files != null)
            {
                foreach (var collection in CollectionValue)
                {
                    DataManager.SetupProductGroup(dbContext, product, Category, collection, Brand);
                }
                //DataManager.SetupProductGroup(dbContext, product, Category, Collection, DataManager.Subject.Code);
            }

            var propertyValue = PropertyValue;
            if (propertyValue != null)
            {
                //есть свойство "Состав"
                var propertyPropertyValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertyComp.Id && v.StringValue == propertyValue).FirstOrDefault();
                if (propertyPropertyValue == null)
                {
                    // св-ва нет в базе
                    propertyPropertyValue = new PropertyValue
                    {
                        Id = DataManager.GetId(),
                        StringValue = propertyValue,
                        Status = EntityStatus.Normal,
                        PropertyTypeId = propertyComp.Id
                    };
                }

                product.Properties.Where(v => v.PropertyTypeId == propertyComp.Id).ToList().ForEach(
                    x => product.Properties.Remove(x));

                product.Properties.Add(propertyPropertyValue);
            }

            
            if (Sizes != null)
            {
                if (SizeType != null)
                {
                    //есть свойство "Тип размера"
                    var sizeTypeValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertySizeType.Id && v.StringValue == SizeType).FirstOrDefault();
                    if (sizeTypeValue == null)
                    {
                        // св-ва нет в базе
                        sizeTypeValue = new PropertyValue
                        {
                            Id = DataManager.GetId(),
                            StringValue = SizeType,
                            Status = EntityStatus.Normal,
                            PropertyTypeId = propertySizeType.Id
                        };

                    }

                    product.Properties.Where(v => v.PropertyTypeId == propertySizeType.Id).ToList().ForEach(
                        x => product.Properties.Remove(x));

                    product.Properties.Add(sizeTypeValue);
                } 

                foreach (string _size in Sizes)
                {
                    var sizeValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertySize.Id && v.StringValue == _size).FirstOrDefault();
                    if (sizeValue == null)
                    {
                        sizeValue = new PropertyValue
                        {
                            Id = DataManager.GetId(),
                            StringValue = _size,
                            Status = EntityStatus.Normal,
                            PropertyTypeId = propertySize.Id
                        };
                    }

                    var productSize = product.Properties.Where(p => p.Id == sizeValue.Id).FirstOrDefault();
                    if (productSize == null)
                    {
                        product.Properties.Add(sizeValue);
                        dbContext.SaveChanges();
                    }

                    if (Colors != null)
                    {
                        foreach (var color in Colors)
                        {
                            var colorValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertyColor.Id && v.StringValue == color).FirstOrDefault();
                            if (colorValue == null)
                            {
                                colorValue = new PropertyValue
                                {
                                    Id = DataManager.GetId(),
                                    StringValue = color,
                                    Status = EntityStatus.Normal,
                                    PropertyTypeId = propertyColor.Id
                                };
                                dbContext.SaveChanges();
                            }

                            var productColor = product.Properties.Where(p => p.Id == colorValue.Id).FirstOrDefault();
                            if (productColor == null)
                            {
                                product.Properties.Add(colorValue);
                                dbContext.SaveChanges();
                            }

                            //var offer = product.Offers.Where(o => o.Properties.Any(p => p.Id == sizeValue.Id)).FirstOrDefault();
                            var offersToRemove = product.Offers.Where(o => o.Properties.All(p => p.PropertyTypeId != propertyColor.Id)).ToList();
                            offersToRemove.ForEach(x => dbContext.Offers.Remove(x));

                            var offer = product.Offers.Where(o => o.Properties.Any(p => p.Id == sizeValue.Id) && o.Properties.Any(p => p.Id == colorValue.Id)).FirstOrDefault();
                            if (offer == null)
                            {
                                offer = dbContext.Offers.Add(new Offer()
                                {
                                    Id = DataManager.GetId(),
                                    CurrencyCode = Cfg.CurrencyCode,
                                    ProductId = product.Id,
                                    Status = EntityStatus.Normal,
                                    Unit = itemUnits,
                                    SupplierId = DataManager.Subject.Id,
                                    Price = this.PriceVal,
                                    PriceOpt20 = this.PriceVal20,
                                    PriceOpt40 = this.PriceVal40,
                                    StartDate = DateTime.UtcNow,
                                    ExpiredDate = DateTime.MaxValue,
                                    AvailQuantity = Qty,
                                });

                                offer.Properties.Add(sizeValue);
                                offer.Properties.Add(colorValue);
                                
                                dbContext.SaveChanges();
                            }
                            else
                            {
                                if (this.PriceVal > 0)
                                {
                                    offer.Price = this.PriceVal;
                                    offer.PriceOpt20 = this.PriceVal20;
                                    offer.PriceOpt40 = this.PriceVal40;
                                }
                                offer.StartDate = DateTime.UtcNow;
                                offer.ExpiredDate = DateTime.MaxValue;
                                offer.AvailQuantity = Qty;
                            }


                        }
                    }
                    else
                    {
                        if (files != null)
                        {
                            var offersToRemove = product.Offers.Where(o => o.Properties.Any(p => p.PropertyTypeId == propertyColor.Id)).ToList();
                            offersToRemove.ForEach(x => dbContext.Offers.Remove(x));
                        }

                        var offer = product.Offers.Where(o => o.Properties.Any(p => p.Id == sizeValue.Id)).FirstOrDefault();
                        if (offer == null)
                        {
                            offer = dbContext.Offers.Add(new Offer()
                            {
                                Id = DataManager.GetId(),
                                CurrencyCode = "RUB",
                                ProductId = product.Id,
                                Status = EntityStatus.Normal,
                                Unit = itemUnits,
                                SupplierId = DataManager.Subject.Id,
                                Price = this.PriceVal,
                                PriceOpt20 = this.PriceVal20,
                                PriceOpt40 = this.PriceVal40,
                                StartDate = DateTime.UtcNow,
                                ExpiredDate = DateTime.MaxValue,
                                AvailQuantity = Qty,
                            });

                            offer.Properties.Add(sizeValue);
                        }
                        else
                        {
                            if (this.PriceVal > 0)
                            {
                                offer.Price = this.PriceVal;
                                offer.PriceOpt20 = this.PriceVal20;
                                offer.PriceOpt40 = this.PriceVal40;
                            }
                            offer.StartDate = DateTime.UtcNow;
                            offer.ExpiredDate = DateTime.MaxValue;
                            offer.AvailQuantity = Qty;
                        }
                    }
                }
            }
            else
            {
                var offer = product.Offers.FirstOrDefault();
                if (offer == null)
                {
                    offer = dbContext.Offers.Add(new Offer()
                    {
                        Id = DataManager.GetId(),
                        CurrencyCode = "RUB",
                        ProductId = product.Id,
                        Status = EntityStatus.Normal,
                        Unit = itemUnits,
                        SupplierId = DataManager.Subject.Id,
                        Price = this.PriceVal,
                        PriceOpt20 = this.PriceVal20,
                        PriceOpt40 = this.PriceVal40,
                        StartDate = DateTime.UtcNow,
                        ExpiredDate = DateTime.MaxValue,
                        AvailQuantity = Qty,
                    });
                }
                else
                {

                    if (this.PriceVal > 0)
                    {
                        offer.Price = this.PriceVal;
                        offer.PriceOpt20 = this.PriceVal20;
                        offer.PriceOpt40 = this.PriceVal40;
                    }
                    offer.StartDate = DateTime.UtcNow;
                    offer.ExpiredDate = DateTime.MaxValue;
                    offer.AvailQuantity = Qty;
                }
            }

            if (files != null)
            {
                for (int i = 0; i < files.Count(); i++)
                {
                    SaveImg(files[i], product, dbContext, i);
                }
            }

            SaveProduct(dbContext, product);
        }

        public static void SaveProduct(StoreContext dbContext, Product product)
        {
            try
            {
                dbContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEntityValidationException)
            {
                foreach (var error in dbEntityValidationException.EntityValidationErrors)
                {
                    Console.WriteLine("Error writing row " + product.VendorCode + " with: " + string.Join(",", error.ValidationErrors.Select(e => e.ErrorMessage)));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error writing row " + product.VendorCode + " with: " + ex.Message);
            }
        }

        public void Save(StoreContext dbContext, string[] files)
        {
            dbContext.Classifiers.Attach(DataManager.Root);
            dbContext.Subjects.Attach(DataManager.Subject);
            dbContext.Units.Attach(itemUnits);
            var vendor = dbContext.Subjects.Where(s => s.Code == Brand).SingleOrDefault();

            if (vendor == null)
            {
                Console.WriteLine("Brand {0} not is unknown", Brand);
                return;
            }

            var product = dbContext.Products.Add(new Product()
            {
                Id = DataManager.GetId(),
                Status = EntityStatus.Normal,
                VendorCode = this.VendorCode,
                CreateDate = DateTime.UtcNow,
                LastUpdateDate = DateTime.UtcNow,
                Vendor = vendor,
            });

            product.ProductGroups.ToList().ForEach(x => product.ProductGroups.Remove(x));
            foreach (var collection in CollectionValue)
            {
                DataManager.SetupProductGroup(dbContext, product, Category, collection, Brand);
            }

            //DataManager.SetupProductGroup(dbContext, product, Category, Collection, DataManager.Subject.Code);
            product.Localization.Add(dbContext.ProductLocalizations.Add(new ProductLocalization()
            {
                CultureCode = "ru-RU",
                Title = this.Title,
                Description = this.Description
            }));

            product.Units.Add(itemUnits);

            var propertyValue = PropertyValue;
            if (propertyValue != null)
            {
                //есть свойство "Состав"
                var propertyPropertyValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertyComp.Id && v.StringValue == propertyValue).FirstOrDefault();
                if (propertyPropertyValue == null)
                {
                    propertyPropertyValue = new PropertyValue
                    {
                        Id = DataManager.GetId(),
                        StringValue = propertyValue,
                        Status = EntityStatus.Normal,
                        PropertyTypeId = propertyComp.Id
                    };
                }

                product.Properties.Add(propertyPropertyValue);
            }

            if (Sizes != null)
            {
                PropertyValue sizeTypeValue = null;
                if (SizeType != null)
                {
                    //есть свойство "Тип размера"
                    sizeTypeValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertySizeType.Id && v.StringValue == SizeType).FirstOrDefault();
                    if (sizeTypeValue == null)
                    {
                        // св-ва нет в базе
                        sizeTypeValue = new PropertyValue
                        {
                            Id = DataManager.GetId(),
                            StringValue = SizeType,
                            Status = EntityStatus.Normal,
                            PropertyTypeId = propertySizeType.Id
                        };
                    }

                    product.Properties.Add(sizeTypeValue);
                }

                foreach (string _size in Sizes)
                {
                    var sizeValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertySize.Id && v.StringValue == _size).FirstOrDefault();
                    if (sizeValue == null)
                    {
                        sizeValue = new PropertyValue
                        {
                            Id = DataManager.GetId(),
                            StringValue = _size,
                            Status = EntityStatus.Normal,
                            PropertyTypeId = propertySize.Id
                        };
                        dbContext.SaveChanges();
                    }
                    product.Properties.Add(sizeValue);

                    if (Colors != null)
                    {
                        foreach (var color in Colors)
                        {
                            var colorValue = dbContext.PropertyValues.Where(v => v.PropertyTypeId == propertyColor.Id && v.StringValue == color).FirstOrDefault();
                            if (colorValue == null)
                            {
                                colorValue = new PropertyValue
                                {
                                    Id = DataManager.GetId(),
                                    StringValue = color,
                                    Status = EntityStatus.Normal,
                                    PropertyTypeId = propertyColor.Id
                                };
                                dbContext.SaveChanges();
                            }

                            var productColor = product.Properties.Where(p => p.Id == colorValue.Id).FirstOrDefault();
                            if (productColor == null)
                            {
                                product.Properties.Add(colorValue);
                            }

                            var offer = dbContext.Offers.Add(new Offer()
                            {
                                Id = DataManager.GetId(),
                                CurrencyCode = "RUB",
                                ProductId = product.Id,
                                Status = EntityStatus.Normal,
                                Unit = itemUnits,
                                SupplierId = DataManager.Subject.Id,
                                Price = this.PriceVal,
                                PriceOpt20 = this.PriceVal20,
                                PriceOpt40 = this.PriceVal40,
                                StartDate = DateTime.UtcNow,
                                ExpiredDate = DateTime.MaxValue,
                                AvailQuantity = Qty,
                            });

                            offer.Properties.Add(sizeValue);
                            offer.Properties.Add(colorValue);
                            dbContext.SaveChanges();

                        }
                    }
                    else
                    {

                        var offer = dbContext.Offers.Add(new Offer()
                        {
                            Id = DataManager.GetId(),
                            CurrencyCode = "RUB",
                            ProductId = product.Id,
                            Status = EntityStatus.Normal,
                            Unit = itemUnits,
                            SupplierId = DataManager.Subject.Id,
                            Price = this.PriceVal,
                            PriceOpt20 = this.PriceVal20,
                            PriceOpt40 = this.PriceVal40,
                            StartDate = DateTime.UtcNow,
                            ExpiredDate = DateTime.MaxValue,
                            AvailQuantity = Qty,
                        });

                        offer.Properties.Add(sizeValue);

                    }
                }
            }
            else
            {
                var offer = dbContext.Offers.Add(new Offer()
                {
                    Id = DataManager.GetId(),
                    CurrencyCode = "RUB",
                    ProductId = product.Id,
                    Status = EntityStatus.Normal,
                    Unit = itemUnits,
                    SupplierId = DataManager.Subject.Id,
                    Price = this.PriceVal,
                    StartDate = DateTime.UtcNow,
                    ExpiredDate = DateTime.MaxValue,
                    AvailQuantity = Qty,
                });
            }

            for (int i = 0; i < files.Count(); i++)
            {
                SaveImg(files[i], product, dbContext, i);
            }

            SaveProduct(dbContext, product);
        }

        private static Unit GetUnit(StoreContext dbContext)
        {
            var property = dbContext.Units.Where(t => t.Code == "unit").FirstOrDefault();
            if (property == null)
            {
                property = new Unit
                {
                    Code = "unit",
                    Id = DataManager.GetId(),
                    Status = EntityStatus.Normal,
                };
                property.Localization.Add(new DictionaryItemLocalization
                {
                    CultureCode = "ru-RU",
                    Title = "шт.",
                    Description = "Штуки"
                });
                dbContext.Units.Add(property);
                dbContext.SaveChanges();
            }
            return property;
        }

        private static PropertyType GetPropertySizeType(StoreContext dbContext)
        {
            var property = dbContext.PropertyTypes.Where(t => t.Code == "sizeType").FirstOrDefault();
            if (property == null)
            {
                property = new PropertyType
                {
                    Code = "sizeType",
                    Id = DataManager.GetId(),
                    Status = EntityStatus.Normal,
                };
                property.Localization.Add(new DictionaryItemLocalization
                {
                    CultureCode = "ru-RU",
                    Title = "Тип размера",
                    Description = "Тип размера (европа, UK, и т.д.)"
                });
                dbContext.PropertyTypes.Add(property);
                dbContext.SaveChanges();
            }
            return property;
        }


        private static PropertyType GetPropertyColor(StoreContext dbContext)
        {
            var property = dbContext.PropertyTypes.Where(t => t.Code == "color").FirstOrDefault();
            if (property == null)
            {
                property = new PropertyType
                {
                    Code = "color",
                    Id = DataManager.GetId(),
                    Status = EntityStatus.Normal,
                };
                property.Localization.Add(new DictionaryItemLocalization
                {
                    CultureCode = "ru-RU",
                    Title = "Цвет",
                    Description = "Основной цвет изделия"
                });
                dbContext.PropertyTypes.Add(property);
                dbContext.SaveChanges();
            }
            return property;
        }

        private static PropertyType GetPropertyComp(StoreContext dbContext)
        {
            var property = dbContext.PropertyTypes.Where(t => t.Code == "composition").FirstOrDefault();
            if (property == null)
            {
                property = new PropertyType
                {
                    Code = "composition",
                    Id = DataManager.GetId(),
                    Status = EntityStatus.Normal,
                };
                property.Localization.Add(new DictionaryItemLocalization
                {
                    CultureCode = "ru-RU",
                    Title = "Состав",
                    Description = "Состав материала"
                });
                dbContext.PropertyTypes.Add(property);
                dbContext.SaveChanges();
            }
            return property;
        }

        private static PropertyType GetPropertySize(StoreContext dbContext)
        {
            var property = dbContext.PropertyTypes.Where(t => t.Code == "size").FirstOrDefault();
            if (property == null)
            {
                property = new PropertyType
                {
                    Code = "size",
                    Id = DataManager.GetId(),
                    Status = EntityStatus.Normal,
                };
                property.Localization.Add(new DictionaryItemLocalization
                {
                    CultureCode = "ru-RU",
                    Title = "Размер",
                    Description = "Размер изделия"
                });
                dbContext.PropertyTypes.Add(property);
                dbContext.SaveChanges();
            }
            return property;
        }

        public void TryRemove()
        {
        }


        public static string GetExpectingFilename(string value)
        {
            return Regex.Replace(value.Trim(), @"[^A-Za-z0-9А-Яа-я]", "").ToLower();

            //var expecting_filename = value;
            //Path.GetInvalidFileNameChars().ToList().ForEach(c => { expecting_filename = expecting_filename.Replace(c, '_'); });
            //return expecting_filename.ToLower();
        }


        public static string GetKey(string value)
        {
            return Regex.Replace(value.Trim(), @"[^A-Za-z0-9\-А-Яа-я /.]", "").ToLower();
        }
    }
}
