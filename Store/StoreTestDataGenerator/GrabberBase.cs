﻿using C2.DataMapping.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Util;
using System.Data.Entity;

namespace StoreTestDataGenerator
{
    delegate string ImgEvaluator(string input, IItem item);

    abstract class GrabberBase : IGrabber
    {
        public static string _start_ulr = @"http://primalinea.ru/catalog";
        public static string _base = @"http://primalinea.ru";
        public static string _imgPattern = @"<a href=[""'](http://primalinea.ru/images/.*?)[""']";
        public static string _collection = @"D/";
        public static string _pageItemPattern = "{0}/category/{1}/all/0/";
        public static string _startCatalogString = "class=\"catalog-items";
        public static string _catalogItemPattern = @"href=[""'].*?(/catalog/item/(.*?))[""']";
        public static string _brand;

        public static ImgEvaluator ImgEvaluator = null;
        
        protected static Dictionary<string, string> xBind = new Dictionary<string, string>();

        protected T Get<T>(string url) where T : IItem, new()
        {
            string _page = new Http().GetPage(url);
            Type type = typeof(T);
            T obj = new T();

            foreach (string key in xBind.Keys)
            {
                Match m = Regex.Match(_page, xBind[key], RegexOptions.Singleline | RegexOptions.IgnoreCase);
                if (m.Success)
                {
                    string value = m.Groups[1].Value.Trim();
                    value = Regex.Replace(value, @"[^A-Za-z0-9\-А-Яа-я /.,;]", "");
                    type.InvokeMember(key, BindingFlags.SetProperty | BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance, null, obj, new object[] { value });
                }
            }

            var images = new HashSet<string>();
            foreach (Match m in Regex.Matches(_page, _imgPattern))
            {
                if (m.Success)
                {
                    var src = _base + m.Groups[1].Value;
                    if (ImgEvaluator != null)
                    {
                        src = ImgEvaluator(m.Groups[1].Value, obj);
                    }
                    if (!images.Contains(src))
                    {
                        images.Add(src);

                    }

                }
            }
            
            obj.Images = images.ToArray();
            return obj;
        }

        protected void SaveFromUrl(StoreContext dbContext, string url)
        {
            if (string.IsNullOrWhiteSpace(url)) return;

            string _url = "";
            Dress dress = null;
            try
            {
                Console.WriteLine("Working on {0}", url);
                _url = url.StartsWith(_base) ? url : _base + url;
                dress = Get<Dress>(_url);

                dress.Collection = _collection;
                dress.Brand = _brand;
                dress.Qty = 5;

                var product = dbContext.Products
                    .Include(p => p.Localization)
                    .Include(p => p.Properties)
                    .Include(p => p.ProductGroups)
                    .Include(p => p.Offers)
                    .Include(p => p.Offers.Select(x => x.Properties))
                    .Where(p => p.VendorCode == dress.VendorCode).FirstOrDefault();

                if (product != null)
                {
                    dress.Update(dbContext, product, dress.Images);
                }
                else
                {
                    if (dress.Images.Length > 0)
                    {
                        dress.Save(dbContext, dress.Images);
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach (var error in ex.EntityValidationErrors)
                {
                    Console.WriteLine(string.Join(",", error.ValidationErrors.Select(e => e.ErrorMessage)));
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    if (ex.InnerException.InnerException != null)
                    {
                        Console.WriteLine(ex.InnerException.InnerException.Message);
                    }
                    else
                    {
                        Console.WriteLine(ex.InnerException.Message);
                    }
                }
                else
                {
                    Console.WriteLine(ex.Message);
                }

                if (dress != null)
                {
                    dress.TryRemove();
                }
            }
        }

        /// <summary>
        /// список Url-ов с итемами на странице p со списком
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        protected string[] FindItemPages(int page)
        {
            string url = string.Format(_pageItemPattern, _start_ulr, page.ToString());
            return GetDressList(new Http().GetPage(url));
        }

        /// <summary>
        /// список Url-ов с итемами из контента
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        protected string[] GetDressList(string content)
        {

            Dictionary<string, string> _list = new Dictionary<string, string>();
            int tabel_start = content.IndexOf(_startCatalogString);
            if (tabel_start > 0)
            {
                content = content.Substring(tabel_start);
                foreach (Match m in Regex.Matches(content, _catalogItemPattern, RegexOptions.IgnoreCase))
                {
                    if (!_list.ContainsKey(m.Groups[2].Value))
                    {
                        _list.Add(m.Groups[2].Value, m.Groups[1].Value);
                    }
                }
            }
            return _list.Values.ToArray();
        }

    }
}
