﻿using C2.DataMapping.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Util;
using System.Data.Entity;

namespace StoreTestDataGenerator
{
    class GrabberOKiss : GrabberBase
    {
        static GrabberOKiss()
        {
            _base = @"http://www.o-kiss.cn";
            _start_ulr = _base + @"/products_list/";
            _imgPattern = @"jqimg=\s*[""'](.*?)[""']";
            _collection = @"8025/";
            _brand = "okiss";
            _pageItemPattern = "{0}&brandId=1&pageNo_Product_list01-105={1}&pageSize_Product_list01-105=60.html";
            _startCatalogString = "mainul productlist-02";
            _catalogItemPattern = @"href=[""'](/products_detail/&amp;productId=(\d+)\.html)";

            xBind.Add("Category", @"<h1 class=[""']htmlinline[""']>(.*?)</");
            xBind.Add("Price", @"<li class=[""']marketprice.*?<cite>(\d+).00");
            xBind.Add("VendorCode", @"<li class=[""']number.*?<em>(\w+)");
            xBind.Add("Title", @"");
            xBind.Add("SizeSet", @"");
            xBind.Add("PropertySet", @"");
            //xBind.Add("ColorSet", "Цвет: (.*?)");
            xBind.Add("Description", @"");
        }

        public void Grab()
        {
            using (var dbContext = new StoreContext(Cfg.Connection))
            {
                foreach (int p in new int[] { 1, 2 })
                {
                    FindItemPages(p).ToList().ForEach(x => SaveFromUrl(dbContext, x));
                }
            }
        }

    }
}
