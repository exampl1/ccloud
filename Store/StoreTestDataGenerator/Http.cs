﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Data.SqlClient;

namespace Util
{
    public class Http
    {
        public static CookieContainer Cookie = null;
        public string GetPage(string _url)
        {
            return GetPage(_url, "");
        }

        public string GetPage(string _url, string request)
        {
            _url = _url.Replace("&amp;", "&");
            string res = "";

            try
            {
                using (var resp = GetPageResponse(_url, request))
                {
                    if (resp.Cookies != null && resp.Cookies.Count > 0)
                    {
                        Cookie = new CookieContainer();
                        foreach (Cookie c in resp.Cookies)
                        {
                            Cookie.Add(c);
                        }
                    }
                    using (Stream s = resp.GetResponseStream())
                    {
                        res = new StreamReader(s, Encoding.UTF8).ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
            }

            return res;
        }

        private bool PageIsValid(string res)
        {
            return res.IndexOf("???") > 0;
        }

        public HttpWebResponse GetPageResponse(string _url)
        {
            return GetPageResponse(_url, "");
        }

        public HttpWebResponse GetPageResponse(string _url, string request)
        {
            HttpWebRequest req = HttpWebRequest.Create(_url) as HttpWebRequest;
            if (Cookie != null)
            {
                req.CookieContainer = Cookie;
                req.UserAgent = StoreTestDataGenerator.Properties.Settings.Default.userAgent;
            }

            if (request.Length > 0)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(request);

                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = bytes.Length;

                using (var s = req.GetRequestStream())
                {
                    s.Write(bytes, 0, bytes.Length);
                    s.Close();
                }

            }
            else
            {
                req.Method = "GET";
            }

            return req.GetResponse() as HttpWebResponse;
        }



    }
}
