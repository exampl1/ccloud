﻿using System;
namespace StoreTestDataGenerator
{
    interface IItem
    {
        string Category { get; set; }
        string Description { get; set; }
        string[] Images { get; set; }
        string Key { get; }
        string Price { set; }
        int Qty { get; set; }
        void Save(C2.DataMapping.Store.StoreContext dbContext, string[] files);
        string SizeSet { get; set; }
        string Title { get; set; }
        string VendorCode { get; set; }
    }
}
