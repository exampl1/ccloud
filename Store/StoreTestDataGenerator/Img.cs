﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Xml.Serialization;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Xml;
using System.Net;


namespace Util
{
    /// <summary>
    /// Img class handles the image processing: loading, resizing, saving;
    /// </summary>
    public class Img : IDisposable
    {
        private string m_FileName;
        private int m_Width;
        private int m_Height;
        private int m_MaxAllowedSize = 1024;
        private byte[] m_ImageBuffer = null;
        private Bitmap m_Bitmap = null;
        private Bitmap m_ResizedBitmap = null;
        private Graphics graphicsRef;
        public Bitmap ResizedBitmap
        {
            get { return m_ResizedBitmap; }
        }

        public event EventHandler BeforeProcessing;
        public event EventHandler AfterProcessing;

        /// <summary>
        /// Creates Img from BitMap
        /// </summary>
        /// <param name="bitMap"></param>
        public Img(Bitmap bitMap)
        {
            this.m_Bitmap = this.m_ResizedBitmap = bitMap;
        }

        /// <summary>
        /// Creates Img from file
        /// </summary>
        /// <param name="originailFileName"></param>
        public Img(string originailFileName)
        {
            this.m_FileName = originailFileName;
            this.m_ImageFileInfo = new FileInfo(originailFileName);
        }

        /// <summary>
        /// CreatesImg from file and set Maximum Size of the image
        /// </summary>
        /// <param name="originailFileName"></param>
        /// <param name="size"></param>
        public Img(string originailFileName, int size)
        {
            this.m_FileName = originailFileName;
            this.m_ImageFileInfo = new FileInfo(originailFileName);
            this.m_MaxAllowedSize = size;
        }

        //public void InitFrom(Img img) {
        //    this.m_FileName = img.m_FileName;
        //    this.m_ImageFileInfo = img.m_ImageFileInfo;
        //    this.m_Width = img.m_Width;
        //    this.m_Height = img.m_Height;
        //    this.m_ImageBuffer = img.m_ImageBuffer;
        //    this.m_Bitmap = img.m_Bitmap;
        //}

        private FileInfo m_ImageFileInfo;
        /// <summary>
        /// Returns FileInfo of ImageFile
        /// </summary>
        public FileInfo ImageFileInfo
        {
            get { return m_ImageFileInfo; }
        }

        private int MaxAllowedSize
        {
            get { return this.m_MaxAllowedSize; }
        }

        private bool exceedImageSize()
        {
            return ((Width > MaxAllowedSize) || (Height > MaxAllowedSize));
        }

        /// <summary>
        /// Processing resize of Image
        /// </summary>
        public void DoResize()
        {
            this.processResizing();
        }

        /// <summary>
        /// Processing resize of image with specifying start and end dimentions
        /// </summary>
        /// <param name="originalW"></param>
        /// <param name="originalH"></param>
        /// <param name="maxW"></param>
        /// <param name="maxH"></param>
        public void DoResize(int originalW, int originalH, int maxW, int maxH)
        {
            if (originalH > maxH || originalW > maxW)
            {
                float rel = (float)originalW / (float)originalH;

                float resizedH = maxW / rel;
                float resizedW = maxH * rel;
                int resizeByH = 0, resizeByW = 0;

                if (resizedH <= maxH)
                {
                    resizeByW = maxW;
                    resizeByH = (int)resizedH;
                }
                else
                { /*resizedW <= w*/
                    resizeByH = maxH;
                    resizeByW = (int)resizedW;
                }

                this.DoResize(resizeByW > resizeByH ? resizeByW : resizeByH);
            }
        }

        /// <summary>
        /// Resizing image with max dimention by X or Y = <paramref name="maxSize"/>
        /// </summary>
        /// <param name="maxSize"></param>
        public void DoResize(int maxSize)
        {
            this.m_MaxAllowedSize = maxSize;
            this.processResizing();
        }

        /// <summary>
        /// Resizes image with maximal width = <paramref name="width"/> and/or maximal height = <paramref name="height"/>
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void DoResize(int width, int height)
        {
            using (Bitmap srcBmp = new Bitmap(m_FileName))
            {
                this.m_MaxAllowedSize = srcBmp.Width < srcBmp.Height ? height : width;
            }
            this.processResizing();
            this.processCliping(width, height);
        }

        private void processCliping(int width, int height)
        {
            if (BeforeProcessing != null)
                BeforeProcessing(this, new EventArgs());

            Bitmap srcBitmap = (Bitmap)this.m_ResizedBitmap.Clone();
            
            try
            {
                this.m_ResizedBitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
                m_ResizedBitmap.SetResolution(96, 96);
                Graphics graphicsRef = Graphics.FromImage(m_ResizedBitmap);
                graphicsRef.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphicsRef.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                Rectangle destRect;
                if (srcBitmap.Width == this.m_ResizedBitmap.Width)
                {
                    destRect = new Rectangle(0, (this.m_ResizedBitmap.Height - srcBitmap.Height) / 2, srcBitmap.Width, srcBitmap.Height);
                }
                else
                {
                    destRect = new Rectangle((this.m_ResizedBitmap.Width - srcBitmap.Width) / 2, 0, srcBitmap.Width, srcBitmap.Height);
                }
                graphicsRef.DrawImage(srcBitmap, destRect);

                if (!string.IsNullOrWhiteSpace(StoreTestDataGenerator.Cfg.Watermark))
                {
                    Bitmap wmBitmap = new Bitmap(StoreTestDataGenerator.Cfg.Watermark);
                    graphicsRef.DrawImage(wmBitmap, destRect);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (AfterProcessing != null)
                AfterProcessing(this, new EventArgs());
        }

        private void processResizing()
        {
            if (BeforeProcessing != null)
                BeforeProcessing(this, new EventArgs());

            try
            {
                if (this.m_Bitmap != null)
                    ResizeBitmap(this.m_Bitmap);
                else if (File.Exists(m_FileName))
                {
                    Bitmap srcBmp = new Bitmap(m_FileName);
                    ResizeBitmap(srcBmp);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (AfterProcessing != null)
                AfterProcessing(this, new EventArgs());
        }

        /// <summary>
        /// Resizes Bitmap <paramref name="srcBmp"/> with size MaxAllowedSize
        /// </summary>
        /// <param name="srcBmp"></param>
        public void ResizeBitmap(Bitmap srcBmp)
        {
            m_Width = srcBmp.Width;
            m_Height = srcBmp.Height;

            //if (exceedImageSize()) {
            this.m_ResizedBitmap = new Bitmap(GetResizedWidth(MaxAllowedSize), GetResizedHeight(MaxAllowedSize), PixelFormat.Format32bppArgb); // PixelFormat.Format24bppRgb);
            m_ResizedBitmap.SetResolution(96, 96);
            
            this.graphicsRef = Graphics.FromImage(m_ResizedBitmap);
            
            graphicsRef.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphicsRef.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            Rectangle destRect = new Rectangle(0, 0, m_ResizedBitmap.Width, m_ResizedBitmap.Height);

            if (this.m_ImageFileInfo != null)
            {
                if (this.m_ImageFileInfo.Extension == ".gif")
                {
                    //srcBmp.MakeTransparent();
                    graphicsRef.DrawRectangle(Pens.White, destRect);
                    graphicsRef.FillRectangle(Brushes.White, destRect);
                }
            }

            this.graphicsRef.DrawImage(srcBmp, destRect); //, 0, 0, srcBmp.Width, srcBmp.Height, GraphicsUnit.Pixel, ImgAttr);                            

            //}
        }

        /// <summary>
        /// Saves Image to file <paramref name="fileName"/>
        /// </summary>
        /// <param name="fileName"></param>
        public void SaveAs(string fileName)
        {
            ImageCodecInfo jpegCodec = GetImageCodec("image/jpeg");
            if (jpegCodec != null)
            {
                EncoderParameters codecParams = new EncoderParameters(2);
                codecParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90L);
                codecParams.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.RenderMethod, (long)EncoderValue.RenderNonProgressive);
                m_ResizedBitmap.Save(fileName, jpegCodec, codecParams);
            }
            else
            {
                m_ResizedBitmap.Save(fileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
        }

        /// <summary>
        /// Returns info about Image Type
        /// </summary>
        /// <param name="mimeType"></param>
        /// <returns></returns>
        private ImageCodecInfo GetImageCodec(string mimeType)
        {
            foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageEncoders())
                if (codec.MimeType == mimeType) return codec;
            return null;
        }

        public Bitmap OriginalBitmap
        {
            get
            {
                if (File.Exists(this.m_FileName))
                {
                    if (this.m_Bitmap == null)
                    {
                        this.m_Bitmap = new Bitmap(this.m_FileName);
                    }
                    return m_Bitmap;
                }
                else
                {
                    return null;
                }
            }

            set { this.m_Bitmap = value; }
        }

        public byte[] ImageBuffer
        {
            get { return m_ImageBuffer; }
        }

        private void DisposeBuffer()
        {
            this.m_ImageBuffer = null;
        }

        public int Width
        {
            get { return this.m_Width; }
        }

        public int Height
        {
            get { return this.m_Height; }
        }

        public int GetResizedWidth(int refSize)
        {
            int resizedWidth = this.Width;
            if ((this.Width > refSize || this.Height > refSize) && (this.Width > 0) && (this.Height > 0))
            {
            }

            if (this.Width < this.Height)
            {
                resizedWidth = refSize * this.Width / this.Height;
            }
            else
            {
                resizedWidth = refSize;
            }
            return resizedWidth;
        }

        public int GetResizedHeight(int refSize)
        {
            int resizedHeight = this.Height;
            if ((this.Width > refSize || this.Height > refSize) && (this.Width > 0) && (this.Height > 0))
            {
            }

            if (this.Width > this.Height)
            {
                resizedHeight = refSize * this.Height / this.Width;
            }
            else
            {
                resizedHeight = refSize;
            }
            return resizedHeight;
        }

        public override string ToString()
        {
            if (this.m_ImageFileInfo != null)
            {
                return "FileName: " + this.m_ImageFileInfo.FullName.ToString() +
                    ", Width: " + this.Width.ToString() +
                    ", Height: " + this.Height.ToString() +
                    ", FileLength: " + this.m_ImageFileInfo.Length.ToString();
            }
            else
            {
                return "Image initialized from Bitmap. Original Width: " + this.m_Bitmap.Size.Width.ToString() +
                    ", Original Height: " + this.m_Bitmap.Size.Height.ToString();
            }
        }


        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (m_Bitmap != null) m_Bitmap.Dispose();
                    if (m_ResizedBitmap != null) m_ResizedBitmap.Dispose();
                }
            }
            _disposed = true;
        }
    }

}
