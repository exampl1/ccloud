﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Data.Entity;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using CsvHelper;
using System.Data.Entity.Validation;

namespace StoreTestDataGenerator
{


    class ProgramDress
    {
        static void Main(string[] args)
        {

            if (args.Length > 0)
            {
                Cfg.VendorCode = args[0];
                
                if (args.Length > 1)
                {
                    Cfg.ImportFile = args[1];
                }

                if (args.Length > 2)
                {
                    Cfg.Watermark = args[2];
                }
            }

            ResourceManager.Init(new FakeResourceProvider(), new List<CultureInfo> { CultureInfo.GetCultureInfo("ru-RU") }, CultureInfo.GetCultureInfo("ru-RU"));

            CsvReader reader = new CsvReader(new StreamReader(File.OpenRead(Cfg.ImportFile), Encoding.Default));
            reader.Configuration.Delimiter = ';';
            reader.Configuration.FieldCount = 10;
            reader.Configuration.Quote = '"';

            List<Dress> records = new List<Dress>();
            while (reader.Read())
            {
                try
                {
                    var vcode = Dress.GetKey(reader.GetField(0));
                    if (!string.IsNullOrEmpty(vcode))
                    {
                        records.Add(new Dress
                        {
                            VendorCode = vcode,
                            ColorSet = reader.GetField(1).Trim(),
                            PropertySet = reader.GetField(2).Trim(),
                            SizeSet = reader.GetField(3).Trim(),
                            Qty = reader.GetField<int>(4),

                            Price40 = reader.GetField(5).Trim(),
                            Price20 = reader.GetField(6).Trim(),
                            Price = reader.GetField(7).Trim(),

                            Title = reader.GetField(8).Trim(),
                            Category = reader.GetField(9).Trim(),

                            Collection = reader.GetField(10).Trim(),
                            Brand = reader.GetField(11).Trim(),
                            SizeType = reader.GetField(12).Trim(),
                            Description = reader.GetField(13).Trim(),
                        });
                    }

                    Console.WriteLine("item found: {0}", reader.GetField(0));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("error: {0}", ex.Message);
                }
            }

            var allfiles = Directory.GetFiles(Cfg.LookupPath);

            //Хэш с файлами вида vendorcode => {file1, file2,..}
            Dictionary<string, List<string>> files = new Dictionary<string, List<string>>();

            foreach (var file in allfiles) // .OrderByDescending(x => x)
            {
                Console.WriteLine("{1}file found: {0}", file, Environment.NewLine);

                var nameonly = Dress.GetExpectingFilename(Path.GetFileNameWithoutExtension(file));

                var file_keys = records.Where(r => nameonly.Contains(r.ExpectingFilename)).Select(x => x.Key).ToList();

                foreach (var k in file_keys)
                {
                    Console.WriteLine("key found: {0}", k);
                    if (!files.ContainsKey(k))
                    {
                        files.Add(k, new List<string>(new string[] { file }));
                    }
                    else
                    {
                        if (!files[k].Contains(file))
                        {
                            files[k].Add(file);
                        }
                    }
                }

            }


            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            HashSet<string> worked = new HashSet<string>();

            int total = 0;
            using (var dbContext = new StoreContext(Cfg.Connection))
            {
                var prods = dbContext.Products
                    .Include(p => p.Localization)
                    .Include(p => p.Properties)
                    .Include(p => p.ProductGroups)
                    .Include(p => p.Offers)
                    .Include(p => p.Offers.Select(x=>x.Properties));

                foreach (var rec in records)
                {
                    bool hasFile = false;
                    var key = "";
                    foreach (var file_key in files.Keys.OrderBy(x => x))
                    {
                        if (file_key.Contains(rec.Key))
                        {
                            //файл есть.
                            hasFile = true;
                            key = file_key;
                            break;
                        }
                    }

                    if (!hasFile || files[key].Count() == 0)
                    {
                        Console.WriteLine("item {0} has no expecting file {1}", rec.VendorCode, rec.ExpectingFilename);
                        continue;
                    }

                    //Console.WriteLine("working: {0}, file {1}", rec.VendorCode, files[key]);

                    var product = prods.Where(p => p.VendorCode == rec.VendorCode).FirstOrDefault();
                    if (product != null)
                    {
                        //продукт есть. обновим данные.
                        if (!worked.Contains(key) && !string.IsNullOrWhiteSpace(rec.Category))
                        {
                            dbContext.MediaCollectionEntries.Where(e => e.CollectionHostId == product.Id).ToList().ForEach(x => dbContext.MediaCollectionEntries.Remove(x));
                            product.ProductGroups.ToList().ForEach(x => product.ProductGroups.Remove(x));
                            Dress.SaveProduct(dbContext, product);
                            rec.Update(dbContext, product, (files[key] as List<string>).ToArray());
                        }
                        else
                        {
                            rec.Update(dbContext, product, null);
                        }
                    }
                    else
                    {
                        rec.Save(dbContext, (files[key] as List<string>).ToArray());
                    }
                    worked.Add(key);
                    total++;

                    Dress.SaveProduct(dbContext, product);
                }

            }

            Console.WriteLine("Done {0} rows", total);
        }

    }
}
