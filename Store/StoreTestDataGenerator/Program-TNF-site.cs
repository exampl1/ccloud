﻿using C2.Globalization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace StoreTestDataGenerator
{


    class ProgramGrabTNF
    {

        static void Main(string[] args)
        {
            Cfg.VendorCode = "tnf";
            
            ResourceManager.Init(new FakeResourceProvider(), new List<CultureInfo> { CultureInfo.GetCultureInfo("ru-RU") }, CultureInfo.GetCultureInfo("ru-RU"));

            new GrabberTNF().Grab();
        }
    }



}
