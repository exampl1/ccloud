﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Data.Entity;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using CsvHelper;

namespace StoreTestDataGenerator
{
    class ProgramAddSubject
    {
        static IdGenerator idGenerator;
        static ProgramAddSubject()
        {
            
            Cfg.VendorCode = "magnolica";
            idGenerator = IdGenerator.CreateIdentityGenerator(ConfigurationManager.ConnectionStrings[Cfg.Connection].ConnectionString, "dbo.UidTable");
        }

        static void Main(string[] args)
        {

            ResourceManager.Init(new FakeResourceProvider(), new List<CultureInfo> { CultureInfo.GetCultureInfo("ru-RU") }, CultureInfo.GetCultureInfo("ru-RU"));
            
            using (var dbContext = new StoreContext(Cfg.Connection))
            {
                string code = "vaide";
                var sup = dbContext.Subjects.Where(s => s.Code == code).FirstOrDefault();

                if (sup == null)
                {
                    sup = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = code,
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "magnosib@yandex.ru",
                    });
                    sup.Localization.Add(new SubjectLocalization
                    {
                        CultureCode="ru-RU",
                        Description = "Vaide",
                        FullName = "Vaide",
                        ShortName = "Vaide",
                        Name = "Vaide",
                    });

                }


                //code = "rzz";
                //sup = dbContext.Subjects.Where(s => s.Code == code).FirstOrDefault();

                //if (sup == null)
                //{
                //    sup = dbContext.Subjects.Add(new Subject
                //    {
                //        Id = GetId(),
                //        Code = code,
                //        Status = EntityStatus.Normal,
                //        SubjectType = SubjectType.Organization,
                //        Email = "manager@bbsw.ru",
                //    });
                //    sup.Localization.Add(new SubjectLocalization
                //    {
                //        CultureCode = "ru-RU",
                //        Description = "RZZ",
                //        FullName = "RZZ",
                //        ShortName = "RZZ",
                //        Name = "RZZ",
                //    });

                //}

         
                Console.WriteLine(sup.Id);
                dbContext.SaveChanges();
            }

            Console.ReadKey();
        }

        static long GetId()
        {
            return idGenerator.GetId();
        }
    }
}
