﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Data.Entity;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using CsvHelper;

namespace StoreTestDataGenerator
{
    class ProgramInitOKiss
    {
        static IdGenerator idGenerator;
        static ProgramInitOKiss()
        {
            idGenerator = IdGenerator.CreateIdentityGenerator(ConfigurationManager.ConnectionStrings["TNFConnection"].ConnectionString, "dbo.UidTable");
        }

        static void Main(string[] args)
        {

            string code = "okiss";
            ResourceManager.Init(new FakeResourceProvider(), new List<CultureInfo> { CultureInfo.GetCultureInfo("ru-RU") }, CultureInfo.GetCultureInfo("ru-RU"));

            using (var dbContext = new StoreContext("OKissConnection"))
            {
                // Описание дефолтного классификатора
                var classifier = dbContext.Classifiers.Where(c => c.Code == "ProductGroup.Default").FirstOrDefault();
                if (classifier == null)
                {
                    classifier = dbContext.Classifiers.Add(new Classifier
                    {
                        Code = ProductGroup.DefaultClassifierCode,
                        Id = GetId(),
                        Status = EntityStatus.Normal,
                        ClassifierType = 1,
                    });

                    classifier.Localization.Add(new ClassifierLocalization
                    {
                        CultureCode="ru-RU",
                        Description = "Классификатор товарных групп по умолчанию",
                        Title = "Группы товаров",
                    });
                }


                // Субьект-поставщик товара для сайта
                var sub = dbContext.Subjects.Where(s => s.Code == code).FirstOrDefault();
                if (sub == null)
                {
                    
                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = code,
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@o-kiss.ru",
                    });
                    
                    // Описание субьекта
                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode="ru-RU",
                        Description = "O-KISS",
                        FullName = "Купальники O-KISS",
                        ShortName = "O-KISS",
                        Name = "O-KISS",
                    });
                }

                // Описание сайта
                var suplier_data = new Supplier
                {
                    Logo = "O-KISS.ru",
                    Domain = "o-kiss.ru",
                    PhoneBlock = "тел. 8-953-77-58-358",
                    FirstFooterBlock = "",
                    SecondFooterBlock = "",
                    ThirdFooterBlock = ""
                };

                var suplier = dbContext.Suppliers.FirstOrDefault();
                if (suplier == null)
                {
                    suplier_data.SubjectId = sub.Id;
                    dbContext.Suppliers.Add(suplier_data);
                }
                else
                {
                    suplier.Banner = suplier_data.Banner;
                    suplier.Domain = suplier_data.Domain;
                    suplier.Logo = suplier_data.Logo;
                    suplier.PhoneBlock = suplier_data.PhoneBlock;
                    suplier.FirstFooterBlock = suplier_data.FirstFooterBlock;
                    suplier.SecondFooterBlock = suplier_data.SecondFooterBlock;
                    suplier.ThirdFooterBlock = suplier_data.ThirdFooterBlock;
                }


                // Рутовая нода в классификаторе
                var node = dbContext.ClassifierNodes.FirstOrDefault();
                if (node == null)
                {
                    var pg = dbContext.ProductGroups.Add(new ProductGroup
                    {
                        Id = GetId(),
                        Code = "root",
                        Status = EntityStatus.Normal,
                    });

                    pg.Localization.Add(new DictionaryItemLocalization
                    {
                        CultureCode = "ru-RU",
                        Title = "root",
                        Description = "root",
                    });

                    node = dbContext.ClassifierNodes.Add(new ClassifierNode
                    {
                        ClassifierId = classifier.Id,
                        Id = GetId(),
                        Code = "root",
                        EntityId = pg.Id,
                        Status = EntityStatus.Normal,
                    });

                    node.SetParent(null);
                }

                dbContext.SaveChanges();
                
                Console.WriteLine("done");
            }

            Console.ReadKey();
        }

        static long GetId()
        {
            return idGenerator.GetId();
        }
    }
}
