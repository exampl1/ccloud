﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Data.Entity;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using CsvHelper;

namespace StoreTestDataGenerator
{
    class ProgramTest
    {
        static ProgramTest()
        {
        }

        static void Main(string[] args)
        {
            Cfg.VendorCode = "okiss";
            var path = "c:/inetpub/WebApp.Store.OKiss/imgs/okiss/";

            var dirs = Directory.GetDirectories(path);

            foreach (var dir in dirs)
            {
                var seppos = dir.LastIndexOf("/");
                var dname = dir.Substring(seppos + 1, dir.Length - seppos - 1);

                File.Copy(dir + "/00_full.jpg", path + "/" + dname + ".jpg");
            }

            Console.ReadKey();
        }

    }
}
