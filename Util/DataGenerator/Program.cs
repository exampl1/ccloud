﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 0;
            int length = 9;
            string format = string.Empty;
            if (args.Length > 1 && int.TryParse(args[0], out count) && count > 0 && !string.IsNullOrEmpty(args[1]))
            {
                if (args.Length > 2)
                {
                    int.TryParse(args[2], out length);
                }
                format = args[1];
                for (int i = 0; i < count; i++)
                {
                    Console.WriteLine(string.Format(format, Generator.GetNewCode(length)));
                }                
            }
            else
            {
                Console.WriteLine("Parameters needed: countToGenerate format_string_to_output");
            }
        }
    }
}
