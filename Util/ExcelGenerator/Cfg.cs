﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelGenerator
{
    static class Cfg
    {
        private static string supplier;
        public static string Supplier
        {
            get { return supplier; }
            set
            {
                supplier = value;

                if (supplier == "bbsw")
                {
                    Connection = "TNFConnection";
                    BaseUrl = "http://bbsw.ru";
                } 
            }
        }

        public static decimal PriceCorrection = 1;
        public static string Connection = "DefaultConnection";
        public static string BaseUrl = "http://optodrom.ru";
        public static string[] VendorCode = new string[] { "magnolica" };
    }
}
