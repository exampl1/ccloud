﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging; 
using DocumentFormat.OpenXml.Spreadsheet; 
using DocumentFormat.OpenXml;
using System.IO;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using System.Drawing;
using System.Net;

using C2.WebApp.Store.Models;

using C2.DataModel.StoreModel;

using C2.DataManager.Store;
using C2.DataMapping.Store;
using C2.AppModel;

using System.Data.Entity;

namespace ExcelGenerator
{
    /// <summary>
    /// Вспомогательные функции
    /// </summary>
    class ExcelHelper
    {
        readonly int UPI = 914400;
        readonly int RowHeigh = 61;
        readonly int firstRowHeigh = 26;
        readonly float UPP = 0.75f;
        private WebClient webClient = new WebClient();

        StoreContext _context;
        StoreDataManager _dataManager;

        public ExcelHelper()
        {
            _context = new StoreContext(Cfg.Connection);
            _dataManager = new StoreDataManager(_context);
        }

        public void WriteExcel(string template = "template.xlsx")
        {
            //string[] cursor = new string [] { @"/", @"\", @"|" };
            foreach (string code in Cfg.VendorCode)
            {
                string output = code + ".xlsx";
                Console.Write("Writign {0}", output);

                File.Copy(template, output, true);

                //Открываем скопированный шаблон
                using (SpreadsheetDocument myWorkbook = SpreadsheetDocument.Open(output, true))
                {
                    //Получаем доступ к Workbook part, которая содержит все ссылки
                    WorkbookPart workbookPart = myWorkbook.WorkbookPart;

                    //Получаем первый лист worksheet. 
                    WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();

                    // Объект SheetData будет содержать все нужные данные
                    SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                    var products = _context.Products
                        .Include(p => p.ProductGroups)
                        .Include(p => p.Localization)
                        .Include(p => p.Vendor)
                        .Include(p => p.Vendor.Localization)
                        .Include(p => p.Offers)
                        .Include(p => p.MediaCollection)
                        .Include(p => p.MediaCollection.Select(m => m.MediaItem))
                        .Include(p => p.Properties)
                        .Include(p => p.Properties.Select(pr => pr.PropertyType))
                        .Include(p => p.Properties.Select(pr => pr.PropertyType.Localization)).Where(x => x.Vendor.Code == code).ToList();

                    int index = 2;

                    foreach (Product p in products)
                    {
                        var minoffer = p.Offers.GetMinOffer();

                        sheetData.AppendChild(CreateContentRow(index++, worksheetPart,
                            Cfg.BaseUrl + "/Product/Item/" + p.Id,
                            Cfg.BaseUrl + p.MediaCollection.OrderByDescending(m => m.MediaItemId).First().AsPreview(),
                            p.VendorCode,
                            p.CurrentLocalization.Title + " " + p.Vendor.CurrentLocalization.Name,
                            string.Join(", ", p.Properties.Where(g => g.PropertyType.Code == "color").Select(v => v.StringValue)),
                            string.Join(", ", p.Properties.Where(g => g.PropertyType.Code == "size").Select(v => v.StringValue)),
                            minoffer.PriceOpt40.GetValueOrDefault().ToString("N")
                            ));

                        if (index % 10 == 0)
                        {
                            //Console.SetCursorPosition(Console.CursorTop, Console.CursorLeft-1);
                            Console.Write(".");
                        }
                    }


                    //sheetData.AppendChild(CreateContentRow(2, worksheetPart, "http://bbsw.ru/imgs/okiss/12006/00_prv.jpg", "http://www/pic.jpg", "c"));


                    workbookPart.Workbook.Save();
                    myWorkbook.Close();

                    Console.WriteLine("done");
                }

            }
        }


        private string[] headerColumns = new string[] { "A", "B", "C", "D", "E", "F", "G", "H" };
        Row CreateContentRow(int index, WorksheetPart wsp, params string[] @params)
        {
            //Создаем новую строку
            Row r = new Row
            {
                RowIndex = (UInt32)index,
                Height = RowHeigh * UPP,
                CustomHeight = true
            };

            r.AppendChild(CreateHyperlinkCell(wsp, headerColumns[1], @params[2], @params[0], index));
            CreateDrawing(headerColumns[0], @params[1], index, wsp);

            //Cell firstCell = CreateTextCell(headerColumns[0], @params[0], index);
            //r.AppendChild(firstCell);

            //Создаем ячейки таблицы, которые содержат данные
            for (int i = 3; i < @params.Length; i++)
            {
                //Cell c = new Cell();
                //c.CellReference = headerColumns[i] + index;

                //CellValue v = new CellValue();
                //v.Text = @params[i];

                //c.AppendChild(v);
                r.AppendChild(CreateTextCell(headerColumns[i-1], @params[i], index));
                //r.AppendChild(CreateHyperlinkCell(wsp, headerColumns[i], @params[i], index));
            }

            return r;
        }

        Cell CreateTextCell(string header, string text, int index)
        {
            //Создаем новую ячейку типа InlineString 
            Cell c = new Cell
            {
                DataType = CellValues.InlineString,
                CellReference = header + index
            };

            //Добавляем текст в ячейку
            InlineString inlineString = new InlineString();
            Text t = new Text
            {
                Text = text
            };

            inlineString.AppendChild(t);
            c.AppendChild(inlineString);

            return c;
        }
        
        Cell CreateHyperlinkCell(WorksheetPart wsp, string header, string text, string url, int index)
        {
            var hyperlinks = wsp.Worksheet.GetFirstChild<Hyperlinks>();
            if (hyperlinks == null)
            {
                // Create the hyperlinks node
                hyperlinks = new Hyperlinks();

                PageMargins pageMargins = wsp.Worksheet.Descendants<PageMargins>().First();
                wsp.Worksheet.InsertBefore<Hyperlinks>(hyperlinks, pageMargins);
            }

            var c = CreateTextCell(header, text, index);

            Hyperlink h = new Hyperlink() { Reference = header + index, Id = string.Empty };

            var hyperlinkRelationship = wsp.AddHyperlinkRelationship(new Uri(url, UriKind.Absolute), true);

            h.Id = hyperlinkRelationship.Id;
            hyperlinks.AppendChild<Hyperlink>(h);
            wsp.Worksheet.Save();

            return c;
        }

        void CreateDrawing(string header, string sImagePath, int index, WorksheetPart wsp)
        {
            ImagePart imgp;
            DrawingsPart dp;
            WorksheetDrawing wsd;             

            if (wsp.DrawingsPart == null)
            {
                //----- no drawing part exists, add a new one
                dp = wsp.AddNewPart<DrawingsPart>();
                imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                wsd = new WorksheetDrawing();
            }
            else
            {
                //----- use existing drawing part
                dp = wsp.DrawingsPart;
                imgp = dp.AddImagePart(ImagePartType.Png);
                dp.CreateRelationshipToPart(imgp);
                wsd = dp.WorksheetDrawing;
            }


            int imageNumber = dp.ImageParts.Count<ImagePart>();
            if (imageNumber == 1)
            {
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);
                wsp.Worksheet.AppendChild(drawing);
            }

            var tmpFile = Path.GetTempFileName();
            webClient.DownloadFile(sImagePath, tmpFile);

            using (FileStream fs = new FileStream(tmpFile, FileMode.Open))
            {
                imgp.FeedData(fs);
            }

            NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
            nvdp.Id = (UInt16)index;
            nvdp.Name = sImagePath;
            nvdp.Description = sImagePath;

            DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks
            {
                NoChangeAspect = true,
                NoChangeArrowheads = true,
            };

            NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties
            {
                PictureLocks = picLocks
            };

            NonVisualPictureProperties nvpp = new NonVisualPictureProperties
            {
                NonVisualDrawingProperties = nvdp,
                NonVisualPictureDrawingProperties = nvpdp
            };

            DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch
            {
                FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle()
            };

            DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip
            {
                Embed = dp.GetIdOfPart(imgp),
                CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print,
            };

            BlipFill blipFill = new BlipFill
            {
                Blip = blip,
                SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle(),
            };

            blipFill.Append(stretch);

            DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset()
            {
                X = 0, Y = 0,
            };

            DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D { Offset = offset };

            Bitmap bm = new Bitmap(tmpFile);
            //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
            //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
            //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c

            // размеры картинки, приведенные к высоте строки
            DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents
            {
                Cx = (long)(bm.Width * ((float)RowHeigh / bm.Height)) * (long)((float)UPI / bm.HorizontalResolution),
                Cy = (long)RowHeigh * (long)((float)UPI / bm.VerticalResolution)
            };

            bm.Dispose();
            File.Delete(tmpFile);

            t2d.Extents = extents;

            ShapeProperties sp = new ShapeProperties
            {
                BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto,
                Transform2D = t2d,
            };

            DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry
            {
                Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle,
                AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList(),
            };
            sp.Append(prstGeom);
            sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

            DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
            picture.NonVisualPictureProperties = nvpp;
            picture.BlipFill = blipFill;
            picture.ShapeProperties = sp;

            Position pos = new Position
            {
                X = 0,
                Y = (int)((RowHeigh / 0.96 * UPI / 100) * (index - 2)) + (int)(firstRowHeigh * UPI / 100),
            };

            Extent ext = new Extent
            {
                Cx = extents.Cx,
                Cy = extents.Cy
            };

            AbsoluteAnchor anchor = new AbsoluteAnchor();
            anchor.Position = pos;
            anchor.Extent = ext;
            anchor.Append(picture);
            anchor.Append(new ClientData());

            wsd.Append(anchor);
            wsd.Save(dp);
            
        }
    }


}
