﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YML
{
    [Serializable]
    public class yml_catalog
    {
        private string DateValue = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

        [XmlAttribute(AttributeName = "date")]
        public string Date
        {
            get { return DateValue; }
            set { DateValue = value; }
        }

        private ShopClass ShopValue;

        [XmlElement(ElementName = "shop")]
        public ShopClass Shop
        {
            get { return ShopValue; }
            set { ShopValue = value; }
        }

        public yml_catalog()
        {
            Shop = new ShopClass();
            
        }
    }

    [Serializable]
    public class ShopClass
    {
        private string NameValue;

        [XmlElement(ElementName = "name", Order = 1)]
        public string Name
        {
            get { return NameValue; }
            set { NameValue = value; }
        }

        private string CompanyValue;
        [XmlElement(ElementName = "company", Order = 2)]
        public string Company
        {
            get { return CompanyValue; }
            set { CompanyValue = value; }
        }

        private string UrlValue;
        [XmlElement(ElementName = "url", Order = 3)]
        public string Url
        {
            get { return UrlValue; }
            set { UrlValue = value; }
        }

        //private Collection<Currency> CurrencyValue;
        [XmlArray(ElementName = "currencies", Order = 4)]
        [XmlArrayItem(ElementName = "currency")]
        public Collection<Currency> Currency;
        //{get { return CurrencyValue; }
        //set { CurrencyValue = value; }
        //}

        private Collection<Category> CategoriesValue;
        [XmlArray(ElementName = "categories", Order = 5)]
        [XmlArrayItem(ElementName = "category")]
        public Collection<Category> Categories
        {
            get { return CategoriesValue; }
            set { CategoriesValue = value; }
        }

        [XmlArray(ElementName = "offers", Order = 10)]
        [XmlArrayItem(ElementName = "offer")]
        public Collection<Offer> offers;

        public ShopClass(string name, string company)
        {
            Name = name; Company = company;
        }

        public ShopClass()
        {
            Currency = new Collection<Currency>();
        }

    }


    [Serializable]
    public class Currency
    {
        private string IdValue = "RUR";

        [XmlAttribute("id")]
        public string Id
        {
            get { return IdValue; }
            set { IdValue = value; }
        }
        private int RateValue = 31;
        [XmlAttribute("rate")]
        public int Rate
        {
            get { return RateValue; }
            set { RateValue = value; }
        }

        public Currency()
        {
        }
    }

    public class OfferCategory
    {
        public OfferCategory()
        {
            //Type = "Own";
        }

        public OfferCategory(long id) : this()
        {
            Id = id;
        }

        [XmlText]
        public long Id
        {
            get;
            set;
        }
    
        [XmlAttribute(AttributeName = "type")]
        public string Type
        {
            get;
            set;
        }
    }

    [XmlRoot(ElementName = "category")]
    public class Category
    {
        private int IdValue = -100;
        [XmlAttribute(AttributeName = "id")]
        public int Id
        {
            get { return IdValue; }
            set { IdValue = value; }
        }

        private int ParentIdValue;
        [XmlAttribute(AttributeName = "parentId")]
        public string ParentId
        {
            get { return ParentIdValue > 0 ? ParentIdValue.ToString() : null; }
            set
            {
                int i = 0;
                int.TryParse(value, out i);
                ParentIdValue = i;
            }
        }

        private string NameValue;
        [XmlText]
        public string Name
        {
            get { return NameValue; }
            set { NameValue = value; }
        }

        public Category()
        {
        }
    }

    [XmlRoot(ElementName = "offer")]
    public class Offer
    {
        [XmlAttribute(AttributeName = "bid")]
        public string Bid
        {
            get;
            set;
        }        
        
        [XmlAttribute(AttributeName = "cbid")]
        public string Cbid
        {
            get;
            set;
        }
        

        private long IDValue;
        [XmlAttribute(AttributeName = "id")]
        public long ID
        {
            get { return IDValue; }
            set { IDValue = value; }
        }

        [XmlAttribute(AttributeName = "type")]
        public string Type
        {
            get;
            set;
        }
        
        private bool AvailableValue;
        [XmlAttribute(AttributeName = "available")]
        public bool Available
        {
            get { return AvailableValue; }
            set { AvailableValue = value; }
        }

        private string UrlValue;
        [XmlElement(ElementName = "url", Order = 1)]
        public string Url
        {
            get { return UrlValue; }
            set { UrlValue = value; }
        }

        [XmlElement(ElementName = "price", Order = 2)]
        public double Price
        {
            get;
            set;
        }

        private string CurrencyValue;
        [XmlElement(ElementName = "currencyId", Order = 3)]
        public string Currency
        {
            get { return CurrencyValue; }
            set { CurrencyValue = value; }
        }




        [XmlElement(ElementName = "picture", Order = 5)]
        public List<string> Pictures
        {
            get;
            set;
        }

        [XmlElement(ElementName = "categoryId", Order = 4)]
        public OfferCategory Category
        {
            get;
            set;
        }

        [XmlElement(ElementName = "vendorCode", Order = 9)]
        public string VendorCode { get; set; }

        [XmlElement(ElementName = "country_of_origin", Order = 20)]
        public string Country { get; set; }
        
        [XmlElement(ElementName = "typePrefix", Order = 7)]
        public string TypePrefix { get; set; }

        private bool DeliveryValue;
        [XmlElement(ElementName = "delivery", Order = 6)]
        public bool Delivery
        {
            get { return DeliveryValue; }
            set { DeliveryValue = value; }
        }

        [XmlArray(ElementName = "orderingTime", Order = 19)]
        [XmlArrayItem(ElementName = "ordering")]
        public Collection<string> OrderingTime
        {
            get;
            set;
        }

        private string ModelValue;
        [XmlElement(ElementName = "model", Order = 10)]
        public string Model
        {
            get { return ModelValue; }
            set { ModelValue = value; }
        }

        private string VendorValue;
        [XmlElement(ElementName = "vendor", Order = 8)]
        public string Vendor
        {
            get { return VendorValue; }
            set { VendorValue = value; }
        }

        private string DescriptionValue;
        [XmlElement(ElementName = "description", Order = 11)]
        public string Description
        {
            get { return DescriptionValue; }
            set { DescriptionValue = value; }
        }

        public Offer()
        {
            Type = "vendor.model";
            SalesNotes = "предоплата";

        }

        [XmlElement(ElementName = "name", Order = 18)]
        public string Name { get; set; }

        [XmlElement(ElementName = "sales_notes", Order = 25)]
        public string SalesNotes { get; set; }

        [XmlElement(ElementName = "adult", Order = 30)]
        public string Adult { get; set; }
    
        [XmlElement(ElementName = "param", Order = 40)]
        public List<Param> Params
        {
            get;
            set;
        }
    }


    [Serializable]
    public class Param
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "unit")]
        public string Unit { get; set; }
        
        [XmlText]
        public string Value { get; set; }
    }
}
