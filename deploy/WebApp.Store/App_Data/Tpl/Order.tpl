﻿<div style='font-family: Segoe UI, Segoe, Arial, sans-serif'>
  <p>Здравствуйте, {{CustomerInfo}}!</p>
  <p>Ваш заказ {{Number}} от {{Date}} на сайте <a href='http://optodrom.ru'>http://optodrom.ru</a> подтвержден менеджером и ожидает оплаты.</p>
  <h2>Состав заказа</h2>
  <table border='1' cellpadding='2' cellspacing='1'>
    <tr>
      <th>№</th>
      <th>Артикул</th>
      <th>Название товара</th>
      <th>Количество</th>
      <th>Ед.</th>
      <th>Цена</th>
      <th>Итого</th>
    </tr>
    {{foreach Items Items}}
  </table>
  <p>Итого: {{Total}} р.</p>

{{include Footer}}
</div>